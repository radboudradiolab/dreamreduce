# -*- coding: utf-8 -*-
"""
Created on Mon Nov  7 14:10:15 2016

@author: talens
"""

import os
import logging
import datetime
from pprint import pprint
import time
import glob
from itertools import chain
import collections
import shutil

# set up log
import logging
import time
logfmt = ('%(asctime)s.%(msecs)03d [%(levelname)s, %(process)s] %(message)s '
          '[%(funcName)s, line %(lineno)d]')
datefmt = '%Y-%m-%dT%H:%M:%S'
logging.basicConfig(level='INFO', format=logfmt, datefmt=datefmt)
logFormatter = logging.Formatter(logfmt, datefmt)
logging.Formatter.converter = time.gmtime #convert time in logger to UTC
log = logging.getLogger()

import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields

from astropy.io import fits
from astropy.wcs import WCS
from astropy.time import Time
from astropy.stats import sigma_clipped_stats, sigma_clip, mad_std
from astropy.table import Table

# for socket connection to control computer
import zmq

from dreamreduce import rawdir_monitor as rm
from dreamreduce import configuration as cfg
from dreamreduce import io
from dreamreduce import stacking
from dreamreduce import astrometry
from dreamreduce import photometry
from dreamreduce import misc
from dreamreduce import sigmas

import fitsio


###############################################################################
# Helper functions.
###############################################################################


def headers2recarray(headers, fields):
    nimages = len(headers)

    dtype = [(key, fields[key]) for key in list(fields.keys())]
    array = np.recarray(nimages, dtype=dtype)

    for i in range(nimages):
        header = headers[i]

        for key in list(fields.keys()):
            array[i][key] = header[key]

    return array


def expand_recarray(array, fields):

    dtype = array.dtype.descr + [(key, fields[key])
                                 for key in list(fields.keys())]
    new_array = np.recarray(array.shape, dtype=dtype)

    for key in array.dtype.names:
        new_array[key] = array[key]

    return new_array

###############################################################################
# Functions for reducing dark frames.
###############################################################################


def combine_bias(stack, headers):
    """ Create a masterbias. """

    nbias = len(stack)

    # Create the master bias.
    masterbias = np.nanmean(stack, axis=0)

    # Create the header of the masterbias
    header = fits.Header()
    header['NOBS'] = (nbias, 'Number of images used')
    header['METHOD'] = ('mean', 'Method used to combine the images')
    header['SITE-OBS'] = (str(headers[0]['SITE-OBS'],
                              'utf-8'), 'Observation site')
    header['CAMERA'] = (str(headers[0]['CAMERA'], 'utf-8'),
                        'Camera name on site')
    header['IMAGETYP'] = ('MASTERBIAS', 'Type of image')
    header['EXPTIME'] = (np.mean(headers['EXPTIME']),
                         'Exposure time in seconds')
    header['CCDTEMP'] = (np.mean(headers['CCDTEMP']),
                         'Average CCD temperature (C)')
    header['LSTSEQ'] = (headers[0]['LSTSEQ'],
                        'Exposure num of first frame used')
    header['X0'] = headers[0]['X0']
    header['XSIZE'] = headers[0]['XSIZE']
    header['Y0'] = headers[0]['Y0']
    header['YSIZE'] = headers[0]['YSIZE']

    return masterbias, header


def combine_darks(stack, headers):
    """ Create a masterdark. """

    ndark = len(stack)

    # Create the master dark.
    masterdark = np.nanmean(stack, axis=0)

    # Create the header of the masterdark.
    header = fits.Header()
    header['NOBS'] = (ndark, 'Number of images used')
    header['METHOD'] = ('mean', 'Method used to combine the images')
    header['SITE-OBS'] = (str(headers[0]['SITE-OBS'],
                              'utf-8'), 'Observation site')
    header['CAMERA'] = (str(headers[0]['CAMERA'], 'utf-8'),
                        'Camera name on site')
    header['IMAGETYP'] = ('MASTERDARK', 'Type of image')
    header['EXPTIME'] = (np.mean(headers['EXPTIME']),
                         'Exposure time in seconds')
    header['CCDTEMP'] = (np.mean(headers['CCDTEMP']),
                         'Average CCD temperature (C)')
    header['LSTSEQ'] = (headers[0]['LSTSEQ'],
                        'Exposure num of first frame used')
    header['X0'] = headers[0]['X0']
    header['XSIZE'] = headers[0]['XSIZE']
    header['Y0'] = headers[0]['Y0']
    header['YSIZE'] = headers[0]['YSIZE']

    return masterdark, header


def combine_flats(stack, headers, dark):
    """ Create a masterflat. """

    nflat = len(stack)

    # Create the master flat
    stack = stack - dark
    stack = stack/np.nanmean(stack, axis=(1, 2), keepdims=True)
    masterflat = np.nanmean(stack, axis=0)

    # Create the header of the masterflat
    header = fits.Header()
    header['NOBS'] = (nflat, 'Number of images used')
    header['METHOD'] = ('mean', 'Method used to combine the images')
    header['SITE-OBS'] = (str(headers[0]['SITE-OBS'],
                              'utf-8'), 'Observation site')
    header['CAMERA'] = (str(headers[0]['CAMERA'], 'utf-8'),
                        'Camera name on site')
    header['IMAGETYP'] = ('MASTERFLAT', 'Type of image')
    header['EXPTIME'] = (np.mean(headers['EXPTIME']),
                         'Exposure time in seconds')
    header['CCDTEMP'] = (np.mean(headers['CCDTEMP']),
                         'Average CCD temperature (C)')
    header['LSTSEQ'] = (headers[0]['LSTSEQ'],
                        'Exposure num of first frame used')
    header['X0'] = headers[0]['X0']
    header['XSIZE'] = headers[0]['XSIZE']
    header['Y0'] = headers[0]['Y0']
    header['YSIZE'] = headers[0]['YSIZE']

    return masterflat, header


def reduce_bias_frames(camid, filelist, dirtree, nmin=cfg.nbias_min):
    """ Create a masterbias. """

    log.info('Received {} bias frames.'.format(len(filelist)))

    # Read the files.
    stack, headers = io.read_stack(filelist)

    if (len(stack) == 0):
        log.warn('No images were succesfully read.')
        return

    # Create a recarray containing the required header fields.
    fields = {'LSTSEQ': 'uint32', 'JD': 'float64', 'LST': 'float64',
              'EXPTIME': 'float32', 'CCDTEMP': 'float32',
              'SITE-OBS': '|S20', 'CAMERA': '|S1',
              'X0': 'uint16', 'XSIZE': 'uint16',
              'Y0': 'uint16', 'YSIZE': 'uint16'}
    headers = headers2recarray(headers, fields)

    nbias = len(stack)
    if (nbias >= nmin):

        log.info('Creating masterbias from {} exposures.'.format(nbias))

        masterbias, header = combine_bias(stack, headers)

        filename = '{:08d}{}masterbias.fits'.format(header['LSTSEQ'], camid)
        filename = os.path.join(dirtree['masters'], filename)

        log.info('Saving masterbias to {}'.format(filename))

        io.write_masterframe(filename, masterbias, header)

    else:
        log.info('Not enough valid bias frames.')

    return


def reduce_dark_frames(camid, filelist, dirtree, darktable, nmin=cfg.ndark_min):
    """ Create a masterdark. """

    log.info('Received {} dark frames.'.format(len(filelist)))

    # Read the files.
    stack, headers = io.read_stack(filelist)

    if (len(stack) == 0):
        log.warn('No images were succesfully read.')
        return

    # Create a recarray containing the required header fields.
    fields = {'LSTSEQ': 'uint32', 'JD': 'float64', 'LST': 'float64',
              'EXPTIME': 'float32', 'CCDTEMP': 'float32',
              'SITE-OBS': '|S20', 'CAMERA': '|S1',
              'X0': 'uint16', 'XSIZE': 'uint16',
              'Y0': 'uint16', 'YSIZE': 'uint16'}
    headers = headers2recarray(headers, fields)

    ndarks = len(stack)
    if (ndarks >= nmin):

        log.info('Creating masterdark from {} exposures.'.format(ndarks))

        masterdark, header = combine_darks(stack, headers)

        filename = '{:08d}{}masterdark.fits'.format(header['LSTSEQ'], camid)
        filename = os.path.join(dirtree['masters'], filename)

        log.info('Saving masterdark to {}'.format(filename))

        io.write_masterframe(filename, masterdark, header)

        io.update_table(filename, darktable)
        #log.warn('Running with standard dark, not updating darktable.')
    else:
        log.info('Not enough valid darks.')

    return


def reduce_flat_frames(camid, filelist, dirtree, darktable, nmin=cfg.nflat_min):
    """ Create a masterflat. """

    log.info('Received {} flat frames.'.format(len(filelist)))

    # Read the files.
    stack, headers = io.read_stack(filelist)
    dark, header, __ = io.read_masterdark(darktable)

    if (len(stack) == 0):
        log.warn('No images were succesfully read.')
        return

    # Create a recarray containing the required header fields.
    fields = {'LSTSEQ': 'uint32', 'JD': 'float64', 'LST': 'float64',
              'EXPTIME': 'float32', 'CCDTEMP': 'float32',
              'SITE-OBS': '|S20', 'CAMERA': '|S1',
              'X0': 'uint16', 'XSIZE': 'uint16',
              'Y0': 'uint16', 'YSIZE': 'uint16'}
    headers = headers2recarray(headers, fields)

    nflats = len(stack)
    if (nflats >= nmin):

        log.info('Creating masterflat from {} exposures.'.format(nflats))

        masterflat, header = combine_flats(stack, headers, dark)

        filename = '{:08d}{}masterflat.fits'.format(header['LSTSEQ'], camid)
        filename = os.path.join(dirtree['masters'], filename)

        log.info('Saving masterflat to {}'.format(filename))

        io.write_masterframe(filename, masterflat, header)

    else:
        log.info('Not enough valid flats.')

    return

###############################################################################
# Functions for reducing science frames.
###############################################################################


def utcobs2datetime(utcobs):

    return datetime.datetime.strptime(utcobs, '%Y-%m-%d %H:%M:%S.%f')


def preprocess(stack, headers, darktable):

    nimages = len(stack)

    # Create basic station field from the headers.
    fields = {'lstseq': 'uint32', 'jd': 'float64', 'lst': 'float64',
              'exptime': 'float32', 'ccdtemp': 'float32',
              'dwldtime': 'float32', 'margtime': 'float32'}
    station = headers2recarray(headers, fields)

    # Add fields for derived information.
    fields = {'imnzero': 'uint32', 'imnsat': 'uint32', 'imstd': 'float32',
              'immed': 'uint16', 'oversca0': 'uint16', 'oversca1': 'uint16',
              'oversca2': 'uint16', 'oversca3': 'uint16', 'lstday': 'uint32',
              'lstidx': 'uint32', 'darkfile': 'uint32', 'year': 'uint16',
              'month': 'uint8', 'day': 'uint8', 'hour': 'uint8', 'min': 'uint8',
              'sec': 'uint8', 'fsec': 'float32'}
    station = expand_recarray(station, fields)

    # Add derivatives of the header fields.
    station['lstday'] = station['lstseq']//13500
    station['lstidx'] = station['lstseq'] % 13500


    for i in range(nimages):

        tmp = utcobs2datetime(headers[i]['UTC-OBS'])

        station[i]['year'] = tmp.year
        station[i]['month'] = tmp.month
        station[i]['day'] = tmp.day
        station[i]['hour'] = tmp.hour
        station[i]['min'] = tmp.minute
        station[i]['sec'] = tmp.second
        station[i]['fsec'] = tmp.microsecond


    # Compute statistics on the raw images.
    station['imnzero'] = np.sum((stack == 0), axis=(1, 2))
    station['imnsat'] = np.sum((stack > 64000), axis=(1, 2))
    station['imstd'] = np.nanstd(stack, axis=(1, 2))
    station['immed'] = np.nanmedian(stack, axis=(1, 2))


    # statistics of overscan regions
    if False:

        # previously done using keywords X0, Y0, XSIZE and YSIZE in
        # header

        # Boundaries of the overscan region.
        header = headers[0]
        lx = header['X0'] + 1
        ux = lx + header['XSIZE']
        ly = header['Y0'] + 1
        uy = ly + header['YSIZE']

        station['oversca0'] = np.nanmedian(
            stack[:, uy:, lx:ux], axis=(1, 2))  # Top
        station['oversca1'] = np.nanmedian(
            stack[:, ly:uy, :lx], axis=(1, 2))  # Left
        station['oversca2'] = np.nanmedian(
            stack[:, :ly, lx:ux], axis=(1, 2))  # Bottom
        station['oversca3'] = np.nanmedian(
            stack[:, ly:uy, ux:], axis=(1, 2))  # Right


    # using slices defined in the configuration module
    ccd_os_all = [cfg.ccd_os_top, cfg.ccd_os_left,
                  cfg.ccd_os_bottom, cfg.ccd_os_right]
    for i, ccd_os in enumerate(ccd_os_all):
        slices_tmp = tuple([slice(None)] + ccd_os)
        station['oversca{}'.format(i)] = np.nanmedian(stack[slices_tmp],
                                                      axis=(1,2))
        log.info ('station[oversca{}]: {}'
                  .format(i, station['oversca{}'.format(i)]))


    # convert array to single precision
    stack = stack.astype('float32')


    # Subtract the masterdark.
    masterdark, darkheader, darkname = io.read_masterdark(darktable)
    station['darkfile'] = darkheader['LSTSEQ']
    stack = stack - masterdark


    # add name of masterdark to the header
    for hdr in headers:
        hdr['MDARK'] = (darkname.split('/')[-1],
                        'master dark that was subtracted')


    # Remove the overscan region.
    #stack = stack[:, ly:uy, lx:ux]
    stack = stack[tuple([slice(None)] + cfg.ccd_active)]


    return stack, station


def sunmoon2station(station, siteinfo, headers):

    import ephem

    nimages = len(station)

    fields = {'sunra': 'float32', 'sundec': 'float32', 'sunalt': 'float32',
              'moonra': 'float32', 'moondec': 'float32', 'moonalt': 'float32',
              'moonx': 'uint16', 'moony': 'uint16',
              'moonph': 'float32', 'moonmag': 'float32'}
    dtype = [(key, fields[key]) for key in list(fields.keys())]
    dtype = station.dtype.descr + dtype
    nstation = np.recarray(nimages, dtype=dtype)

    # Copy the existing fields.
    for key in station.dtype.names:
        nstation[key] = station[key]
    station = nstation

    # Compute the sky position of the sun.
    ra, dec, alt = astrometry.sun_position(siteinfo, station['jd'])

    station['sunra'] = ra
    station['sundec'] = dec
    station['sunalt'] = alt

    # Compute the sky position of the moon.
    ra, dec, alt = astrometry.moon_position(siteinfo, station['jd'])

    station['moonra'] = ra
    station['moondec'] = dec
    station['moonalt'] = alt

    # Compute the CCD position of the moon.
    moon = ephem.Moon()

    for i in range(nimages):

        x, y, mask = astrometry.world2pix(headers[i], ra[i], dec[i])
        # x,y should be pixel indices while world2pix provides pixel
        # coordinates
        x -= 1
        y -= 1
        # x, y, mask are 0- or 1 element arrays
        if mask:
            log.info ('Moon is in the field at x,y: {:.0f},{:.0f}'
                      .format(x[0],y[0]))
            station[i]['moonx'] = np.around(x[0])
            station[i]['moony'] = np.around(y[0])
        else:
            station[i]['moonx'] = 9999
            station[i]['moony'] = 9999

        moon.compute(station[i]['jd'] - 2415020)  # ephem uses Dublin JD
        station[i]['moonph'] = moon.moon_phase
        station[i]['moonmag'] = moon.mag

    return station


def lightcurves(stack, headers, station, cat, aper, skyrad, maglim):
    """ Perform aperture photometry on the images. """

    nimages = len(stack)
    naper = len(aper)

    # Initialize the photometry.
    phot = photometry.Photometry(aper, skyrad)

    # Select stars brighter than the magnitude limit.
    select = (cat['vmag'] <= maglim) | cat['inclusion']
    ascc = cat['ascc'][select]
    ra, dec = cat['ra'][select], cat['dec'][select]
    # add Gaia proper motion to starcat; only needed if proper motion
    # is applied below
    if cfg.apply_gaia_pm:
        pmra, pmdec = cat['pmra'][select], cat['pmdec'][select]


    # number of coordinates in ra,dec
    log.info ('inside lightcurves: len(ra): {}, len(dec): {}'
              .format(len(ra), len(dec)))


    # Create a recarray to hold the results.
    names = ['ascc', 'lstseq', 'sky', 'esky', 'peak', 'x',
             'y', 'pflag', 'aflag', 'tmpmag0', 'tmpemag0', 'cflag']
    names = names + ['flux{}'.format(i) for i in range(naper)]
    names = names + ['eflux{}'.format(i) for i in range(naper)]

    formats = ['|S32', 'uint32', 'float64', 'float32', 'float32',
               'float32', 'float32', 'uint8', 'uint8', 'float32', 'float32',
               'uint8']
    formats = formats + naper*['float64'] + naper*['float32']

    curves = np.recarray((0,), names=names, formats=formats)

    for i in range(nimages):

        image = stack[i]
        header = headers[i]
        jd = station[i]['jd']
        lst = station[i]['lst']
        lstseq = station[i]['lstseq']
        moonpos = [station[i]['moonx'], station[i]['moony']]

        # apply proper motion if needed
        if cfg.apply_gaia_pm:
            obsdate = header['UTC-OBS']
            ra_tmp, dec_tmp = astrometry.apply_pm (ra, dec, pmra, pmdec, obsdate)
        else:
            ra_tmp = ra
            dec_tmp = dec


        # Compute positions and photometry.
        x, y, mask = astrometry.world2pix(header, ra_tmp, dec_tmp)
        # x,y should be pixel indices while world2pix provides pixel
        # coordinates
        x -= 1
        y -= 1
        flux, eflux, sky, esky, peak, pflag = phot(image, x, y, moonpos)

        nstars = len(x)

        # Put the results in the recarray.
        curves_ = np.recarray(nstars, names=names, formats=formats)
        curves_['ascc'] = ascc[mask]
        curves_['lstseq'] = np.repeat(lstseq, nstars)
        curves_['sky'] = sky
        curves_['esky'] = esky
        curves_['peak'] = peak
        curves_['x'] = x
        curves_['y'] = y
        curves_['pflag'] = pflag
        curves_['aflag'] = 0  # Will be set later.
        for i in range(naper):
            curves_['flux{}'.format(i)] = flux[:, i]
            curves_['eflux{}'.format(i)] = eflux[:, i]

        curves = np.append(curves, curves_)

    curves['tmpmag0'] = 0.
    curves['tmpemag0'] = 0.
    # changed from 1 to 0
    curves['cflag'] = 0

    # Sort the array be ascc first and lstseq second.
    sort = np.lexsort((curves['lstseq'], curves['ascc']))
    curves = curves[sort]

    return curves


def calibrate_clouds(lstidx, skyidx, res, errsq, lstlen, method='clip'):

    # number of healpixels
    hp_npix = 12*4**cfg.hp_level

    # initialize output arrays
    nobs = np.zeros((lstlen, hp_npix), dtype='uint32')
    clouds = np.full((lstlen, hp_npix), fill_value=np.nan, dtype='float32')
    sigma = np.full((lstlen, hp_npix), fill_value=np.nan, dtype='float32')
    #std_w = np.full((lstlen, hp_npix), fill_value=np.nan, dtype='float32')

    if (len(lstidx) == 0):
        return nobs, clouds, sigma #, std_w

    # input [res] and [errsq] are flat arrays the size of the masked
    # curves array, i.e. corresponding to the number of (masked) flux
    # measurements in an image (or a stack of 50 images in the old
    # reduction)
    #
    # [lstidx] and [skyidx] have the same length as [res] and [errsq]
    # and indicate the LST epoch and healpixel index, respectively
    #
    # determine array of flat indices [idx_flat] - with same length as
    # [lstidx], [skyidx], [res] and [errsq] - of the tuple of array
    # indices (lstidx, skyidx) of array with shape (lstlen, hp_npix)
    idx_flat = np.ravel_multi_index((lstidx, skyidx), (lstlen, hp_npix))
    # determine unique flat indices, including the indices to
    # reconstruct the [idx_flat] array from [idx_flat_uniq], so
    # len(idx_inv) = len(idx_flat) and idx_flat_uniq[idx_inv] = idx_flat
    idx_flat_uniq, idx_inv = np.unique(idx_flat, return_inverse=True)
    # convert the unique flat indices to indices of 2-dim array with
    # shape (lstlen, hp_npix); so [lstidx_2d] and [skyidx_2d] are
    # unique indices, i.e. marking a particular epoch/healpixel
    # combination only once - to save a single value for each
    # epoch/healpixel combination that is returned by the
    # [sigmas.find_par_sigma] function below
    lstidx_2d, skyidx_2d = np.unravel_index(idx_flat_uniq, (lstlen, hp_npix))

    # Compute the clouds calibration and fill nobs, clouds and sigma arrays
    nobs[lstidx_2d, skyidx_2d] = np.bincount(idx_inv)

    if method == 'orig':

        if True:
            # original find_par_sigma
            clouds[lstidx_2d, skyidx_2d], sigma[lstidx_2d, skyidx_2d] = (
                sigmas.find_par_sigma (idx_inv, res, errsq))

        else:

            par, err3 = (sigmas.find_par_sigma (idx_inv, res, errsq))

            # save par as clouds
            clouds[lstidx_2d, skyidx_2d] = par

            # sigma (or err3) above only consists of the additional error
            # required to make the chi-square equal to unity; add that
            # additional error to errsq in the weights to determine the
            # final error in weighted mean
            w = 1/(errsq + err3[idx_inv]**2)

            # sum of weights and weights**2
            V1 = np.bincount(idx_inv, w)
            V2 = np.bincount(idx_inv, w**2)

            # the standard variance of the weighted mean
            var_mean_w = 1 / V1

            # save corresponding standard deviation as sigma
            sigma[lstidx_2d, skyidx_2d] = np.sqrt(var_mean_w)

            # the biased weighted sample variance
            var_w_biased = np.bincount(idx_inv, w*(res - par[idx_inv])**2) / V1
            # the unbiased weighted sample variance
            var_w_unbiased = var_w_biased / (V1 - V2/V1)

            # unbiased weighted sample standard deviation
            #std_w[lstidx_2d, skyidx_2d] = np.sqrt(var_w_unbiased)

    else:
        # using alternative combination, where different methods
        # can be used:
        # clip: return weighted mean and weighted sample STD using
        #       sigma clipping, rejecting measurements with:
        #       abs(res - median) > 3 * std
        #       where std = standard deviation of sample
        # median: return simple median and corresponding error
        # Henk/wmean: return weighted mean its error as sigma (no clipping)
        clouds[lstidx_2d, skyidx_2d], sigma[lstidx_2d, skyidx_2d] = (
            sigmas.find_par_sigma_alt (idx_inv, res, errsq, method))


    return nobs, clouds, sigma #, std_w


def live_calibration(station, curves, cat, sysfile, dirtree, use_zps=False,
                     fits_zps=None, camid=None, header=None,
                     socket2control=False):

    # Create empty arrays.
    sysmag = np.zeros(len(curves), dtype='float32')
    cflag = np.zeros(len(curves), dtype='uint8')

    # Perform look-up in station field.
    args = np.searchsorted(station['lstseq'], curves['lstseq'])
    lst = station['lst'][args]
    exptime = station['exptime'][args]
    # this lstseq is for zeropoints calibration, which assumes there
    # is only one epoch being processed (rather than e.g.  the
    # original 50 epochs simultaneously)
    lstseq = station['lstseq'][args[0]]

    # short filename for logging
    fn_short = '{}{}'.format(lstseq, camid)

    # Perform look-up in catalogue.
    args = np.searchsorted(cat['ascc'], curves['ascc'])
    ra = cat['ra'][args]
    dec = cat['dec'][args]
    vmag = cat['vmag'][args]
    incl = cat['inclusion'][args]

    # Convert fluxes to magnitudes.
    mag, emag = misc.flux2mag(curves['flux0']/exptime, curves['eflux0']/exptime)

    # Convert RA to HA.
    ha = astrometry.ra2ha(ra, lst)

    # Open the latest systematics file.
    f = io.SysFile(sysfile)

    # Compute the magnitude calibration.
    ascc, nobs, _, sigma_cal = f.read_magnitudes()

    tmp = np.zeros(len(curves))
    for i in range(len(curves)):
        if curves['ascc'][i] in ascc:
            tmp[i] = sigma_cal[ascc == curves['ascc'][i]][0]

    sigma_cal = tmp

    # Compute the transmission calibration.
    pg, nobs, trans = f.read_transmission()
    idx1, idx2 = pg.radec2idx(ha, dec)
    sysmag = sysmag + trans[idx1, idx2]


    # split between original calibration and alternative individual
    # zeropoints calibration; input fits_zps is assumed to be
    # existing, which should be checked before calling this function
    if not use_zps:

        log.info ('using original sys calibration in reduction.live_calibration')

        # update cflag if too few stars in grid cell
        cflag = np.where(nobs[idx1, idx2] < 25, cflag+2, cflag)

    else:

        log.info ('using zeropoints from {} in reduction.live_calibration'
                  .format(fits_zps))

        if False:
            log.info ('mag: {}'.format(mag))
            log.info ('sysmag: {}'.format(sysmag))
            log.info ('mag-sysmag: {}'.format(mag-sysmag))
            log.info ('vmag: {}'.format(vmag))
            log.info ('mag-vmag-sysmag: {}'.format(mag-vmag-sysmag))


        # using zeropoints table that contain the zeropoints at each
        # LSTIDX; this assumes that only a single LST epoch is
        # supplied (rather than 50)
        lstidx_sidday = station['lstidx'][0]
        log.info ('lstidx_sidday: {}'.format(lstidx_sidday))
        try:
            table_zps = Table(fitsio.FITS(fits_zps)[str(lstidx_sidday)].read())
            # make sure table is sorted by ascc
            table_zps.sort('ascc')
        except:
            log.warn ('unable to read LSTIDX extension {} of table_zps in '
                      'reduction.live_calibration; skipping zeropoints '
                      'calibration for {}'
                      .format(lstidx_sidday, fn_short))
            # return the input curves and None for nobs, clouds and sigma
            return curves, None, None, None


        # find matching indices in table_zps for sources in
        # curves['ascc']; using the searchsorted method for that
        # requires that table_zps[ascc] contains all possible IDs that
        # are in curves[ascc]; if there are elements missing, the
        # wrong indices will be returned. So check if there are any
        # values in curves[ascc] that are not in table_zps[ascc]

        ascc_curves = curves['ascc'].astype(int)
        missing_list = list(set(ascc_curves) - set(table_zps['ascc']))
        n_missing = len(missing_list)
        if n_missing == 0:
            # it is safe to use searchsorted
            t0 = time.time()
            indices = np.searchsorted(table_zps['ascc'], ascc_curves)
            log.info ('time spent in searchsorted in [live_calibration] for '
                      '{}: {:.3f}s'.format(fn_short, time.time()-t0))
        else:
            log.warn ('{} sources in curves[\'ascc\'] are not present in '
                      'table_zps[\'ascc\'] for {}: {}'
                      .format(n_missing, fn_short, missing_list))


            # with missing stars, there are two options:
            #
            # (1) find nearby LSTs for which the star is available
            #
            # (2) discard the missing stars; need to take care that
            #     any array with the length of curves that is used
            #     below needs to be masked properly

            if False:

                # try option (1) first, going down and up in LST up to
                # some number of steps
                dlst_range = chain(range(-1,-11,-1),range(1,11))
                for dlst in dlst_range:

                    # read table_zps at new LSTIDX
                    table_zps_tmp = Table(fitsio.FITS(fits_zps)[
                        str(lstidx_sidday+dlst)].read())

                    # loop missing stars
                    tmp_list = missing_list
                    for star in tmp_list:
                        # check if present in new table_zps
                        if star in table_zps_tmp['ascc']:
                            # if so, add it to original table_zps
                            index = list(table_zps_tmp['ascc']).index(star)
                            table_zps.add_row(table_zps_tmp[index])
                            # update missing_list
                            missing_list.pop(star)


                    # check if missing_list is empty
                    if len(missing_list)==0:
                        # sort table_zps
                        table_zps.sort('ascc')
                        # break out of for dlst loop
                        break



            # if missing_list still not empty, go for option (2)
            if len(missing_list) > 0:

                log.warn ('discarding the missing stars from curves and '
                          'other relevant arrays for {}'.format(fn_short))

                mask_keep = np.ones(len(ascc_curves), dtype=bool)
                ascc_curves_list = list(ascc_curves)

                for ascc_missing in missing_list:

                    index_curves = ascc_curves_list.index(ascc_missing)
                    mask_keep[index_curves] = False

                    #log.info ('ascc_missing: {}'.format(ascc_missing))
                    #log.info ('index: {}, ascc_curves[index]: {}'
                    #          .format(index, ascc_curves[index]))


                # mask relevant arrays
                curves = curves[mask_keep].copy()
                cflag = cflag[mask_keep]
                ra = ra[mask_keep]
                ha = ha[mask_keep]
                dec = dec[mask_keep]
                vmag = vmag[mask_keep]
                incl = incl[mask_keep]
                mag = mag[mask_keep]
                emag = emag[mask_keep]
                sigma_cal = sigma_cal[mask_keep]
                sysmag = sysmag[mask_keep]


                # search sorted
                ascc_curves = curves['ascc'].astype(int)
                indices = np.searchsorted(table_zps['ascc'], ascc_curves)


        # instrumental magnitude used above is aperture 0
        zp0 = table_zps['zp0'][indices].value
        zp0err = table_zps['zp0err'][indices].value
        #mag0_cal = zp0 + mag - 25
        #zp1 = table_zps['zp1'][indices]
        #mag1_cal = zp1 + mag - 25

        # when calculating zp, m0 offset of 25 was not applied in
        # flux2mag, and also, sysmag is defined as -zp:
        # zp = mag_cal - mag_inst (+ airmass*k)
        # sysmag = mag_inst + 25 - mag_cal = 25 - zp
        sysmag0 = 25 - zp0
        #sysmag1 = 25 - zp1

        if False:
            log.info ('zp0:    {}'.format(zp0))
            log.info ('zp0err: {}'.format(zp0err))
            log.info ('sysmag0: {}'.format(sysmag0))
            log.info ('mag-sysmag0: {}'.format(mag-sysmag0))
            log.info ('mag-vmag-sysmag0: {}'.format(mag-vmag-sysmag0))

        #log.info ('zp1: {}'.format(zp1.value))
        #log.info ('sysmag1: {}'.format(sysmag1.value))
        #log.info ('mag-sysmag1: {}'.format(mag-sysmag1.value))
        #log.info ('mag-vmag-sysmag1: {}'.format(mag-vmag-sysmag1.value))

        # the checks and statistics below use the original sysmag, or
        # actually trans[idx1,idx2], to compare the original
        # calibration with this zps alternative

        # check for nan/infinite values in sysmag
        mask_sysmag_finite = np.isfinite(sysmag)
        n_invalid = np.sum(~mask_sysmag_finite)
        if n_invalid > 0:
            log.error ('{} invalid values in sysmag array'.format(n_invalid))


        # check for nan/infinite values in sysmag0
        mask_sysmag0_finite = np.isfinite(sysmag0)
        n_invalid0 = np.sum(~mask_sysmag0_finite)
        if n_invalid0 > 0:
            log.error ('{} invalid values in sysmag0 array'.format(n_invalid0))


        # combined finite mask
        mask_finite = mask_sysmag_finite & mask_sysmag0_finite


        # clipped statistics dsysmag0
        dsysmag0 = sysmag[mask_finite] - sysmag0[mask_finite]
        mean0, median0, std0 = sigma_clipped_stats (dsysmag0)
        log.info ('mean0: {:.3f}, median0: {:.3f}, std0: {:.3f}'
                  .format(mean0, median0, std0))


        # clipped statistics dsysmag1
        #dsysmag1 = sysmag[mask_finite] - sysmag1[mask_finite]
        #mean1, median1, std1 = sigma_clipped_stats (dsysmag1)
        #log.info ('mean1: {:.3f}, median1: {:.3f}, std1: {:.3f}'
        #          .format(mean1, median1, std1))

        # adopting sysmag0 as sysmag
        sysmag = sysmag0
        sysmagerr = zp0err

        # also update cflag like is done after original sysmag
        # determination? Which adds 2 to cflag if the sysmag value
        # is determined from nobs (shape: 13502, 722) having less
        # than 25 stars in a grid cell. Actually, maybe for the
        # zeropoints calibration this does not need to be done, as
        # they are individual zeropoints which will be combined to
        # a healpix average/median value later on, at which stage
        # the cflag could be updated.



    # CHECK!!! - turn off intrapix correction for zeropoints
    # calibration
    if use_zps:
        log.warn ('skipping intrapix correction to sysmag in zeropoints '
                  '[reduction.live_calibration] for {}'.format(fn_short))
    else:
        # Compute the intrapixel calibration.
        pg, nobs, amp = f.read_intrapix()
        idx1, idx2 = pg.radec2idx(ha, dec)
        ipx_x = amp[idx1, idx2, 0]*np.sin(2*np.pi*curves['x']) + \
            amp[idx1, idx2, 1]*np.cos(2*np.pi*curves['x'])
        ipx_y = amp[idx1, idx2, 2]*np.sin(2*np.pi*curves['y']) + \
            amp[idx1, idx2, 3]*np.cos(2*np.pi*curves['y'])
        sysmag = sysmag + ipx_x + ipx_y
        cflag = np.where(nobs[idx1, idx2] < 25, cflag+4, cflag)


    # Get indices for computing the cloud calibration.
    lstmin = np.amin(station['lstseq'])
    lstmax = np.amax(station['lstseq'])
    lstlen = lstmax - lstmin + 1
    lstidx = curves['lstseq'] - lstmin
    skyidx = io.read_skyidx(curves['ascc'])

    # set sigma_cal to sysmagerr (=zp0err) if [use_zps] is True
    if use_zps:
        sigma_cal = sysmagerr

    # Compute the cloud calibrations.
    mask = (curves['pflag'] == 0) & (
        curves['aflag'] == 0) & np.isfinite(sysmag) & (~incl)
    #nobs, clouds, sigma, std_w = calibrate_clouds(lstidx[mask], skyidx[mask],
    nobs, clouds, sigma = calibrate_clouds(lstidx[mask], skyidx[mask],
                                           (mag - vmag - sysmag)[mask],
                                           (emag**2 + sigma_cal**2)[mask],
                                           lstlen)

    sysmag = sysmag + clouds[lstidx, skyidx]
    cflag = np.where(nobs[lstidx, skyidx] < 25, cflag+8, cflag)
    cflag = np.where(sigma[lstidx, skyidx] > .05, cflag+16, cflag)
    cflag = np.where(np.isnan(sysmag), cflag+1, cflag)

    # Add the temporary calibration to the lightcurves.
    curves['tmpmag0'] = mag - sysmag
    curves['tmpemag0'] = emag
    curves['cflag'] = cflag

    # nobs, clouds and sigma are 2-dimensional arrays with indices
    # [lstidx, skyidx]; if length of zero axis is one, remove it using
    # np.squeeze
    clouds = np.squeeze(clouds, axis=0)
    sigma = np.squeeze(sigma, axis=0)
    #std_w = np.squeeze(std_w, axis=0)
    nobs = np.squeeze(nobs, axis=0)


    # this part was added later; only execute if camid and header are
    # provided for backward compatibility
    if camid is not None and header is not None:

        # save the live calibration
        # -------------------------

        # add extension to zps cloud files
        if use_zps:
            ext = '_zps'
        else:
            ext = ''


        # in clouds folder with prefix clouds
        fn_clouds_hdf5 = 'clouds{:08d}{}{}.hdf5'.format(station[0]['lstseq'],
                                                        camid, ext)
        fn_clouds_hdf5 = os.path.join(dirtree['clouds'], fn_clouds_hdf5)

        jd = header['JD']
        # write hdf5 file
        io.write_clouds(fn_clouds_hdf5, nobs, clouds, sigma, #std_w,
                        lstmin, lstmax, lstlen, jd, header=header)
        # write fits table
        fn_clouds_fits = fn_clouds_hdf5.replace('.hdf5', '.fits')
        io.write_clouds(fn_clouds_fits, nobs, clouds, sigma, #std_w,
                        lstmin, lstmax, lstlen, jd, header=header)


        t3 = time.time()
        fn_clouds_fits = misc.gzip (fn_clouds_fits)
        log.info ('time to gzip cloud fits table: {:.3f}'.format(time.time()-t3))


        # send info to control computer
        # -----------------------------
        if socket2control:

            if os.path.exists(fn_clouds_fits):

                try:
                    log.info ('sending socket message to control computer '
                              'related to {}'.format(fn_clouds_fits))

                    with zmq.Context() as ctx:
                        socket = ctx.socket(zmq.PUB)
                        address = 'tcp://{}:{}'.format(cfg.control_ip,
                                                       cfg.control_port)
                        socket.connect(address)
                        socket.send_multipart([b'cloud',
                                               str.encode(fn_clouds_fits)])
                        socket.disconnect(address)
                        socket.close()

                except Exception as e:
                    log.exception ('socket message to control computer failed '
                                   'for {} with exception: {}'
                                   .format(fn_clouds_fits, e))

            else:
                log.error ('{} not found; not sent to control computer'
                           .format(fn_clouds_fits))



    return curves, nobs, clouds, sigma #, std_w



def binned_image(stack, station, header, astro):
    """ Create a binned image. """

    nimages = len(stack)

    # Choose a reference image.
    mid_idx = nimages//2  # Central image for uneven number of images.

    # Get the WCS parameters and initialize the stacker.
    wcspars = astro.get_wcspars()
    stacker = stacking.Stacker(wcspars)

    # Stack the images together.
    for i in range(nimages):
        stacker.add_image(station[i]['lst'], stack[i],
                          station[i]['exptime'], station[mid_idx]['lst'])

    # Get the resulting image and a header containing the WCS transformations.
    binimage, binheader = stacker.get_image(station[mid_idx]['lst'])

    # Add additional information to the header.
    binheader['NIMAGES'] = (nimages, 'Number of images combined.')
    binheader['SITE-OBS'] = (header['site-obs'], 'Observation site')
    binheader['CAMERA'] = (header['camera'], 'Camera name on site')
    binheader['IMAGETYP'] = ('BINIMAGE', 'Type of image')
    binheader['EXPTIME'] = (np.mean(station['exptime']),
                            'Exposure time in seconds')
    binheader['CCDTEMP'] = (np.mean(station['ccdtemp']),
                            'Average CCD temperature (C)')
    binheader['LSTSEQ'] = (station[mid_idx]['lstseq'],
                           'Exposure num of the reference frame')
    binheader['LST'] = (station[mid_idx]['lst'],
                        'Sidereal time of the reference frame')
    binheader['JD'] = (station[mid_idx]['jd'], 'JD of the reference frame')
    binheader['LSTMID'] = (np.mean(station['lst']), 'Average LST in hours')
    binheader['JDMID'] = (np.mean(station['jd']), 'Average JD')

    return binimage, binheader


def postage_stamps(curves, stack, station, nhalf=cfg.nhalf):

    npoints = len(curves)

    idx = np.searchsorted(station['lstseq'], curves['lstseq'])
    stamps = np.zeros((npoints, 2*nhalf+1, 2*nhalf+1), dtype='float32')

    for i in range(npoints):

        x, y = curves['x'][i], curves['y'][i]
        xi, yi = np.around(x), np.around(y)

        lx = int(xi - nhalf)
        ux = int(xi + nhalf + 1)
        ly = int(yi - nhalf)
        uy = int(yi + nhalf + 1)

        stamps[i] = stack[idx[i], ly:uy, lx:ux]

    return stamps


################################################################################

def extract_sources(fits_ima, header, psfex_mode=False):

    """run source extractor on input image [fits_ima] and its
       [header]; the resulting measurements are saved in output
       catalog [fits_cat] and the background-subtracted image is saved
       in [fits_red] - their names are returned
    """

    t1 = time.time()
    log.info('executing extract_sources ...')

    base_ima = fits_ima.split('.fits')[0]
    fits_cat = '{}_cat.fits'.format(base_ima)
    fits_red = '{}_red.fits'.format(base_ima)


    # define cmd dictionary
    #cmd_dict = collections.OrderedDict()
    cmd_dict = {}
    cmd_dict['source-extractor'] = fits_ima
    cmd_dict['-c'] = cfg.sex_config
    cmd_dict['-CATALOG_TYPE'] = 'FITS_1.0'
    cmd_dict['-CATALOG_NAME'] = fits_cat
    cmd_dict['-PARAMETERS_NAME'] = cfg.sex_params
    cmd_dict['-FILTER_NAME'] = cfg.sex_conv
    cmd_dict['-STARNNW_NAME'] = cfg.sex_nnw
    cmd_dict['-VERBOSE_TYPE'] = 'QUIET'

    # only if sky needs to be subtracted
    if cfg.subtract_skybkg:
        log.info ('subtracting sky determined by SExtractor for {}'
                  .format(fits_ima.split('/')[-1].split('.fits')[0]))
        cmd_dict['-CHECKIMAGE_TYPE'] = '-BACKGROUND'
        cmd_dict['-CHECKIMAGE_NAME'] = fits_red


    # for PSFEx to work
    if psfex_mode:

        # PSFEx requires a LDAC catalogue
        fits_cat_ldac = '{}_cat_ldac.fits'.format(base_ima)
        cmd_dict['-CATALOG_TYPE'] = 'FITS_LDAC'
        cmd_dict['-CATALOG_NAME'] = fits_cat_ldac

        # update size of VIGNET in source extractor configuration file
        # with the output parameters
        size_vignet = 15
        # write vignet_size to header
        header['S-VIGNET'] = (size_vignet, '[pix] size square VIGNET used '
                              'in SExtractor')

        # create updated parameters file starting from original file
        sex_params_vignet = ('{}_vignet.params'
                             .format(cfg.sex_params.split('.params')[0]))
        shutil.copy2(cfg.sex_params, sex_params_vignet)

        # append a line with the VIGNET size
        size_vignet_str = str((size_vignet, size_vignet))
        with open(sex_params_vignet, 'a') as myfile:
            myfile.write('VIGNET{}\n'.format(size_vignet_str))

        # use the new file
        cmd_dict['-PARAMETERS_NAME'] = sex_params_vignet



    # convert cmd_dict to list and execute command
    cmd_list = list(chain.from_iterable(list(cmd_dict.items())))
    cmd_str = ' '.join(cmd_list)
    log.info('SExtractor command executed:\n{}'.format(cmd_str))
    status_SE = misc.run_cmd (cmd_str, shell=True)

    if status_SE != 0:
        msg = 'Source extractor failed with exit code {}'.format(status_SE)
        log.exception(msg)
        raise Exception(msg)


    if not cfg.subtract_skybkg:
        # sky background was not subtracted, so fits_red was not
        # created (see CHECKIMAGE_NAME above); move fits_ima to
        # fits_red
        shutil.move(fits_ima, fits_red)



    # run PSFEx
    if psfex_mode:

        # profile sub-pixel sampling in pixels
        psf_samp = 0.33
        # [size_vignet] defined above and [psf_samp] define
        # the PSF configuration size
        psf_size_config = int(np.ceil(size_vignet / psf_samp))
        # make sure it is odd
        if psf_size_config % 2 == 0:
            psf_size_config += 1

        # degree of polynomial to fit
        poldeg = 2
        # minimum required S/N ratio to include PSF stars
        snr_min = 20
        # name output catalog with PSF star properties
        psfexcat = '{}_psfex.cat'.format(base_ima)
        # reshaping (4008,2672) image into (24,16) snap image to show
        # PSFs on 167x167 pixel images
        snap = '24,16'

        # run PSFEx
        run_psfex(fits_cat_ldac, cfg.psfex_config, psfexcat, poldeg, snap,
                  snr_min, psf_samp, psf_size_config, make_plots=True)

        # also create a regular fits table without the VIGNET column
        ldac2fits (fits_cat_ldac, fits_cat)



    # record number of objects detected in the header
    with fits.open(fits_cat) as hdulist:
        header_cat = hdulist[-1].header

    nobjects = header_cat['NAXIS2']
    header['NOBJECTS'] = (nobjects, '# objects detected by source-extractor')
    log.info ('{} objects detected by source-extractor'.format(nobjects))


    # also record if sky background was subtracted
    header['BKG_SUB'] = (cfg.subtract_skybkg, 'source extractor sky background '
                         'subtracted?')


    misc.log_timing_memory (t1, label='extract_sources')

    return fits_cat, fits_red


################################################################################

def run_psfex (cat_in, file_config, cat_out, poldeg, snap, snr_min, psf_samp,
               psf_size_config, nthreads=1, make_plots=False):

    """Function that runs PSFEx on [cat_in] (which is a SExtractor
       output catalog in FITS_LDAC format) using the configuration
       file [file_config]

    """


    t1 = time.time()
    log.info('executing run_psfex ...')

    # base name
    base = cat_in.split('_cat_ldac.fits')[0]


    # run psfex from the unix command line
    psf_size_config_str = '{},{}'.format(psf_size_config, psf_size_config)
    cmd_dict = {}
    cmd_dict['psfex'] = cat_in
    cmd_dict['-c'] = file_config
    cmd_dict['-OUTCAT_NAME'] = cat_out
    cmd_dict['-PSF_SIZE'] = psf_size_config_str
    cmd_dict['-PSF_SAMPLING'] = str(psf_samp)
    cmd_dict['-SAMPLE_MINSN'] = str(snr_min)
    cmd_dict['-PSFVAR_NSNAP'] = snap
    cmd_dict['-PSFVAR_DEGREES'] = str(poldeg)
    cmd_dict['-NTHREADS'] = str(nthreads)

    cmd_dict['-SAMPLE_MAXELLIP'] = str(1.0)
    cmd_dict['-SAMPLE_AUTOSELECT'] = 'Y'
    cmd_dict['-SAMPLEVAR_TYPE'] = 'NONE'
    cmd_dict['-SAMPLE_FWHMRANGE'] = '0.1,100'
    cmd_dict['-SAMPLE_VARIABILITY'] = str(10.0)

    if make_plots:

        cmd_dict['-CHECKPLOT_TYPE'] = 'FWHM,ELLIPTICITY,COUNTS,COUNT_FRACTION,' \
            'CHI2,RESIDUALS'
        cmd_dict['-CHECKPLOT_DEV'] = 'PS'
        cmd_dict['-CHECKPLOT_ANTIALIAS'] = 'N'
        cmd_dict['-CHECKPLOT_NAME'] = 'psfex_fwhm,psfex_ellip,psfex_counts,' \
            'psfex_countfrac,psfex_chi2,psfex_resi'
        cmd_dict['-CHECKIMAGE_TYPE'] = 'CHI,PROTOTYPES,SAMPLES,RESIDUALS,' \
            'SNAPSHOTS,BASIS,MOFFAT,-MOFFAT,-SYMMETRICAL'
        cmd_dict['-CHECKIMAGE_NAME'] = 'psfex_chi,psfex_proto,psfex_samp,' \
            'psfex_resi,psfex_snap,psfex_basis,psfex_moffat,psfex_minmoffat,' \
            'psfex_minsymmetrical'



    # convert cmd_dict to list and execute command
    cmd_list = list(chain.from_iterable(list(cmd_dict.items())))
    cmd_str = ' '.join(cmd_list)
    log.info('PSFEx command executed:\n{}'.format(cmd_str))
    status_PSFEx = misc.run_cmd (cmd_str, shell=True)

    if status_PSFEx != 0:
        msg = 'PSFEx failed with exit code {}'.format(status_PSFEx)
        log.exception(msg)
        raise Exception(msg)


    # standard output of PSFEx is .psf; change this to _psf.fits
    psf_in = cat_in.replace('.fits', '.psf')
    psf_out = '{}_psf.fits'.format(base)
    os.rename (psf_in, psf_out)


    # if zero stars were found by PSFEx, raise exception
    with fits.open(psf_out) as hdulist:
        header_psf = hdulist[-1].header

    if header_psf['ACCEPTED']==0:
        msg = ('no appropriate source found by PSFEx in [run_psfex] for catalog '
               '{}'.format(cat_in))
        log.exception (msg)
        raise Exception (msg)



    # record the PSFEx output check images defined above, into
    # extensions of a single fits file
    if make_plots:

        hdulist = fits.HDUList()
        hdulist.append(fits.PrimaryHDU())

        cwd = os.getcwd()
        file_list = glob.glob('{}/psfex*'.format(cwd))
        for name in file_list:

            short = name.split('/')[-1]
            prefix = '_'.join(short.split('_')[0:2])

            if False and 'fits' in name:
                # read image data and header
                with fits.open(name) as hdulist:
                    data = hdulist[-1].data
                    header = hdulist[-1].header

                # add extension to hdulist with extname=prefix
                hdulist.append(fits.ImageHDU(data=data, header=header,
                                             name=prefix))
                # remove individual image
                #os.remove(name)
                log.info ('added fits image {} to multi-extension fits file'
                          .format(name))

            else:
                # if not a fits file, rename it
                dir_out, __ = os.path.split(psf_out)
                name_new = short.replace('ldac', prefix)
                name_new = name_new.replace('{}_'.format(prefix),'')
                name_new = '{}/{}'.format(dir_out, name_new)
                log.info ('name: {}, name_new: {}'.format(name, name_new))
                os.rename (name, name_new)


        # write image
        #fits_output = '{}_psf_checkimages.fits'.format(base)
        #hdulist.writeto(fits_output, overwrite=True)



    misc.log_timing_memory (t1, label='run_psfex')

    return


################################################################################

def ldac2fits (cat_ldac, cat_fits):

    """This function converts the LDAC binary FITS table from SExtractor
    to a common binary FITS table (that can be read by Astrometry.net) """

    t1 = time.time()
    log.info('executing ldac2fits ...')

    # read input table and write out primary header and 2nd extension
    columns = []
    with fits.open(cat_ldac, memmap=True) as hdulist:

        # delete VIGNET column
        hdulist[2].data = drop_fields(hdulist[2].data, 'VIGNET')

        # and write regular fits file
        hdulist_new = fits.HDUList([hdulist[0], hdulist[2]])
        hdulist_new.writeto(cat_fits, overwrite=True)
        hdulist_new.close()


    misc.log_timing_memory (t1, label='ldac2fits')

    return


################################################################################

def try_reduce_science_single (fits_raw, camid, leading,
                               lstseq_leading, darktable, cat,
                               siteinfo, systable, dirtree, zpstable,
                               astro_done_list, socket2control, lock,
                               mode_rereduce=False, nocal=False):

    try:
        fits_red = reduce_science_single (fits_raw, camid, leading,
                                          lstseq_leading, darktable, cat,
                                          siteinfo, systable, dirtree, zpstable,
                                          astro_done_list, socket2control, lock,
                                          mode_rereduce, nocal)
        return fits_red

    except Exception as e:
        log.exception ('exception was raised in [reduce_science_single] while '
                       'processing {}; returning None for fits_red\n{}'
                       .format(fits_raw, e))
        return None


################################################################################

def reduce_science_single (fits_raw, camid, leading, lstseq_leading, darktable,
                           cat, siteinfo, systable, dirtree, zpstable,
                           astro_done_list, socket2control, lock,
                           mode_rereduce=False, nocal=False):

    """function to reduce single raw DREAM science frame (full path
    required), allowing for concurrency, i.e. for multiple CPUs to
    simultaneously process difference frames. In case the image is a
    leading image, its astrometry will be solved for using
    Astrometry.net; if it is not, the astrometric solution from the
    leading image will be projected to it.

    """

    # for timing
    t1 = time.time()


    # infer LSTSEQ from fits_raw
    lstseq = int(fits_raw.split('/')[-1].split(camid)[0])


    # logfile
    #logfile = os.path.join(dirtree['log'], 'rawdir_monitor.log')
    logfile = '{}/{}{}.log'.format(cfg.tmpdir, lstseq, camid)
    # attach logfile to logging
    misc.attach_log(log, logfile)
    log.info ('created logfile: {}'.format(logfile))


    log.info ('processing {}'.format(fits_raw))
    fits_short = fits_raw.split('/')[-1].split('.fits')[0]


    # Read the file
    # -------------
    step_label = 'reading raw fits'
    log.info ('{} {}'.format(step_label, fits_raw))

    # do 3 attempts to read image, in case it takes time to arrive
    for nread in range(3):
        try:
            # read image and header
            with fits.open(fits_raw) as hdulist:
                data_raw = hdulist[-1].data
                header = hdulist[-1].header
        except:
            if nread < 2:
                # sleep for a second
                time.sleep(1)
            else:
                # 3 attempts were made; give up
                log.error ('failed to read image {}'.format(fits_raw))
                misc.close_log(log, logfile)
                return
        else:
            # reading went fine; break out of for loop
            break


    # determine lstseq; from either header or from fits_raw
    #lstseq = header['LSTSEQ']


    # leading and lstseq_leading were initially determined inside this
    # function, but are now input parameters
    log.info ('{} leading?: {}'.format(fits_short, leading))
    log.info ('{} lstseq_leading: {}'.format(fits_short, lstseq_leading))

    # add some info to header
    header['LEADING'] = (leading, 'leading image?')
    header['LEADLSEQ'] = (lstseq_leading, 'LSTSEQ of leading image')


    # short name leading image
    fits_leading_short = '{}{}'.format(lstseq_leading, camid)


    # log timing/memory
    t2 = log_tm (t1, t1, fits_short, step_label)


    # preprocess
    # ----------
    step_label = 'preprocessing'
    log.info ('{} {}'.format(step_label, fits_raw))

    # assume that the dark frames have already been combined; could
    # trigger the master dark preparation by the 1st science image of
    # the night, but then the 1st image would take rather long
    #darktable = cfg.darktable[camid]
    # using original preprocess function, which is meant to process a
    # list of images
    data_pp, station = preprocess(np.array([data_raw]), [header], darktable)

    # preprocess returns numpy array with 3 axes; squeeze out the
    # first one
    data_pp = np.squeeze(data_pp)


    # save pre-processed image in tmp folder
    fits_ima = '{}/{}'.format(cfg.tmpdir, header['FILENAME'])
    fits.writeto (fits_ima, data_pp, header, overwrite=True)


    # log timing/memory
    t2 = log_tm (t1, t2, fits_short, step_label)


    # extract sources and sky background subtraction
    # ----------------------------------------------
    step_label = 'extracting sources'
    log.info ('{} {}'.format(step_label, fits_ima))

    fits_cat, fits_red = extract_sources (fits_ima, header, psfex_mode=False)
    base_ima = fits_ima.split('.fits')[0]
    #fits_cat = '{}_cat.fits'.format(base_ima)
    #fits_red = '{}_red.fits'.format(base_ima)

    # remove fits_ima (=dark-corrected image in tmp folder)
    if not (mode_rereduce and cfg.keep_tmp) and os.path.exists(fits_ima):
        os.remove(fits_ima)


    # remove these header scaling parameters, otherwise they are added
    # to the reduced image header below when updating it
    for key in ['BZERO', 'BSCALE']:
        if key in header:
            del header[key]


    # log timing/memory
    t2 = log_tm (t1, t2, fits_short, step_label)


    # astrometry
    # ----------
    step_label = 'astrometric calibration'
    log.info ('{} {}'.format(step_label, fits_ima))


    # infer starcat ra, dec and mag for selected stars
    select = ((cat['vmag'] <= cfg.maglim_astro[1]) &
              (cat['vmag'] > cfg.maglim_astro[0]) &
              (cat['dist8'] > cfg.dist8_lim))
    ra, dec, mag = cat['ra'][select], cat['dec'][select], cat['vmag'][select]


    # plot figures if in rereduce mode and temporary products are kept
    plot = mode_rereduce and cfg.keep_tmp


    # the follower processes need to wait before the lock until the
    # astrometry of their leader is finished
    if not leading:

        leaderless = False

        t3 = time.time()
        tmax = cfg.tmax_leader_wcs
        while lstseq_leading not in astro_done_list:

            time.sleep(0.1)

            if time.time() - t3 > tmax:

                leaderless = True
                log.warn ('{} reached timeout ({}s) in waiting for leading '
                          'image ({}) astrometric solution; continuing as '
                          'leaderless image'
                          .format(fits_short, tmax, fits_leading_short))
                break

        else:
            log.info ('{} waited for {:.3f}s for leading image ({}) astrometric '
                      'solution'.format(fits_short, time.time()-t3,
                                        fits_leading_short))


    # need to put a multiprocessing lock on this WCS-solving block, as
    # two leading images could enter, potentially leading to one
    # thread updating the astromaster inside astrometry.solve_single,
    # while the other attempts to read it; this has occurred in
    # rereduce mode while using more CPUs than cfg.nimages_astro (i.e.
    # where 2 leading images could be processed simulteneously)
    lock.acquire()
    log.info ('entering the multiprocessing lock (leading?: {})'
              .format(leading))


    if leading:

        try:
            # Perform astrometry
            aflag, astrosol, header = astrometry.solve_single (
                data_pp, header, fits_cat, ra, dec, dirtree, plot=plot)

        except:
            # astrometry on leading image has failed; as a last
            # resort, use WCS from from a leading image nearest in UTC
            # from astromaster
            header = astrometry.wcs_from_astromaster (header, use_LST=False)
            log.error ('could not find astrometric solution for {}; using the '
                       'WCS solution from a nearby header in astromaster '
                       'instead'.format(fits_short))

        else:
            # append the leading image lstseq to astro_done_list so
            # that following images can continue; only done if
            # astrometry.solve_single ran without any exception
            astro_done_list.append(lstseq)

        finally:
            # add RA and DEC image center to header and update reduced
            # leading image with WCS header; this update cannot wait until
            # after this if/else block (updating both leading/following
            # images) as that will lead to some following images needing
            # the WCS header info before it is available
            add_radec_center (header)
            with fits.open(fits_red, 'update') as hdulist:
                hdulist[0].header = header




    else:

        if not leaderless:

            # read the header of the leading image
            fits_leading = '{}/{}{}_red.fits'.format(cfg.tmpdir, lstseq_leading,
                                                     camid)
            with fits.open(fits_leading) as hdulist:
                header_leading = hdulist[-1].header


            # extrapolate WCS solution from the leading header
            # N.B.: _red.fits should not be fpacked!
            log.info ('extrapolating the WCS solution from {} to {}'
                      .format(fits_leading, fits_red))


            # infer CRVAL1 value from difference in LST between header_leading
            # and header
            header_leading = astrometry.update_crval (
                header_leading, lst_image=header['LST'],
                obsdate_image=header['UTC-OBS'])


            # update current header with WCS keys from leading header
            for key in astrometry.get_keys_wcs():
                if key in header_leading:
                    header[key] = header_leading[key]

        else:

            # astrometry on leading image has failed; instead pick the
            # WCS from a leading image nearest in UTC from astromaster
            header = astrometry.wcs_from_astromaster (header, use_LST=False)
            log.error ('{} is leaderless; using the WCS solution from a nearby '
                       'header in astromaster instead'.format(fits_short))




        # add accuracy of astrometry to header of following image as
        # well; previously only done for leading image

        # extract source-extractor pixel coordinates from input
        # fits_cat
        table_cat = Table.read(fits_cat)
        xpix = table_cat['XWIN_IMAGE']
        ypix = table_cat['YWIN_IMAGE']
        astrometry.check_wcs (header, xpix, ypix, ra, dec, dirtree)


        # for now, these are parameters are needed below
        aflag = 0
        astrosol = dict()


        # add RA and DEC image center to header and update reduced
        # following image with WCS header
        add_radec_center (header)
        with fits.open(fits_red, 'update') as hdulist:
            hdulist[0].header = header




    # open the lock
    log.info ('exiting the multiprocessing lock (leading?: {})'
              .format(leading))
    lock.release()


    # log timing/memory
    t2 = log_tm (t1, t2, fits_short, step_label)


    # archive raw image with updated header
    # -------------------------------------

    arcdir = dirtree['rawarchive']

    # in rereduce mode, arcdir may be None
    if arcdir is None:
        log.warn ('arcdir is None; not raw-archiving {}'.format(fits_short))

    else:

        fits_rawarchive = os.path.join(arcdir, header['FILENAME'])

        # check if image to be written already exists; note that
        # header[FILENAME] has .fits extension: also check for
        # possible compressed versions
        if np.any([os.path.exists('{}{}'.format(fits_rawarchive, ext))
                   for ext in ['', '.fz', '.gz']]):
            log.warn ('{} already exists; not archiving {}'
                      .format(fits_rawarchive, fits_short))

        else:

            try:
                step_label = 'archiving raw fits including astrometry'
                log.info ('{} {} with updated header'.format(step_label,
                                                             fits_short))

                # save raw image including the astrometry header to
                # rawarchive folder; this requires a small update to
                # CRPIX1 and CRPIX2 as the raw image contains the
                # overscan strips
                header_edited = header.copy()
                dy = range(100)[cfg.ccd_active[0]][0]
                dx = range(100)[cfg.ccd_active[1]][0]
                header_edited['CRPIX1'] += dx
                header_edited['CRPIX2'] += dy


                # delete some keys irrelevant for raw image
                keys_del = ['LEADING', 'LEADLSEQ', 'MDARK', 'NOBJECTS',
                            'BKG_SUB']
                for key in keys_del:
                    if key in header_edited:
                        del header_edited[key]


                t3 = time.time()
                fits.writeto (fits_rawarchive, data_raw, header_edited,
                              overwrite=True)
                log.info ('time to archive raw image {}: {:.3f}s'
                          .format(fits_rawarchive, time.time()-t3))

            except Exception as e:
                log.exception ('exception occurred when {} for {}: {}'
                               .format(step_label, fits_short, e))

            else:
                # if archiving went fine, delete the original raw image
                # when not in rereduce mode
                if not mode_rereduce:
                    log.warn ('removing {}'.format(fits_raw))
                    os.remove (fits_raw)



    # Add sun and moon information.
    station = sunmoon2station(station, siteinfo, [header])


    # log timing/memory
    t2 = log_tm (t1, t2, fits_short, step_label)


    # Perform the fast photometry
    # ---------------------------
    step_label = 'photometry'
    log.info ('{} {}'.format(step_label, fits_ima))

    # read reduced data; this is also done in [create_diff], so could
    # avoid doing so twice
    with fits.open(fits_red, memmap=True) as hdulist:
        data_red = hdulist[-1].data


    fast_curves = lightcurves ([data_red], [header], station, cat,
                               cfg.aper_fast, cfg.skyrad, cfg.maglim_fast)
    fast_curves['aflag'] = aflag


    # log timing/memory
    t2 = log_tm (t1, t2, fits_short, step_label)



    # Perform the live calibration
    # ----------------------------
    if nocal:
        log.warn('nocal is True: skipping the live calibration')
    else:

        # Perform the live calibration.
        step_label = 'live calibration'
        log.info ('{} {}'.format(step_label, fits_ima))


        # infer sysfile name from systable
        sysfile = io.get_file(systable)


        # original sys live calibration
        if sysfile is None or not os.path.exists(sysfile):
            log.warn ('sysfile {} not found: skipping the original live '
                      'calibration'.format(sysfile))
        else:
            fast_curves, nobs, clouds, sigma = live_calibration(
                station, fast_curves, cat, sysfile, dirtree, camid=camid,
                header=header, socket2control=socket2control)


        # alternative individual zeropoints calibration
        #fits_zps = '{}/zeropoints_{}.fits'.format(cfg.confdir, camid)
        # infer zeropoints fits table from zpstable
        fits_zps = io.get_file(zpstable)

        if fits_zps is None or not os.path.exists(fits_zps):
            log.warn ('zeropoints file {} not found; skipping the zeropoints '
                      'live calibration'.format(fits_zp))
        else:

            # N.B.: if zeropoints calibration file does not contain
            # all ascc stars from fast_curves, the latter will be
            # masked, so using fast_curves (instead of
            # fast_curves_zps) further down below; note that
            # live_calibration otherwise only updates the fast_curves
            # fields 'tmpmag0', 'tmpemag0' and 'cflag'
            fast_curves_zps = fast_curves.copy()
            fast_curves_zps, nobs_zps, clouds_zps, sigma_zps = live_calibration(
                station, fast_curves_zps, cat, sysfile, dirtree, use_zps=True,
                fits_zps=fits_zps, camid=camid, header=header,
                socket2control=socket2control)



    # log timing/memory
    t2 = log_tm (t1, t2, fits_short, step_label)


    # Save the lightcurves
    # --------------------
    step_label = 'saving fast photometry'

    fn_fastphot = 'tmp_fast{:08d}.hdf5'.format(station[0]['lstseq'])
    fn_fastphot = os.path.join(dirtree['tmp'], fn_fastphot)
    log.info ('{} {} to {}'.format(step_label, fits_ima, fn_fastphot))


    f = io.TmpFile(fn_fastphot)
    f.add_header(header, siteinfo, cfg.aper_fast, cfg.skyrad)
    f.add_station(station)
    f.add_lightcurves(fast_curves, cat)
    f.add_astrometry(astrosol)

    # log timing/memory
    t2 = log_tm (t1, t2, fits_short, step_label)


    # Create postage stamps
    # ---------------------
    step_label = 'creating postage stamps'
    log.info ('{} {}'.format(step_label, fits_ima))

    #targets = io.read_targets(targets)
    targets = cfg.targets
    for key in list(targets.keys()):

        log.info('Trying to make postage stamps for target group {}'.format(key))

        mask = np.in1d(fast_curves['ascc'], targets[key])

        if np.any(mask):

            stamp_curves = fast_curves[mask]
            stamps = postage_stamps(stamp_curves, stack, station)

            fn_stamps = '{}_{:08d}{}.hdf5'.format(
                key, station[0]['lstseq'], camid)
            fn_stamps = os.path.join(dirtree['targets'], fn_stamps)

            log.info(
                'Saving postage stamps for target group {} to {}'
                .format(key, fn_stamps))

            io.write_stamps(fn_stamps, stamps, stamp_curves, station, cat)

        else:
            log.info('No stars present for target group {}'.format(key))



    # log timing/memory
    t2 = log_tm (t1, t2, fits_short, step_label)


    # set to True if difference image making needs to be skipped
    if False:
        misc.close_log(log, logfile)
        return fits_red


    # difference image
    # ----------------
    step_label = 'creating difference image'
    log.info ('{} {}'.format(step_label, fits_ima))


    # determine previous image, determined from the raw images
    rawdir, __ = os.path.split(fits_raw)
    fits_red_prev, dseq = get_fits_prev (rawdir, lstseq, camid)
    log.info ('fits_red_prev: {}, dseq: {}'.format(fits_red_prev, dseq))

    # do subtraction only if previous image is not too long ago
    if dseq > cfg.imdiff_dseq_max:

        log.warn ('previous image too long ago to prepare difference image '
                  'for {}'.format(fits_red))

    else:

        # in rereduce mode, the previous image might not be ready
        # quite yet; keep checking it has finished for a total of 5s,
        # if not, no difference image will be made
        t3 = time.time()
        for i in range(0,10):

            try:
                with fits.open(fits_red_prev) as hdulist:
                    header_prev = hdulist[-1].header
            except:
                pass
            else:
                if 'CRVAL1' in header_prev and 'CRVAL2' in header_prev:

                    # create difference image, writing it directly to
                    # the dirtree['diff'] folder
                    fits_tmp = fits_red.split('/')[-1].replace('.fits',
                                                               '_diff.fits')
                    fits_red_diff = os.path.join(dirtree['diff'], fits_tmp)
                    create_diff (fits_red, fits_red_prev, fits_red_diff, header,
                                 mode_rereduce)

                    # archive difference image - can be skipped now that the
                    # difference image is written to its final destination
                    #if os.path.exists(fits_red_diff):
                    #    log.info ('archiving the difference image')
                    #    io.archive_files(fits_red_diff, dirtree['diff'])

                    # break out of waiting loop
                    break

            time.sleep(0.5)


        log.info ('{} waited for {:.3f}s for previous image {} to be ready to '
                  'subtract'.format(fits_short, time.time()-t3, fits_red_prev))



    # to save space in tmp folder, delete reduced image from some time
    # ago, but needs to be max(ncpus, nimages_astro) epochs before the
    # current LSTSEQ
    if False and not (mode_rereduce and cfg.keep_tmp):
        lstseq_remove = lstseq - max(cfg.ncpus, cfg.nimages_astro)
        fits_red_remove = fits_red.replace(str(lstseq), str(lstseq_remove))
        if os.path.exists(fits_red_remove):
            log.info ('removing older reduced file {}'.format(fits_red_remove))
            os.remove(fits_red_remove)



    # log timing/memory
    t2 = log_tm (t1, t2, fits_short, step_label)
    misc.log_timing_memory (t1, label='reduce_science_single on {} at end'
                            .format(fits_short))


    # detach logfile
    misc.close_log(log, logfile)


    # archive logfile
    io.archive_files(logfile, dirtree['logs'])


    return fits_red


################################################################################

def log_tm (t1, t2, filename, step_label):

    misc.log_timing_memory (t2, label='{} step for {}'
                            .format(step_label, filename))
    misc.log_timing_memory (t1, label='reduce_science_single on {} after {}'
                            .format(filename, step_label))
    t2 = time.time()

    return t2


################################################################################

def create_diff (fits_red, fits_red_prev, fits_red_diff, header, mode_rereduce):

    log.info ('executing create_diff ...')

    t1 = time.time()

    # remapping fits_red_prev to fits_red
    fits_red_prev_remap = fits_red_prev.replace('.fits', '_remap.fits')
    result = run_remap(fits_red, fits_red_prev, fits_red_prev_remap,
                       (cfg.ysize, cfg.xsize),
                       gain=1, config=cfg.swarp_config,
                       resampling_type='LANCZOS3', dtype='float32',
                       resample_dir=cfg.tmpdir, nthreads=1)

    # determine difference image
    with fits.open(fits_red, memmap=True) as hdulist:
        data_red = hdulist[-1].data

    with fits.open(fits_red_prev_remap, memmap=True) as hdulist:
        data_red_prev_remap = hdulist[-1].data

    # save difference image
    t3 = time.time()
    fits.writeto (fits_red_diff, data_red-data_red_prev_remap,
                  header=header, overwrite=True)
    log.info ('time to archive difference image {}: {:.3f}s'
              .format(fits_red_diff, time.time()-t3))


    if not (mode_rereduce and cfg.keep_tmp):

        # delete remapped and weight images
        os.remove(fits_red_prev_remap)
        os.remove(fits_red_prev_remap.replace('.fits', '_weight.fits'))



    misc.log_timing_memory (t1, label='create_diff')


    # return the name of the difference image
    return fits_red_diff


################################################################################

def get_fits_prev (rawdir, lstseq, camid):

    dseq = 1
    while True:

        fits_raw_prev = '{}/{}{}.fits'.format(rawdir, lstseq-dseq, camid)
        if (os.path.exists(fits_raw_prev) or
            os.path.exists('{}.gz'.format(fits_raw_prev)) or
            os.path.exists('{}.fz'.format(fits_raw_prev)) or
            dseq > 13500):
            break
        else:
            dseq += 1

    if dseq > 10:
        log.warn ('previous image used for difference image was taken {:.1f}s'
                  ' before the current image'.format(dseq*6.4))

    # now define previous reduced image
    fits_red_prev = '{}/{}{}_red.fits'.format(cfg.tmpdir, lstseq-dseq, camid)

    return fits_red_prev, dseq


################################################################################

def run_remap (image_in, image_ref, image_out, image_out_shape, gain=1,
               config=None, resample='Y', resampling_type='LANCZOS3',
               projection_err=0.01, mask=None, header_only='N',
               resample_suffix='_resamp.fits', resample_dir='.', dtype='float32',
               value_edge=0, timing=True, nthreads=0, oversampling=0):

    """Function that remaps [image_ref] onto the coordinate grid of
       [image_in] and saves the resulting image in [image_out] with
       size [image_size].
    """

    t1 = time.time()
    log.info('executing run_remap (swarp) ...')

    with fits.open(image_ref) as hdulist:
        header_ref = hdulist[-1].header

    with fits.open(image_in) as hdulist:
        header_in = hdulist[-1].header

    # create .head file with header info from [image_in]
    header_out = header_in

    # delete some others
    for key in ['WCSAXES', 'NAXIS1', 'NAXIS2']:
        if key in header_out:
            del header_out[key]
    # write to .head file
    with open(image_out.replace('.fits','.head'),'w') as newrefhdr:
        for card in header_out.cards:
            newrefhdr.write('{}\n'.format(card))

    size_str = '{},{}'.format(image_out_shape[1], image_out_shape[0])
    cmd = ['swarp', image_ref, '-c', config, '-IMAGEOUT_NAME', image_out,
           '-IMAGE_SIZE', size_str, '-GAIN_DEFAULT', str(gain),
           '-RESAMPLE', resample,
           '-RESAMPLE_DIR', resample_dir,
           '-RESAMPLING_TYPE', resampling_type,
           '-OVERSAMPLING', str(oversampling),
           '-PROJECTION_ERR', str(projection_err),
           '-NTHREADS', str(nthreads),
           '-VERBOSE_TYPE', 'QUIET',
           # weights are not used, but if not defining this parameter,
           # the image with default name coadd.weight.fits is saved in
           # current working directory
           '-WEIGHTOUT_NAME', image_out.replace('.fits', '_weight.fits')]



    # log cmd executed
    cmd_str = ' '.join(cmd)
    log.info('swarp command executed:\n{}'.format(cmd_str))
    status_swarp = misc.run_cmd (cmd)


    if status_swarp != 0:
        msg = ('swarp failed with exit code {} on image {}'
               .format(status_swarp, image_in))
        log.error(msg)
        return


    misc.log_timing_memory (t1, label='run_remap (swarp)')


    return


################################################################################

def add_radec_center (header):

    # add RA-CNTR and DEC-CNTR to header
    wcs = WCS(header)
    ra_center, dec_center = wcs.all_pix2world(cfg.xsize/2+0.5,
                                              cfg.ysize/2+0.5, 1)
    log.info('ra_center: {:.4f}, dec_center: {:.4f}'.format(ra_center,
                                                            dec_center))
    header['RA_CNTR'] = (float(ra_center),
                         '[deg] RA (ICRS) at image center (astrom.net)')
    header['DEC_CNTR'] = (float(dec_center),
                          '[deg] DEC (ICRS) at image center (astrom.net)')



################################################################################

def main():

    return


if __name__ == '__main__':

    main()
