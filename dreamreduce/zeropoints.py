
import os
import sys
import argparse
import glob
import itertools
import datetime

# set up log
import logging
import time
logfmt = ('%(asctime)s.%(msecs)03d [%(levelname)s, %(process)s] %(message)s '
          '[%(funcName)s, line %(lineno)d]')
datefmt = '%Y-%m-%dT%H:%M:%S'
logging.basicConfig(level='INFO', format=logfmt, datefmt=datefmt)
logFormatter = logging.Formatter(logfmt, datefmt)
logging.Formatter.converter = time.gmtime #convert time in logger to UTC
log = logging.getLogger()
#log.propagate = False

import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields

import healpy as hp
import h5py

from astropy.io import fits
from astropy.table import Table, vstack, join
from astropy.time import Time
from astropy.stats import sigma_clip, mad_std

from scipy import stats

import ephem

from dreamreduce import rawdir_monitor as rm
from dreamreduce import configuration as cfg
from dreamreduce import io
from dreamreduce import misc
from dreamreduce import astrometry

from fitsio import FITS

plot = False

if plot:
    import matplotlib
    import matplotlib.pyplot as plt
    matplotlib.use('MacOSX')
    import matplotlib.backends.backend_pdf
    import matplotlib.dates as mdates

    import pandas as pd


################################################################################

def create_zeropoints_comb (hdf5_cfast_lcs, fits_out, nsigma_low=2.5,
                            nsigma_high=3, ncpus=cfg.ncpus):


    """function to create a fits table with weighted mean zeropoints
    and the corresponding errors of individual stars. The photometry
    is taken from the combined photometry input file [hdf5_cfast_lcs],
    which typically consists of several nights of data. The zeropoints
    are saved in the output fits table [fits_out] with the following
    columns:

      ascc   - ID in ASCC input catalog
      zp0    - zeropoint based on aperture 0 flux measurements
      zp0err - corresponding error
      zp1    - zeropoint based on aperture 1 flux measurements
      zp1err - corresponding error


    The zeropoints are saved in extensions with names (EXTNAME) equal
    to the LSTIDX (=LSTSEQ % 13500) and they should only be applied to
    stars observed at the same LSTIDX.

    """


    # use io.PhotFile class to read combined fast lightcurves
    photfile = io.PhotFile(hdf5_cfast_lcs)
    #photfile = hdf5_cfast_lcs


    # read stars from photfile; this is a dictionary with the unique
    # stars in photfile, where nobs is the number of times each star
    # was observed
    stars = photfile.read_stars(['ascc', 'ra', 'dec', 'vmag', 'nobs'])
    #stars = read_stars(photfile, ['ascc', 'ra', 'dec', 'vmag', 'nobs'])


    # determine the relevant healpix indices in stars
    nside = 2**cfg.hp_level
    hpidx = hp.pixelfunc.ang2pix(nside, stars['ra'], stars['dec'],
                                 nest=cfg.hp_nest, lonlat=True)
    stars['hpidx'] = hpidx
    hpidx_uniq = np.unique(hpidx)


    # probability (scalar) in tails of normal distribution exceeding
    # nsigma, i.e. ~0.05 for nsigma=2, used in sigma clipping inside
    # function [zps_comb_lstidx]
    nsigma = min(nsigma_low, nsigma_high)
    prob = (1-stats.norm.cdf(nsigma)) * 2


    log.info ('initializing output hdulist')
    prihdr = fits.Header()
    prihdu = fits.PrimaryHDU(header=prihdr)
    hdulist = fits.HDUList([prihdu])


    # read catalogue; resulting cat is a dictionary with the different
    # catalog fields, e.g. ascc, vmag, sorted by ascc
    #cat = io.read_catalogue (cfg.starcat)


    log.info ('creating combined zeropoint tables for different healpix '
              'indices')
    misc.mem_use()


    if not plot:
        # multiprocess array of unique lstidx's using function
        # [zps_comb_lstidx], which returns the corresponding hdu with
        # extension name LSTIDX
        results = rm.pool_func (zps_comb_hpidx, hpidx_uniq, photfile, stars,
                                nsigma_low, nsigma_high, prob, nproc=ncpus,
                                args_lists=False)

    else:
        results = []
        #for i_lstidx_uniq in lstidx_uniq[0:10]:
        for hpidx in hpidx_uniq:
            results.append(zps_comb_hpidx(hpidx, photfile, stars, nsigma_low,
                                          nsigma_high, prob))


    # discard None entries
    results = [r for r in results if r is not None]


    # results contain list of tables with columns ['lstidx', 'ascc',
    # 'zp0', 'zp0err', 'zp1', 'zp1err']; stack them together
    log.info ('stacking individual tables into a single one')
    table = vstack(results)


    # sort table in place, first by lstidx and then ascc
    log.info ('sorting table by lstidx and ascc')
    table.sort(['lstidx', 'ascc'])


    # unique lstidx entries in table and how often they appear
    lstidx_uniq, nstars_lstidx = np.unique(table['lstidx'], return_counts=True)


    # indices of the blocks with the same lstidx
    strides = np.append(0, np.cumsum(nstars_lstidx))


    # add stars at same lstidx to hdu
    log.info ('creating hdu extensions from table entries at the same lstidx')
    for i, lstidx in enumerate(lstidx_uniq):

        #mask_lstidx = (table['lstidx'] == lstidx)
        # collect indices with the same lstidx
        table_tmp = table[strides[i]:strides[i+1]]

        # remove column lstidx, which is the same for all entries
        del table_tmp['lstidx']

        # create hdu
        hdu = fits.BinTableHDU(table_tmp)

        # add EXTNAME to header
        hdu.header['EXTNAME'] = str(lstidx)

        # append to list
        hdulist.append(hdu)



    # log warning in case of overwriting fits_out
    if os.path.exists(fits_out):
        log.warning ('overwriting existing output fits table {}'
                     .format(fits_out))
    else:
        log.info ('writing hdulist to output fits table {}'.format(fits_out))


    # write output fits table
    hdulist.writeto(fits_out, overwrite=True)


    misc.mem_use('at end of create_zeropoints_comb')


    return


################################################################################

def zps_comb_hpidx (hpidx, photfile, stars, nsigma_low, nsigma_high, prob):


    # copy stars to avoid editing the input dictionary
    stars = stars.copy()


    # relevant ascc IDs in stars in this healpixel
    mask = (stars['hpidx'] == hpidx)
    for key in list(stars.keys()):
        stars[key] = stars[key][mask]


    # Read lstseq and fluxes from relevant stars in photfile
    fields = ['lstseq', 'flux0', 'eflux0', 'flux1', 'eflux1', 'aflag', 'pflag']
    # curves is a record array
    curves = photfile.read_lightcurves(stars['ascc'], fields, perstar=False)
    #curves = read_lightcurves(photfile, stars['ascc'], fields, perstar=False)


    # if no entries for these stars, return
    if len(curves) == 0:
        log.warn ('no entries in curves for hpidx {}'.format(hpidx))
        return
    #else:
    #    log.info ('{} entries in curves for hpidx {}'.format(len(curves), hpidx))


    # create staridx array with the same length as curves that refer
    # to the index in stars
    staridx = np.arange(len(stars['ascc']))
    # this will repeat staridx[0] stars['obs'][0] times, etc.
    staridx = np.repeat(staridx, stars['nobs'])


    # masking bad data from curves and staridx
    mask = (curves['aflag'] == 0) & (curves['pflag'] == 0)
    staridx = staridx[mask]
    curves = curves[mask]


    # read relevant fields from station at lstseq of curves
    station = photfile.read_station(['exptime', 'jd'], curves['lstseq'])
    #station = read_station(photfile, ['exptime', 'jd'], curves['lstseq'])
    exptime = station['exptime']
    jd = station['jd']


    # infer zeropoints corresponding to the fluxes in curves
    mag0, emag0 = misc.flux2mag(curves['flux0']/exptime,
                                eflux=curves['eflux0']/exptime, m0=0)
    mag1, emag1 = misc.flux2mag(curves['flux1']/exptime,
                                eflux=curves['eflux1']/exptime, m0=0)
    vmag = stars['vmag'][staridx]
    zp0 = vmag - mag0
    zp1 = vmag - mag1
    zp0err = emag0
    zp1err = emag1

    ascc_curves = stars['ascc'][staridx].astype(int)

    if plot:
        # show values for specific star
        idx_tmp = np.nonzero((stars['ascc'].astype(int) == 1130718))[0]
        if len(idx_tmp) > 0:
            i_tmp = idx_tmp[0]
            print (stars['ascc'][i_tmp])
            print (stars['vmag'][i_tmp])
            print ('hpidx: {}'.format(hpidx))
            lstseq = curves['lstseq']
            lstidx = lstseq % 13500
            flux0 = curves['flux0']
            eflux0 = curves['eflux0']
            ratio = eflux0/flux0

            for lstidx0 in range(5732, 5733):
            #for lstidx0 in range(min(lstidx), max(lstidx)):
                print ('-------------------------------------------------------')
                print ('lstidx0: {}'.format(lstidx0))
                mc = (staridx==i_tmp) & (lstidx == lstidx0)
                for i in range(np.sum(mc)):
                    print ('ascc: {}, mag0: {}, flux0: {}, eflux0: {}, ratio: '
                           '{}, zp0: {}, zp0err: {}, lstseq: {}, lstidx: {}'
                           .format(ascc_curves[mc][i], mag0[mc][i], flux0[mc][i],
                                   eflux0[mc][i], ratio[mc][i], zp0[mc][i],
                                   zp0err[mc][i], lstseq[mc][i], lstidx[mc][i]))



    # ascc; N.B. stars dictionary is not sorted, so ascc_inv !=
    # staridx
    ascc_curves = stars['ascc'][staridx].astype(int)
    ascc_uniq, ascc_index, ascc_inv = np.unique(ascc_curves,
                                                return_index=True,
                                                return_inverse=True)
    nstars = len(ascc_uniq)
    ascc_vmag = vmag[ascc_index]

    # base dates on int(jd)
    dates_uniq, dates_index, dates_inv = np.unique(jd.astype(int),
                                                   return_index=True,
                                                   return_inverse=True)
    ndates = len(dates_uniq)
    jd_dates = jd[dates_index]


    # unique lstidx epochs in curves
    lstidx = (curves['lstseq'] % 13500)
    lstidx_uniq, lstidx_index, lstidx_inv = np.unique(lstidx,
                                                      return_index=True,
                                                      return_inverse=True)
    nlstidx = len(lstidx_uniq)


    if plot:
        log.info ('hpidx: {}, nlstidx: {}, nstars: {}, ndates: {}'
                  .format(hpidx, nlstidx, nstars, ndates))


    # determine array of indices in masked table - so with the
    # same length as table_curves[mask_lstidx] - of the 3-dim array
    # with shape (nlstidx, nstars, ndates), using the tuple of
    # indices (lstidx_inv, ascc_inv, dates_inv)
    idx_flat = np.ravel_multi_index((lstidx_inv, ascc_inv, dates_inv),
                                    (nlstidx, nstars, ndates))

    # convert idx_flat to the corresponding 3-d indices
    lstidx_idx, ascc_idx, dates_idx = np.unravel_index(
        idx_flat, (nlstidx, nstars, ndates))


    # use get_clipped_wmean function to infer the weighted zp0 and zp1
    # zeropoints and their errors
    pars = [nlstidx, nstars, ndates, lstidx_idx, ascc_idx, dates_idx,
            nsigma_low, nsigma_high, prob, hpidx, lstidx_uniq,
            ascc_uniq, dates_uniq, jd_dates, ascc_vmag]

    # output weighted zeropoints are masked arrays with shape
    # (nlstidx, nstars); not all stars will have values at all lstidx
    wzp0, wzp0err = get_clipped_wmean (zp0, zp0err, *pars)
    wzp1, wzp1err = get_clipped_wmean (zp1, zp1err, *pars)


    # create table_lstidx
    for i, ascc in enumerate(ascc_uniq):

        # define table for this particular ascc of length N
        N = len(lstidx_uniq)
        ascc_arr = np.full(N, int(ascc))
        table_tmp = Table([lstidx_uniq, ascc_arr, wzp0[:,i], wzp0err[:,i],
                           wzp1[:,i], wzp1err[:,i]],
                          names=['lstidx', 'ascc', 'zp0', 'zp0err', 'zp1', 'zp1err'])

        # not all stars will have a weighted mean measurement at a
        # particular lstidx; discard masked entries of wzp0 and wzp1
        # arrays
        mask_discard = wzp0.mask[:,i] & wzp1.mask[:,i]
        table_tmp = table_tmp[~mask_discard]

        if 'table_out' not in locals():
            table_out = table_tmp
        else:
            table_out = vstack([table_out, table_tmp])


    return table_out


################################################################################

def get_clipped_wmean (zp, zperr, nlstidx, nstars, ndates, lstidx_idx, ascc_idx,
                       dates_idx, nsigma_low, nsigma_high, prob, hpidx,
                       lstidx_uniq, ascc_uniq, dates_uniq, jd_dates, ascc_vmag):


    # create 3d masked arrays of shape (nlstidx, nstars, ndates)

    # zeropoint
    zp_3d = np.ma.zeros((nlstidx, nstars, ndates), dtype='float32')
    zp_3d[lstidx_idx, ascc_idx, dates_idx] = zp
    zp_3d[zp_3d==0] = np.ma.masked
    zp_3d_init = zp_3d.copy()

    # zperr
    zperr_3d = np.ma.zeros_like(zp_3d)
    zperr_3d[lstidx_idx, ascc_idx, dates_idx] = zperr
    zperr_3d[zperr_3d==0] = np.ma.masked
    zperr_3d_init = zperr_3d.copy()


    # calculate mad_std, with shape (nlstidx, nstars), just to show it in plots
    if plot:
        std_mad = mad_std (zp_3d, axis=2)


    # use astropy.sigma_clip with a single iteration to clip
    # individual outliers along the dates axis (axis=2)
    zp_3d = sigma_clip (zp_3d, axis=2, stdfunc='mad_std',
                        sigma_lower=nsigma_low, sigma_upper=nsigma_high,
                        masked=True, maxiters=1,
                        # CHECK!!! - copy False will keep the input
                        # data in output array
                        copy=False)


    # now add extra sigma for each star, by forcing the reduced
    # chi-square to approach unity for the measurements of the stars
    # in time; previously this was done per epoch per healpixel (still
    # done for the sys calibration; see sigmas.find_par_sigma), but
    # that scatter is not relevant in this individual zeropoints
    # calibration method - that would lead to an overestimated extra
    # error because the error in the starcat magnitude calibration
    # would be included


    # weights and weighted mean mag along dates (axis=2), with shape:
    # (nlstidx, nstars)
    #w_3d = 1 / zperr_3d**2
    #zp_wmean = np.ma.average (zp_3d, weights=w_3d, axis=2, keepdims=True)
    # or use the simple median instead?
    zp_med = np.ma.median (zp_3d, axis=2, keepdims=True)


    # flux residuals with respect to weighted mean mag
    #res = zp_3d - zp_wmean
    # or with respect to median
    res = zp_3d - zp_med


    # extra sigma
    sigma = find_sigma (res, zperr_3d, axis=2)


    # update zperr_3d and w_3d
    zperr_3d = np.sqrt(zperr_3d**2 + sigma**2)
    w_3d = 1 / zperr_3d**2


    # now check if all stars in this healpixel should be discarded

    # calculate chi2, with shape: (nlstidx, ndates), for the stars in
    # this healpix
    chi2 = np.ma.sum(w_3d * res**2, axis=1, keepdims=True)


    # determine degrees of freedom, with same shape as chi2: (nlstidx,
    # ndates), i.e. the number of stars in this healpix tile for the
    # different lstidx and dates; minimum of ndof=1:
    # stats.chi2.ppf(1-prob, ndof) will become nan if ndof=0; same
    # shape as chi2
    ndof = np.maximum(np.ma.sum(~zp_3d.mask, axis=1, keepdims=True) - 1, 1)


    # chi2 limit above which to discard the stars in this healpix at a
    # specific date; has the same shape as chi2 and dof: (nlstidx,
    # dates); for reduced chi2 would need to divide this by ndof, so
    # the chi2 should be compared to this limit
    chi2_limit = stats.chi2.ppf(1-prob, ndof)


    # determine mask to discard, with shape: (nlstidx, nstars, ndates)
    mask_discard = np.broadcast_to((chi2 > chi2_limit), (nlstidx, nstars, ndates))


    # update zp_3d mask (zperr_3d and w_3d masks are not used
    # without zp_3d below)
    zp_3d[mask_discard] = np.ma.masked


    # perform weighted averaging in fluxes; makes a difference when
    # errors get larger than about 0.1 mag
    use_flux = True
    if use_flux:

        # convert magnitudes to flux
        fzp_3d, fzperr_3d = misc.mag2flux (zp_3d, zperr_3d, m0=25)

        # flux weights (sigma is already included in zperr_3d)
        fw_3d = 1 / fzperr_3d**2

        # weighted mean flux
        fzp_wmean, fsum_weights = np.ma.average(fzp_3d, weights=fw_3d,
                                                axis=2, returned=True)
        fzp_werr = np.sqrt(1/fsum_weights)

        # convert back to magnitudes
        zp_wmean, zp_werr = misc.flux2mag (fzp_wmean, fzp_werr, m0=25)


    else:

        # now that all outliers have been discarded, calculate the
        # weighted mean and corresponding error along dates axis,
        # with shape (nlstidx, nstars)
        zp_wmean, sum_weights = np.ma.average(zp_3d, weights=w_3d,
                                              axis=2, returned=True)
        # not necessary to avoid dividing by zero, since both zp_wmean and
        # sum_weights will be a masked array that takes care of that
        zp_werr = np.sqrt(1/sum_weights)




    if plot:

        # select stars in some lstidx; j is index of lstidx_uniq
        j = np.random.randint(len(lstidx_uniq))
        j = 100

        if ndates > 2 and nstars > 1 and hpidx > 100:

            figname = ('Plots/plot_zps_comb_lstidx_{}_hpindex_{}.pdf'
                       .format(lstidx_uniq[j], hpidx))
            pdf = matplotlib.backends.backend_pdf.PdfPages(figname)

            ny = min(7, nstars)
            nx = (nstars-1) // ny + 1

            log.info ('nstars: {}, ny: {}, nx: {}'.format(nstars, ny, nx))

            fig, ax = plt.subplots (ny, nx, sharex=True, sharey=False,
                                    figsize=(2*8.3,11.7), squeeze=False)
            fig.subplots_adjust(hspace=0.1)


            # extra single plot for the labels
            fig.add_subplot(111, frameon=False)
            # hide tick and tick label of the big axes
            plt.tick_params(labelcolor='none', top=False, bottom=False,
                            left=False, right=False)


            #tobs = Time(dates_uniq, format='jd').isot
            log.info ('jd_dates: {}'.format(jd_dates))
            tobs = Time(jd_dates, format='jd').isot


            for i in range(ny*nx):
                iy = i % ny
                ix = i // ny
                ax[iy,ix].grid(None)


            for i in range(nstars):

                # plotting panels indices
                iy = i % ny
                ix = i // ny

                log.info('')
                log.info('--------------------------------------------------------------------------------')
                log.info('i: {}, ascc_uniq[i]: {}'.format(i, ascc_uniq[i]))
                log.info('--------------------------------------------------------------------------------')
                log.info('len(ascc_idx): {}'.format(len(ascc_idx)))

                mask_valid = ~zp_3d.mask[j,i]
                # mask_red indicates difference in initial and final zp_3d mask
                mask_red = np.logical_xor(~zp_3d.mask[j,i], ~zp_3d_init.mask[j,i])
                #log.info ('len(mask_red): {}'.format(len(mask_red)))
                nvalid = np.sum(mask_valid)
                nred = np.sum(mask_red)
                if nvalid == 0 and nred==0:
                    continue

                if nvalid != 0:
                    x = [pd.to_datetime(d) for d in tobs[mask_valid]]

                if nred != 0:
                    xred = [pd.to_datetime(d) for d in tobs[mask_red]]


                # plot data with errorbars
                log.info ('zp_3d[j,i]:           {}'.format(zp_3d[j,i]))
                log.info ('zp_3d.mask[j,i]:      {}'.format(zp_3d.mask[j,i]))
                log.info ('zp_3d_init[j,i]:      {}'.format(zp_3d_init[j,i]))
                log.info ('zp_3d_init.mask[j,i]: {}'.format(zp_3d_init.mask[j,i]))
                log.info ('zperr_3d_init[j,i]:   {}'.format(zperr_3d_init[j,i]))
                log.info ('zperr_3d[j,i]:        {}'.format(zperr_3d[j,i]))
                log.info ('sigma[j,i]:           {}'.format(sigma[j,i]))
                log.info ('len(mask_valid):      {}'.format(len(mask_valid)))
                log.info ('np.sum(mask_valid):   {}'.format(np.sum(mask_valid)))
                log.info ('len(mask_red):        {}'.format(len(mask_red)))
                log.info ('np.sum(mask_red):     {}'.format(np.sum(mask_red)))


                if nvalid != 0:
                    label = ('{} ({:.2f}): zp={:.3f}$\pm${:.3f}; mad_std={:.3f}'
                             .format(ascc_uniq[i], ascc_vmag[i], zp_wmean[j,i],
                                     zp_werr[j,i], std_mad[j,i]))
                    ax[iy,ix].set_title(label, fontsize=10)
                    ax[iy,ix].plot (x, zp_3d[j,i,mask_valid], 'ko') #, label=label)
                    ax[iy,ix].errorbar(x, zp_3d[j,i,mask_valid],
                                       yerr=zperr_3d[j,i,mask_valid],
                                       linestyle='None', ecolor='k', capsize=2)
                    #ax[iy,ix].legend(loc='best', markerscale=0)


                    # plot extra sigma in tab:blue
                    yerr = np.repeat(sigma[j,i], len(mask_valid))
                    ax[iy,ix].errorbar(x, zp_3d[j,i,mask_valid],
                                       yerr=yerr[mask_valid],
                                       linestyle='None', ecolor='tab:blue', capsize=2)


                # plot zero-weight data in red
                if nred > 0:
                    ax[iy,ix].plot (xred, zp_3d_init[j,i,mask_red], 'o',
                                    color='tab:red')
                    ax[iy,ix].errorbar(xred, zp_3d_init[j,i,mask_red],
                                       yerr=zperr_3d_init[j,i,mask_red],
                                       linestyle='None', ecolor='tab:red',
                                       capsize=2)


                # plot weighted mean and its error
                [x1,x2] = ax[0,0].get_xlim()
                y = zp_wmean[j,i]
                dy = zp_werr[j,i]
                lw = 1
                ax[iy,ix].axhline(y=y, ls='--', lw=lw, color='k')
                ax[iy,ix].axhline(y=y+dy, ls=':', lw=lw, color='k')
                ax[iy,ix].axhline(y=y-dy, ls=':', lw=lw, color='k')


                if ((i+1) % ny == 0) or i+1 == nstars:
                    ax[iy,ix].xaxis.set_major_formatter(
                        mdates.DateFormatter('%Y-%m-%d'))



            # end of loop over panels

            plt.tick_params(axis='both', which='major', pad=50)
            #plt.xticks(rotation=30)
            plt.xlabel('date')
            plt.ylabel('zeropoint = vmag $-$ $-$2.5 log10 (flux0/exptime)')
            plt.title('LSTIDX: {}, HP index: {}'.format(lstidx_uniq[j], hpidx), x=0.5, y=1.05)

            # rotates and right aligns the x labels, and moves the bottom of the
            # axes up to make room for them
            fig.autofmt_xdate()
            plt.grid(None)
            plt.tight_layout()

            pdf.savefig(fig)
            if True:
                plt.show()

            pdf.close()
            plt.close()


    return zp_wmean.astype('float32'), zp_werr.astype('float32')


################################################################################

def get_chi2red (res, err, sigma=0, axis=None):

    # flatten arrays if needed
    if axis is None:
        res = np.ravel(res)
        err = np.ravel(err)


    # median along axis
    med = np.ma.median(res, axis=axis, keepdims=True)


    # determine chi-square
    weights = 1/(err**2 + sigma**2)
    chi2 = np.sum(weights * (res-med)**2, axis=axis, keepdims=True)


    # nobs; np.ma.count also works for non-masked arrays
    nobs = np.ma.count(res, axis=axis, keepdims=True)


    # reduced chi2
    chi2red = chi2 / np.maximum(nobs-1, 1)


    # if chi2red is a 1-d array with a single item, convert it to a
    # scalar
    if axis is None and not np.isscalar(chi2red) and chi2red.size == 1:
        chi2red = chi2red[0]


    return chi2red


################################################################################

def find_sigma (res, err, axis=None, maxiter=15, epsilon=1e-2):


    # with too few valid values in case axis is None, return zero
    # immediately
    if axis is None and np.ma.count(res)<2:
        return 0


    # Search for a solution between 0 and 2.
    err1 = 0
    err2 = 2


    # flatten arrays if needed
    if axis is None:
        res = np.ravel(res)
        err = np.ravel(err)
    else:
        # err1 and 2 (and output err3) have one dimension less than
        # res and err
        err1 += np.zeros(np.sum(res, axis=axis, keepdims=True).shape)
        err2 += np.zeros_like(err1)



    # determined reduced chi-square with respect to the median at
    # beginning of interval
    chi2red = get_chi2red (res, err, err1, axis=axis)
    # if chi2red is still below 1, even when a zero sigma was added to
    # the individual errors, return err1 if chi2red is scalar or
    # create mask to set the corresponding values to zero, further
    # below
    if np.isscalar(chi2red):
        # boolean indicating that chi2red, err1, err2 and err3 are
        # scalars
        scalar = True
        if chi2red <= 1:
            return err1
    else:
        scalar = False
        mask1 = (chi2red <= 1)


    # determined reduced chi-square with respect to the median at end
    # of interval
    chi2red = get_chi2red (res, err, err2, axis=axis)
    # if chi2red is still below 1, even when a zero sigma was added to
    # the individual errors, return err2 if chi2red is scalar or
    # create mask to set the corresponding values to zero, further
    # below
    if scalar:
        if chi2red > 1:
            return err2
    else:
        mask2 = (chi2red > 1)


    # Find the solution.
    for niter in range(maxiter):

        err3 = (err1 + err2)/2
        chi2red = get_chi2red (res, err, err3, axis=axis)

        if scalar:

            if chi2red > 1:
                # if chi2red > 1: move lower error up to err3 to decrease
                # chi2 in next iteration
                err1 = err3
            else:
                # if chi2red < 1: move upper error down to err3 to
                # increase chi2red in next iteration
                err2 = err3

        else:

            # use mask if not scalars
            mask = (chi2red > 1)
            err1[mask] = err3[mask]
            err2[~mask] = err3[~mask]



        # break if chi2red - 1 is sufficiently close to 1
        if np.max(np.abs(chi2red-1)) < epsilon:
            break



    err3 = (err2 + err1)/2

    if not scalar:
        err3[mask1] = 0
        err3[mask2] = 2


    if not np.isscalar(err3) and err3.size == 1:
        err3 = err3[0]


    return err3


################################################################################

if __name__ == "__main__":
    main()
