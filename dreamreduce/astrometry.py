# -*- coding: utf-8 -*-
"""
Created on Thu Nov 17 17:59:53 2016

@author: talens
"""

import os
import subprocess
import datetime
import ephem
import itertools
import time

import numpy as np
from numpy.polynomial import legendre

from astropy.wcs import WCS
from astropy.table import Table
from astropy.time import Time
from astropy.coordinates import EarthLocation, get_body, AltAz, SkyCoord, FK5
from astropy.coordinates import SkyOffsetFrame, Angle
from astropy.io import fits
from astropy.stats import sigma_clipped_stats
from astropy import units as u

from sip_tpv import sip_to_pv

# for PSF fitting - see https://lmfit.github.io/lmfit-py/index.html
from lmfit import minimize, Minimizer, Parameters, fit_report

import matplotlib
import matplotlib.pyplot as plt

from dreamreduce import misc
from dreamreduce import configuration as cfg

import logging
log = logging.getLogger()


###############################################################################
# Functions for determining the position of the sun and moon.
###############################################################################

def sun_position(siteinfo, t=None):
    """ Compute sun position. """

    # If no time is given use system time.
    if t is None:
        t = Time(datetime.datetime.utcnow(), scale='utc')
    else:
        t = Time(t, scale='utc', format='jd')

    # Location of observatory and Alt-Az coordinate frame.
    loc = EarthLocation.from_geodetic(
        siteinfo['lon'], siteinfo['lat'], siteinfo['height'])
    frame = AltAz(obstime=t, location=loc)

    # Coordinates of the sun.
    sun = get_body('sun', t)
    ra, dec = sun.ra.value, sun.dec.value
    sun = sun.transform_to(frame)
    alt = sun.alt.value

    return ra, dec, alt


def moon_position(siteinfo, t=None):
    """ Compute moon position. """

    # If no time is given use system time.
    if t is None:
        t = Time(datetime.datetime.utcnow(), scale='utc')
    else:
        t = Time(t, scale='utc', format='jd')

    # Location of observatory and Alt-Az coordinate frame.
    loc = EarthLocation.from_geodetic(
        siteinfo['lon'], siteinfo['lat'], siteinfo['height'])
    frame = AltAz(obstime=t, location=loc)

    # Coordinates of the sun.
    moon = get_body('moon', t)
    ra, dec = moon.ra.value, moon.dec.value
    moon = moon.transform_to(frame)
    alt = moon.alt.value

    return ra, dec, alt


def last_sunset(siteinfo, t=None, horizon=-10, use_center=True):
    """ Compute time of previous sunset. """

    site = ephem.Observer()
    site.lat = siteinfo['lat']*np.pi/180.
    site.lon = siteinfo['lon']*np.pi/180.
    site.elevation = siteinfo['height']
    site.horizon = str(horizon)

    if t is None:
        site.date = datetime.datetime.utcnow()
    else:
        site.date = t

    sunset = site.previous_setting(ephem.Sun(),use_center=use_center).datetime()

    return sunset


def next_sunrise(siteinfo, t=None, horizon=-10, use_center=True):
    """ Compute time of next sunrise. """

    site = ephem.Observer()
    site.lat = siteinfo['lat']*np.pi/180.
    site.lon = siteinfo['lon']*np.pi/180.
    site.elevation = siteinfo['height']
    site.horizon = str(horizon)

    if t is None:
        site.date = datetime.datetime.utcnow()
    else:
        site.date = t

    sunrise = site.next_rising(ephem.Sun(), use_center=use_center).datetime()

    return sunrise


def closest_sunmin(siteinfo, t=None):
    """ Compute time of next sunrise. """

    site = ephem.Observer()
    site.lat = siteinfo['lat']*np.pi/180.
    site.lon = siteinfo['lon']*np.pi/180.
    site.elevation = siteinfo['height']

    if t is None:
        site.date = datetime.datetime.utcnow()
    else:
        site.date = t

    sunmin1 = site.next_antitransit(ephem.Sun()).datetime()
    sunmin2 = site.previous_antitransit(ephem.Sun()).datetime()

    if abs(t - sunmin1) < abs(t - sunmin2):
        return sunmin1
    else:
        return sunmin2


###############################################################################
# Coordinate transormations.
###############################################################################


def ra2ha(ra, lst):
    """ Convert Right Ascension to Hour Angle. """

    return np.mod(lst*15. - ra, 360.)


def ha2ra(ha, lst):
    """ Convert Hour Angle to Right Ascension. """

    return np.mod(lst*15. - ha, 360.)


def j2000_to_equinox(ra, dec, jd):

    t = Time(jd, scale='utc', format='jd')
    t.format = 'jyear_str'

    gc = SkyCoord(ra, dec, frame='fk5', unit='deg', equinox='J2000')
    gc = gc.transform_to(FK5(equinox=t))

    return gc.ra.value, gc.dec.value


def equinox_to_j2000(ra, dec, jd):

    t = Time(jd, scale='utc', format='jd')
    t.format = 'jyear_str'

    gc = SkyCoord(ra, dec, frame='fk5', unit='deg', equinox=t)
    gc = gc.transform_to(FK5(equinox='J2000'))

    return gc.ra.value, gc.dec.value


###############################################################################
# Functions to solve for transformation parameters.
###############################################################################

def fit_header_wcs (header, xpix, ypix, ra, dec):

    # keywords/parameter to fit, including their value ranges
    keys2fit = {'CRVAL1': [  0, 360],
                'CRVAL2': [-90,  90]}
                #'CD1_1': [-1,1],
                #'CD2_1': [-1,1],
                #'CD1_2': [-1,1],
                #'CD2_2': [-1,1]}


    # create fit parameters
    params = Parameters()
    for key in keys2fit:
        params.add (key, value=header[key], vary=True,
                    min=keys2fit[key][0], max=keys2fit[key][1])
        log.info ('input {}: {}'.format(key, header[key]))


    # do leastsq model fit using minimize
    result = minimize(fcn2min, params, method='Least_squares',
                      args=(header, xpix, ypix, ra, dec))

    # update header
    parvalues = list(result.params.valuesdict().values())
    for i, key in enumerate(keys2fit):
        header[key] = parvalues[i]
        log.info ('output {}: {}'.format(key, header[key]))

    log.info(fit_report(result))

    return header


#################################################################################

def fcn2min (params, header, xpix, ypix, ra, dec):

    # use copy to avoid changing the input header
    header_copy = header.copy()

    # update header with parameters being fit
    params_dict = params.valuesdict()
    for key in params_dict:
        header_copy[key] = params_dict[key]

    # convert ra,dec to pixel coordinates with updated header
    wcs_hdr = WCS (header_copy, naxis=2)
    xpix_new, ypix_new = wcs_hdr.all_world2pix (ra, dec, 1)

    if not np.all(np.isfinite(xpix_new)):
        log.info ('non-finite values in xpix_new')

    # return residuals
    return np.sqrt((xpix-xpix_new)**2 + (ypix-ypix_new)**2)


#################################################################################

def get_matches (ra1, dec1, ra2, dec2, dist_max=None, return_offsets=True):

    """Find closest match for sources at [ra1], [dec1] among sources at
    [ra2], [dec2]. If [dist_max] is provided (units: arcseconds), only
    the (indices of) sources with total offsets within that limit are
    returned. The indices of the matching sources, one for ra1/dec1
    and one for ra2/dec2, are returned. If [return_offsets] is True,
    the total offset and the RA and DEC offsets from ra1/dec1 to
    ra2/dec2 (i.e. ra1 or dec1 + offset = ra2 or dec2) are also
    returned in units of arcseconds. The input ras and decs are
    assumed to be in units of degrees.

    """

    coords1 = SkyCoord(ra=ra1*u.degree, dec=dec1*u.degree)
    index1 = np.arange(len(ra1))

    coords2 = SkyCoord(ra=ra2*u.degree, dec=dec2*u.degree)
    index2, dist, __ = coords1.match_to_catalog_sky(coords2)

    if dist_max is not None:
        mask_match = (dist.arcsec <= dist_max)
        index1 = index1[mask_match]
        index2 = index2[mask_match]
        dist = dist[mask_match]

    if return_offsets:
        dra, ddec = coords1[index1].spherical_offsets_to(coords2[index2])
        sep = coords1[index1].separation(coords2[index2])
        return index1, index2, dist.arcsec, dra.arcsec, ddec.arcsec, sep.arcsec
    else:
        return index1, index2


#################################################################################

def check_wcs (header, xpix, ypix, ra_starcat, dec_starcat, dirtree,
               update=True, plot=False):

    t1 = time.time()

    # convert pixel coordinates to ra,dec
    wcs_hdr = WCS(header, naxis=2)
    ra, dec = wcs_hdr.all_pix2world(xpix, ypix, 1)

    # match ra,dec with catalog ra_starcat,dec_starcat
    index_starcat, index_pix, offset, dra, ddec, sep = get_matches (
        ra_starcat, dec_starcat, ra, dec, dist_max=300, return_offsets=True)

    nmatch = len(index_pix)
    log.info ('{} matches between starcat and detected sources'
              .format(nmatch))

    if nmatch > 0:

        # convert to arcminutes
        dra /= 60
        ddec /= 60
        sep /= 60

        # calculate means, stds and medians
        dra_mean, dra_median, dra_std = sigma_clipped_stats(
            dra, sigma=100, mask_value=0)
        ddec_mean, ddec_median, ddec_std = sigma_clipped_stats(
            ddec, sigma=100, mask_value=0)
        sep_mean, sep_median, sep_std = sigma_clipped_stats(
            sep, sigma=100, mask_value=0)

    else:

        # set offsets to large value (np.inf is not allowed in fits
        # header)
        dra_mean, dra_median, dra_std = 100
        ddec_mean, ddec_median, ddec_std = 100
        sep_mean, sep_median, sep_std = 100


    log.info ('dra mean: {:.3f}\', median: {:.3f}\', std: {:.3f}\''
              .format(dra_mean, dra_median, dra_std))
    log.info ('ddec mean: {:.3f}\', median: {:.3f}\', std: {:.3f}\''
              .format(ddec_mean, ddec_median, ddec_std))
    log.info ('sep mean: {:.3f}\', median: {:.3f}\', std: {:.3f}\''
              .format(sep_mean, sep_median, sep_std))


    if update:
        # astrometric calibration accuracy
        header['DRA'] = (
            dra_mean, '[arcmin] mean RA offset wrt catalog stars')
        header['DRA_STD'] = (
            dra_std, '[arcmin] RA sigma (STD) wrt catalog stars')
        header['DDEC'] = (
            ddec_mean, '[arcmin] mean DEC offset wrt catalog stars')
        header['DDEC_STD'] = (
            ddec_std, '[arcmin] DEC sigma (STD) wrt catalog stars')
        header['SEP'] = (
            sep_mean, '[arcmin] mean separation wrt catalog stars')
        #header['SEP_STD'] = (
        #    sep_std, '[arcmin] separation sigma (STD) wrt catalog stars')
        header['NMATCH'] = (
            nmatch, '# matches between catalog and SE sources')



    if nmatch > 0:

        # convert dra, ddec to uncertainties in pixel coordinates
        x, y = wcs_hdr.all_world2pix (ra_starcat[index_starcat],
                                      dec_starcat[index_starcat], 1)
        dx = np.nanstd(x-xpix[index_pix])
        dy = np.nanstd(y-ypix[index_pix])
        dr = np.nanmean(np.sqrt((x-xpix[index_pix])**2 + (y-ypix[index_pix])**2))

    else:

        dx = dy = dr = 100



    if update:
        header['DX'] = (dx, '[pix] X sigma (STD) wrt catalog stars')
        header['DY'] = (dy, '[pix] Y sigma (STD) wrt catalog stars')
        header['DR'] = (dr, '[pix] mean total offset wrt catalog stars')



    misc.log_timing_memory (t1, label='check_wcs')


    if plot:
        basename = '{}/{}'.format(dirtree['tmp'],
                                  header['FILENAME'].split('.fits')[0])
        plot_wcs(dra, ddec, basename,
                 xpix=xpix[index_pix], ypix=ypix[index_pix])


    return (dra_mean, dra_std, ddec_mean, ddec_std, sep_mean, index_pix,
            index_starcat, dx, dy, dr)


#################################################################################

def plot_wcs (dra, ddec, basename, xpix=None, ypix=None):

    t1 = time.time()

    # calculate means, stds and medians
    dra_mean, dra_median, dra_std = sigma_clipped_stats(
        dra, sigma=100, mask_value=0)
    ddec_mean, ddec_median, ddec_std = sigma_clipped_stats(
        ddec, sigma=100, mask_value=0)

    # plot of dra vs. ddec in arcminutes, including histograms
    lim = np.sqrt(dra_std**2+ddec_std**2)
    lim = 1 # fix to 1 arcminutes
    limits = 5. * np.array([-lim,lim,-lim,lim])
    mask_nonzero = ((dra!=0) & (ddec!=0))
    label1 = 'dRA={:.3f}$\pm${:.3f}\''.format(dra_median, dra_std)
    label2 = 'dDEC={:.3f}$\pm${:.3f}\''.format(ddec_median, ddec_std)
    label3 = 'nstars: {}'.format(len(dra))
    filename_pdf = '{}_dRADEC.pdf'.format(basename)
    title = basename.split('/')[-1]
    result = plot_scatter_hist(
        dra[mask_nonzero], ddec[mask_nonzero], limits,
        xlabel='delta RA [arcmin]', ylabel='delta DEC [arcmin]',
        label=[label1,label2,label3],
        labelpos=[(0.77,0.9),(0.77,0.85),(0.77,0.8)],
        filename=filename_pdf, title=title)


    if xpix is not None and ypix is not None:

        # also plot offsets wrt x- and y-pixels
        fig, ax = plt.subplots(4, 1, sharex=False, sharey=False,
                               figsize=(8,8))
        fig.subplots_adjust(hspace=0.5)

        ax[0].plot (xpix, dra, '.', color='tab:blue')
        ax[0].set_xlabel('X [pixels]')
        ax[0].set_ylabel('delta RA [\']')

        ax[1].plot (xpix, ddec, '.', color='tab:blue')
        ax[1].set_xlabel('X [pixels]')
        ax[1].set_ylabel('delta DEC [\']')

        ax[2].plot (ypix, dra, '.', color='tab:blue')
        ax[2].set_xlabel('Y [pixels]')
        ax[2].set_ylabel('delta RA [\']')

        ax[3].plot (ypix, ddec, '.', color='tab:blue')
        ax[3].set_xlabel('Y [pixels]')
        ax[3].set_ylabel('delta DEC [\']')

        plt.setp(ax, ylim=(-5,5))
        plt.suptitle('{}\n\n{}\n{}'.format(title, label1, label2))

        filename_pdf = '{}_dRADEC_pixels.pdf'.format(basename)
        fig.savefig(filename_pdf)


    plt.close()

    misc.log_timing_memory (t1, label='plot_wcs')

    return



#################################################################################

def solve_wcs (fits_cat, header, dirtree, tweak_order=6):

    # run astrometry.net
    t1 = time.time()

    # basename
    basename = os.path.join(dirtree['tmp'], header['FILENAME'].split('.fits')[0])
    path, name = os.path.split(basename)

    # pick brightest stars from catalog
    nbright = 1000
    table_cat = Table.read (fits_cat)
    mask_use = (table_cat['FLAGS']<=3)
    flux2 = np.array(table_cat['FLUX_APER'])
    index_sort = np.argsort(flux2[mask_use,1])[::-1]
    table_cat_bright = table_cat[mask_use][index_sort][:nbright]
    fits_cat_bright = fits_cat.replace('.fits', '_bright.fits')
    table_cat_bright.write (fits_cat_bright, overwrite=True)

    # to be able to show brightest stars in ds9
    filename = header['FILENAME'].split('.fits')[0]
    result = prep_ds9regions('{}/{}_ds9_SEbright.txt'
                             .format(dirtree['tmp'], filename),
                             table_cat_bright['XWIN_IMAGE'],
                             table_cat_bright['YWIN_IMAGE'],
                             radius=2.5, width=2,
                             color='cyan')

    # infer camera to use below
    camera = header['FILENAME'].split('.fits')[0][-3:]

    # approximate declination of camera center
    dec_cntr = cfg.cam_dec_cntr[camera]

    # estimate central RA of camera in degrees from LST and the camera
    # LHA, where LHA = LST - RA
    ra_cntr = ((header['LST'] - cfg.cam_lha[camera]) * 15) % 360

    # parity to slightly speed up
    parity = 'neg'

    # pixel scale range
    pixscale = cfg.pixscale
    varyfrac = cfg.pixscale_varyfrac
    scale_low = (1 - varyfrac) * pixscale
    scale_high = (1 + varyfrac) * pixscale

    log.info ('approximate ra_cntr: {:.4f} deg, dec_cntr: {:.4f} deg'
              .format(ra_cntr, dec_cntr))

    # solve-field command to execute
    cmd = ['solve-field',
           fits_cat_bright,
           '--config', cfg.astronet_cfg,
           '--no-plots',
           '--x-column', 'XWIN_IMAGE',
           '--y-column', 'YWIN_IMAGE',
           '--sort-column', 'FLUX_APER',
           '--no-remove-lines',
           '--uniformize', '0',
           '--width', str(cfg.xsize),
           '--height', str(cfg.ysize),
           #'--crpix-center',
           #'--depth', '30,60,90,120,150,180,210,240,270,300',
           '--cpulimit', '30',
           '--tweak-order', str(tweak_order),
           '--ra', str(ra_cntr), '--dec', str(dec_cntr),
           '--radius', str(cfg.astronet_radius),
           '--overwrite',
           '--scale-low', str(scale_low), '--scale-high', str(scale_high),
           '--scale-units', 'app',
           '--parity', parity,
           '--new-fits', 'none',
           '--out', name,
           '--dir', path]


    status = misc.run_cmd (cmd)
    misc.log_timing_memory (t1, label='Astrometry.net')
    header['WCS_TIME'] = (np.around(time.time()-t1, decimals=3),
                          '[s] time for A.net to find WCS solution')


    # check if Astrometry.net finished ok by checking for presence
    # of .solved file
    if (not os.path.exists('{}.solved'.format(basename))
        or status!=0):
        msg = ('solve-field (Astrometry.net) failed for {} with status {}'
               .format(basename, status))
        log.error(msg)
        # for some reason, status can be 0 even if solution was not
        # found; setting status to -1 in that case
        if status==0:
            msg = ('solving failed but status=0; setting status to -1 for {}'
                   .format(basename))
            log.error(msg)
            status = -1

        return header, status



    # read WCS solution
    fits_wcs = '{}.wcs'.format(basename)
    with fits.open(fits_wcs) as hdulist:
        header_wcs = hdulist[-1].header


    # check if B_ORDER in header
    if 'B_ORDER' not in header_wcs:

        status = 2
        msg = ('B_ORDER not in header_wcs for {}; exiting with status {}'
               .format(basename, status))
        log.error (msg)
        return header, status

        # previously an attempt was made to run astrometry.net again
        # with a tweak_order minus one
        if False:
            tweak_order -= 1
            log.warning ('B_ORDER not in header_wcs for {}; attempting again '
                         'with tweak order {}'.format(basename, tweak_order))
            solve_wcs (fits_cat, header, dirtree, tweak_order=tweak_order)



    # remove HISTORY, COMMENT and DATE fields from Astrometry.net header
    # they are still present in the base+'.wcs' file
    header_wcs.pop('HISTORY', None)
    header_wcs.pop('COMMENT', None)
    header_wcs.pop('DATE', None)


    # convert SIP header keywords from Astrometry.net to PV
    # keywords that swarp, scamp (and sextractor) understand using
    # this module from David Shupe: sip_to_pv
    try:
        sip_to_pv(header_wcs, tpv_format=True, preserve=False)
    except Exception as e:
        log.error ('following exception raised during [sip_to_pv] for {}: {}'
                   .format(basename, e))


    # update input header with [header_wcs]
    for key in header_wcs:
        if key not in header:
            header[key] = (header_wcs[key], header_wcs.comments[key])


    # update RA and DEC columns of fits_cat
    wcs = WCS(header)
    newra, newdec = wcs.all_pix2world(table_cat['XWIN_IMAGE'],
                                      table_cat['YWIN_IMAGE'], 1)
    table_cat['ALPHAWIN_J2000'].name = 'RA_ICRS'
    table_cat['DELTAWIN_J2000'].name = 'DEC_ICRS'
    table_cat['RA_ICRS'] = newra
    table_cat['DEC_ICRS'] = newdec
    table_cat.write (fits_cat, overwrite=True)

    misc.log_timing_memory (t1, label='solve_wcs')

    return header, status


#################################################################################

def astromaster_key (lst_hrs):

    """determines integer key value of astromaster dictionary,
    corresponding to the input [lst_hrs] and the precision timescale
    as defined by cfg.nimages_astromaster; e.g. if nimages is 50, then
    key=0 refers to the LST interval 00:00 to ~00:05:20 (50*6.4s),
    etc."""

    nwindows = int(13500 / cfg.nimages_astromaster)
    key = int(nwindows * lst_hrs/24) % nwindows

    return key


def astromaster_key_lapalma (lstidx):

    """alternative key based on LSTIDX, i.e. exposure number in
    reference (=La Palma station) sidereal time; e.g. if nimages is
    50, then key=0 refers to the La Palma LST interval 00:00 to
    ~00:05:19 (50*~6.383s), etc. N.B.: nimages_astromaster should fit
    integer times in 13500!"""

    return int(lstidx / nimages_astromaster)


#################################################################################

def save_fetch_wcs (header, fetch=False, use_LST=True):

    """save/fetch header with WCS solution to/from astromaster .npy file"""


    t1 = time.time()

    # only save the LST, UTC-OBS and wcs keywords to save space
    if fetch:
        label_timing = 'fetching'
    else:
        label_timing = 'saving'
        # copy WCS part plus LST and UTC-OBS keywords from header to
        # WCS header to be saved
        header_wcs = fits.PrimaryHDU().header
        header_wcs['LST'] = header['LST']
        header_wcs['UTC-OBS'] = header['UTC-OBS']
        for k in get_keys_wcs():
            if k in header:
                header_wcs[k] = header[k]


    # infer key using [astromaster_key] function
    key = astromaster_key (header['LST'])


    # infer filename and camera from header
    filename = header['FILENAME']
    camera = filename.split('.fits')[0][-3:]


    # check if astromaster file exists
    #name = '{}/astromaster_{}.npy'.format(cfg.confdir, camera)
    name = cfg.astromaster[camera]
    if os.path.exists(name):

        # load the astromaster file
        astromaster_headers = np.load(name, allow_pickle=True).item()

        if fetch:

            # fetch header; find the closest key either in LST or
            # UTC-OBS

            if use_LST:

                # find the nearest key in LST
                lst_array = np.array([d['LST']
                                      for d in astromaster_headers.values()])
                # absolute difference, taking care of the discontinuity at 24
                dlst = np.abs((lst_array - header['LST'] + 12) % 24 - 12)

                # minimum
                i_min = dlst.argmin()

                dtime = dlst
                label = 'LST'

            else:

                # alternatively, find the nearest one in MJD
                mjd_array = np.array([Time(d['UTC-OBS']).mjd
                                      for d in astromaster_headers.values()])

                # difference with UTC-OBS of header
                mjd_header = Time(header['UTC-OBS']).mjd
                dmjd = np.abs(mjd_array - mjd_header)

                dtime = dmjd*24
                label = 'UTC-OBS'


            # minimum index
            i_min = dtime.argmin()

            # N.B.: key refers to the key in the astromaster header
            key = list(astromaster_headers.keys())[i_min]

            log.info ('fetching astromaster header from nearest key found: {} '
                      'at delta {} time of {:.2f} minutes'
                      .format(key, label, dtime[i_min]*60))

            if dtime[i_min] > 1:
                log.warning ('more than 1 hour difference between the input '
                             'header[{}] and the nearest astromaster header '
                             'for {}'.format(label, filename))

            # indent end of: if key not in astromaster_headers.keys():


            misc.log_timing_memory (t1, label='save_fetch_wcs ({})'
                                    .format(label_timing))

            return astromaster_headers[key]


        else:

            log.info ('adding header for {} to key {} of astromaster dictionary'
                      .format(filename, key))
            # add WCS header
            astromaster_headers[key] = header_wcs
            # save it
            np.save(name, astromaster_headers)

    else:

        if fetch:
            # error in case of fetching an astromaster header from an
            # non-existent file
            log.error ('astromaster {} does not exist'.format(name))

        else:
            log.info ('creating astromaster dictionary {}'.format(name))
            log.info ('adding header for {} to key {} of astromaster dictionary'
                      .format(filename, key))
            # create astromaster dictionary
            astromaster_headers = {key: header_wcs}
            # save it
            np.save(name, astromaster_headers)




    misc.log_timing_memory (t1, label='save_fetch_wcs ({})'.format(label_timing))

    return


###############################################################################
# Class for using the astrometry starting from a master solution.
###############################################################################


def clip_astromask(x, y, astromask):

    mask = np.zeros_like(x, dtype='bool')
    for i in range(len(astromask)):
        lx, ux, ly, uy = astromask[i]
        submask = (x > lx) & (x < ux) & (y > ly) & (y < uy)
        mask = mask | submask

    return ~mask


#################################################################################

def stars_in_fov (header, ra, dec, edge=300):

    # use the WCS header projection without the higher-order PV terms
    # to determine which ra,dec coordinates are within the image FOV
    # plus some number of pixels as margin
    header_wcs_only = header.copy()


    # delete the PV keys
    pv_keys = ['PV{}_{}'.format(i,j) for i in range(1,3) for j in range(1,41)]
    for key in pv_keys:
        if key in header_wcs_only:
            del header_wcs_only[key]

    header_wcs_only['CTYPE1'] = 'RA---TAN'
    header_wcs_only['CTYPE2'] = 'DEC--TAN'


    # convert ra,dec to pixel coordinates
    wcs_tmp = WCS (header_wcs_only, naxis=2)
    xpix, ypix = wcs_tmp.all_world2pix(ra, dec, 1)

    # mask
    mask_fov = ((xpix>0.5-edge) & (xpix<cfg.xsize+0.5+edge) &
                (ypix>0.5-edge) & (ypix<cfg.ysize+0.5+edge))


    log.info ('{} coordinates within FoV + extra margin of {} pixels'
              .format(np.sum(mask_fov), edge))


    return mask_fov, header_wcs_only


#################################################################################

def stars_in_fov_old (header, ra, dec):

    """function to narrow down the input coordinates ra,dec to those that
       are in the image field-of-view
    """

    # wcs
    wcs_hdr = WCS (header, naxis=2)

    # determine the central ra,dec of the image
    x_cntr = (cfg.xsize+1)/2
    y_cntr = (cfg.ysize+1)/2
    ra_cntr, dec_cntr = wcs_hdr.all_pix2world(x_cntr, y_cntr, 1)

    # read in CD matrix; see Eq. 189 of Calabretta and Greisen
    # (previously the signs in the sin of CD1_2 and CD2_1 below were
    # switched around)
    cd1_1 = header['CD1_1']  # CD1_1 = CDELT1 *  cos (CROTA2)
    cd1_2 = header['CD1_2']  # CD1_2 = CDELT2 * -sin (CROTA2)
    cd2_1 = header['CD2_1']  # CD2_1 = CDELT1 *  sin (CROTA2)
    cd2_2 = header['CD2_2']  # CD2_2 = CDELT2 *  cos (CROTA2)

    # rotation
    rot_x = np.degrees(np.arctan2( cd2_1, cd1_1))
    rot_y = np.degrees(np.arctan2(-cd1_2, cd2_2))
    rotation = np.mean([rot_x, rot_y])
    #log.info ('rotation: {}'.format(rotation))

    # determine separation between axis midpoints and center to get an
    # estimate of the field size in degrees along the x- and y-axis
    center = SkyCoord(ra_cntr*u.deg, dec_cntr*u.deg)
    xh, yh = (cfg.xsize+1)/2, (cfg.ysize+1)/2
    xy = [(1,yh), (cfg.xsize,yh), (xh,1), (xh,cfg.ysize)]
    x,y = zip(*xy)
    ra0, dec0 = wcs_hdr.all_pix2world(x,y,1)
    midpoints = SkyCoord(ra0*u.deg, dec0*u.deg)
    seps = center.separation(midpoints)

    hfov_lon = np.mean(seps[0:2].deg)
    hfov_lat = np.mean(seps[2:4].deg)
    #log.info ('hfov_lon: {}, hfov_lat: {}'.format(hfov_lon, hfov_lat))

    # use [find_stars] to determine which stars are in the fov
    mask_fov = find_stars (ra, dec, ra_cntr, dec_cntr, hfov_lon, hfov_lat,
                           rotation)


    # alternative
    # -----------

    if False:

        # not successful; leads to nan pixel coordinates after the
        # selected ra,dec set is used

        # determine pixel scales and muliply by cfg.xsize and cfg.ysize
        pixscale_x = np.sqrt(cd1_1**2 + cd2_1**2)
        pixscale_y = np.sqrt(cd1_2**2 + cd2_2**2)
        log.info ('pixscale_x: {}, pixscale_y: {}'.format(pixscale_x,
                                                          pixscale_y))

        hfov_lon_alt = pixscale_x * cfg.xsize/2
        hfov_lat_alt = pixscale_y * cfg.ysize/2
        log.info ('hfov_lon_alt: {}, hfov_lat_alt: {}'
                  .format(hfov_lon_alt, hfov_lat_alt))


    # 2nd alternative
    # ---------------

    if False:

        # not successful either: nan values remaining for xpix,ypix
        # after selected ra,dec set is used

        # calculate world2pix using header without distortion keywords
        header_simple = header.copy()

        keys = ['PV{}_{}'.format(j,i) for j in range(1,3) for i in range(1,40)]
        for key in keys:
            if key in header_simple:
                header_simple.pop(key, None)

        wcs_hdr = WCS(header_simple, naxis=2)
        xpix, ypix = wcs_hdr.all_world2pix(ra, dec, 1)
        mask_fov = ((xpix>0) & (xpix<cfg.xsize) &
                    (ypix>0) & (ypix<cfg.ysize))


    log.info ('{} coordinates within FoV according to [stars_in_fov_old]'
              .format(np.sum(mask_fov)))


    return mask_fov


#################################################################################

def jnow2icrs (ra_in, dec_in, equinox, icrs2jnow=False):

    """function to convert RA and DEC coordinates in decimal degrees to
       ICRS, or back using icrs2jnow=True

    """

    if icrs2jnow:
        coords = SkyCoord(ra_in*u.degree, dec_in*u.degree, frame='icrs')
        jnow = FK5(equinox=equinox)
        coords_out = coords.transform_to(jnow)

    else:
        coords_out = SkyCoord(ra_in*u.degree, dec_in*u.degree, frame='fk5',
                              equinox=equinox).icrs

    return coords_out.ra.value, coords_out.dec.value


#################################################################################

def get_lst (obsdate, lon=cfg.siteinfo['lon']):

    return Time(obsdate).sidereal_time('apparent', longitude=lon).hour


#################################################################################

def update_crval (header, lst_image=None, obsdate_image=None):

    """function to project the ICRS/J2000 WCS solution in [header]
       from the LST/UTC-OBS values in the header to an image taken at
       the input epoch parameters [lst_image] and
       [obsdate_image]. This is done by converting the ICRS/J2000
       header to Jnow at the header UTC-OBS, then update the header
       CRVAL1 according to the change in LST, and convert the WCS back
       from Jnow at [obsdate_image] to ICRS. Should significant
       rotation take place because the two epochs are far apart (at
       least tens of days), this is also taken into account by
       updating the header CD matrix.

    """


    # use copy to avoid changing input header
    header_tmp = header.copy()

    # read header LST and UTC-OBS
    lst_hdr = header_tmp['LST']
    obsdate_hdr = header_tmp['UTC-OBS']

    if (lst_image is not None and lst_image != lst_hdr and
        obsdate_image is not None and obsdate_image != obsdate_hdr):

        # update header CRVAL1 with difference between LST in header
        # and lst_image; this needs to be done in Jnow, so first
        # convert to Jnow, do the delta LST correction and then
        # convert back to ICRS

        if True:
            log.info ('header_tmp[CRVAL1] in: {}'.format(header_tmp['CRVAL1']))
            log.info ('header_tmp[CRVAL2] in: {}'.format(header_tmp['CRVAL2']))


        # convert CRVAL coordinates (ICRS) to Jnow (at UTC-OBS of
        # the WCS solution, i.e. in header)
        equinox_hdr = Time(obsdate_hdr).jyear_str
        #log.info ('equinox_hdr: {}'.format(equinox_hdr))
        crval1_now, crval2_now = jnow2icrs (header['CRVAL1'], header['CRVAL2'],
                                            equinox_hdr, icrs2jnow=True)


        # update crval1_now with difference between image LST and that
        # in the header
        ha0 = ra2ha(crval1_now, lst_hdr)
        crval1_now_proj = ha2ra(ha0, lst_image)

        # convert projected Jnow coordinates (at image epoch) back to ICRS
        equinox_image = Time(obsdate_image).jyear_str
        crval1_icrs, crval2_icrs = jnow2icrs (crval1_now_proj, crval2_now,
                                              equinox_image)

        # update the CRVAL header keywords
        header_tmp['CRVAL1'] = crval1_icrs
        header_tmp['CRVAL2'] = crval2_icrs

        if True:
            log.info ('header_tmp[CRVAL1] out: {}'.format(header_tmp['CRVAL1']))
            log.info ('header_tmp[CRVAL2] out: {}'.format(header_tmp['CRVAL2']))


        # determine differential rotation between pair of pixel
        # coordinates at x,y=(1,1) and (1000,1000) at two different
        # FK5 epochs; this needs to be done using the input [header]
        # as the CRVAL values of [header_tmp] were just modified
        wcs_hdr = WCS (header, naxis=2)
        x = (1,1000)
        y = (1,1000)
        ra_icrs, dec_icrs = wcs_hdr.all_pix2world(x, y, 1)


        # SkyCoordinates
        coord_icrs = SkyCoord (ra_icrs*u.deg, dec_icrs*u.deg)
        coord_hdr = coord_icrs.transform_to(FK5(equinox=equinox_hdr))
        coord_image = coord_icrs.transform_to(FK5(equinox=equinox_image))


        # position angle of coordinate pair in FK5 at equinox_hdr
        pa_hdr = coord_hdr[0].position_angle(coord_hdr[1]).deg
        # position angle of coordinate pair in FK5 at equinox_image
        pa_image = coord_image[0].position_angle(coord_image[1]).deg
        # difference
        dpa = pa_hdr - pa_image


        # only bother to do the rest if delta position angle is
        # non-zero; N.B.: delta position angle for a given position
        # was calculated to be 0.005 deg over a year time, so in
        # general this will be negligible
        if dpa != 0:

            log.warning ('delta position angle: {:.3e} is non-zero when '
                         'projecting FK5 position at input header equinox to '
                         'the image equinox; updating the CD matrix accordingly'
                         .format(dpa))

            log.info ('obsdate_hdr: {}'.format(obsdate_hdr))
            log.info ('obsdate_image: {}'.format(obsdate_image))
            log.info ('coord_hdr: {}'.format(coord_hdr))
            log.info ('coord_image: {}'.format(coord_image))

            log.info ('position angle at equinox_hdr: {}'.format(pa_hdr))
            log.info ('position angle at equinox_image: {}'.format(pa_image))

            log.info ('CD1_1: {}'.format(header_tmp['CD1_1']))
            log.info ('CD1_2: {}'.format(header_tmp['CD1_2']))
            log.info ('CD2_1: {}'.format(header_tmp['CD2_1']))
            log.info ('CD2_2: {}'.format(header_tmp['CD2_2']))

            cd1_1 = header_tmp['CD1_1']  # CD1_1 = CDELT1 *  cos (CROTA1)
            cd1_2 = header_tmp['CD1_2']  # CD1_2 = CDELT2 * -sin (CROTA2)
            cd2_1 = header_tmp['CD2_1']  # CD2_1 = CDELT1 *  sin (CROTA1)
            cd2_2 = header_tmp['CD2_2']  # CD2_2 = CDELT2 *  cos (CROTA2)

            cdelta1 = np.sqrt(cd1_1**2 + cd2_1**2)
            cdelta2 = np.sqrt(cd1_2**2 + cd2_2**2)

            # determine new rotation values
            crota1 = np.arctan2( cd2_1, cd1_1) + np.radians(dpa)
            crota2 = np.arctan2(-cd1_2, cd2_2) + np.radians(dpa)

            log.info ('crota1 updated: {} deg'.format(np.degrees(crota1)))
            log.info ('crota2 updated: {} deg'.format(np.degrees(crota2)))

            header_tmp['CD1_1'] = cdelta1 * np.cos (crota1)
            header_tmp['CD1_2'] = cdelta2 * -np.sin (crota2)
            header_tmp['CD2_1'] = cdelta1 * np.sin (crota1)
            header_tmp['CD2_2'] = cdelta2 * np.cos (crota2)

            log.info ('CD1_1 updated: {}'.format(header_tmp['CD1_1']))
            log.info ('CD1_2 updated: {}'.format(header_tmp['CD1_2']))
            log.info ('CD2_1 updated: {}'.format(header_tmp['CD2_1']))
            log.info ('CD2_2 updated: {}'.format(header_tmp['CD2_2']))



    return header_tmp


#################################################################################

def update_ra (ra, lst_wcs, lst_image):

    ha = ra2ha(ra, lst_image)
    ra = ha2ra(ha, lst_wcs)

    return ra


#################################################################################

def get_keys_wcs ():

    """return list with names of header keywords that are relevant for the
       WCS solution"""

    keys_wcs = [['CTYPE1', 'CTYPE2', 'CRVAL1', 'CRVAL2', 'CRPIX1', 'CRPIX2',
                 'CUNIT1', 'CUNIT2', 'CD1_1', 'CD1_2', 'CD2_1', 'CD2_2'],
                ['PV{}_{}'.format(j,i) for j in range(1,3)
                 for i in range(1,40)]]

    return list(itertools.chain.from_iterable(keys_wcs))


#################################################################################

def world2pix (header, ra, dec, lst_image=None, obsdate_image=None,
               margin=cfg.edge_margin, astromask=None, wcs_only=False):


    # copy of input header
    header_tmp = header.copy()

    # update header CRVAL1 with difference between LST in header and
    # lst_image
    if lst_image is not None and obsdate_image is not None:
        header_tmp = update_crval(header, lst_image=lst_image,
                                  obsdate_image=obsdate_image)

    # alternatively, could project ra coordinates instead
    if False:
        if lst_image is not None:
            ra = update_ra (ra, header['LST'], lst_image)


    # limit the input ra,dec to coordinates that are on the image,
    # otherwise the transformation can fail due to iterative nature
    # and checking the solution for this direction from world
    # coordinates to pixels
    mask_fov, header_wcs_only = stars_in_fov (header_tmp, ra, dec)


    # adopt this wcs_only header if input [wcs_only] is True
    if wcs_only:
        log.warning ('discarding higher-order WCS coefficients in [world2pix]')
        header_tmp = header_wcs_only

    # alternative is to use an offsetframe and determine the objects
    # within half the images width from the center, but may not be
    # very precise because of the wide-field images
    #mask_fov = stars_in_fov_old (header_tmp, ra, dec)


    # convert ra,dec within [mask_fov] to pixel coordinates
    wcs_hdr = WCS (header_tmp, naxis=2)
    xpix, ypix = wcs_hdr.all_world2pix(ra[mask_fov], dec[mask_fov], 1)


    # Remove coordinates not within the margins
    mask = np.isfinite(xpix) & np.isfinite(ypix)
    n_infnan = np.sum(~mask)
    if n_infnan != 0:
        log.warning ('{} coordinates with infinite or nan values'
                     .format(n_infnan))

    if margin is not None:
        mask &= misc.clip_rectangle(xpix, ypix, cfg.xsize, cfg.ysize, margin)


    if False:
        # check typical distances between pixel positions inferred
        # using wcs_only header and the full projection
        dpix = np.sqrt((xpix[mask] - xpix1[mask_fov][mask])**2 +
                       (ypix[mask] - ypix1[mask_fov][mask])**2)
        dpix_mean, dpix_median, dpix_std = sigma_clipped_stats(
            dpix, sigma=100, mask_value=0)
        log.info ('dpix_mean: {}, dpix_median: {}, dpix_std: {}'.
                  format(dpix_mean, dpix_median, dpix_std))


    if not isinstance(ra, float):
        nmask = np.sum(mask)
        mask_fov_edge0, __ = stars_in_fov (header_tmp, ra, dec, edge=0)
        nfov = np.sum(mask_fov_edge0)
        log.info ('{} finite coordinates within margin of {} pixels from '
                  'the image edge ({:.2f}%)'
                  .format(nmask, margin, 100*nmask/nfov))


    if astromask is not None:
        mask &= clip_astromask(xpix, ypix, astromask)


    # create mask that identifies the selected entries in the input
    # ra,dec arrays; need to take care of possibilities that 0, 1 (in
    # case of Moon presence in [reduction.sunmoon2station]) or many
    # coordinates are on the image; if 0, the returned objects will be
    # empty arrays
    if len(mask) <= 1:
        mask_radec = mask
    else:
        mask_radec = mask_fov
        mask_radec[mask_radec] = mask


    return xpix[mask], ypix[mask], mask_radec


#################################################################################

def pix2world (header, xpix, ypix, lst_image=None, obsdate_image=None):

    header_tmp = header.copy()

    # update header CRVAL1 with difference between LST in header and
    # lst_image
    if lst_image is not None:
        header_tmp = update_crval (header_tmp, lst_image=lst_image,
                                   obsdate_image=obsdate_image)

    # convert pixel coordinates to ra,dec
    wcs_hdr = WCS (header_tmp, naxis=2)
    ra, dec = wcs_hdr.all_pix2world(xpix, ypix, 1)

    return ra, dec


################################################################################

def solve_single (image, header, fits_cat, ra_starcat, dec_starcat, dirtree,
                  drate=0.6, margin=668, astromask=None, plot=False):
    """ Try to create a new solution from the give image.

        Flags:
        0 : All went well.
        1 : The image contained bright elements, but a solution was made.
        2 : The image is too bright no new solution was made.
        4 : Detection rate fell below the threshold, no solution was made.
        8 : Solution is not within limits set in configuration
    """
    # import ipdb; ipdb.set_trace()
    #log = logging.getLogger('dreamreduce').getChild('astrometry')
    aflag = 0

    # Check for bright elements.
    if (np.nanpercentile(image, 95) > 9500) | (np.nanstd(image) > 2000):

        log.info('Image may be bright, attempting a solution...')
        aflag += 1

    # Check for extreme amounts of bright elements.
    if (np.nanmedian(image) > 20000):

        log.info('Image is bright, original code would be terminating...')
        aflag += 2
        #return aflag, dict(), headers


    # extract source-extractor pixel coordinates from input fits_cat
    table_cat = Table.read(fits_cat)
    xpix = table_cat['XWIN_IMAGE']
    ypix = table_cat['YWIN_IMAGE']


    if True:
        # avoid objects near edge
        mask = misc.clip_rectangle(xpix, ypix, cfg.xsize, cfg.ysize,
                                   cfg.edge_margin)
        xpix = xpix[mask]
        ypix = ypix[mask]
        table_cat = table_cat[mask]
        table_cat.write (fits_cat, format='fits', overwrite=True)


    # save ds9 regions file with source-extractor detections
    filename = header['FILENAME'].split('.fits')[0]
    result = prep_ds9regions('{}/{}_ds9_SEdetections.txt'
                             .format(dirtree['tmp'], filename),
                             xpix, ypix, radius=2.5, width=2,
                             color='blue')
    mask_flags = (table_cat['FLAGS'] != 0)
    result = prep_ds9regions('{}/{}_ds9_SEdetections_FLAGSnonzero.txt'
                             .format(dirtree['tmp'], filename),
                             xpix[mask_flags], ypix[mask_flags],
                             radius=2.5, width=2, color='red')



    # Solve the WCS parameters.
    log.info('Solving for the WCS transformation.')
    header, status = solve_wcs (fits_cat, header, dirtree)


    # determine WCS accuracy for the new solution if all went well
    # in [solve_wcs], i.e. status is zero
    if status == 0:
        dra, dra_std, ddec, ddec_std, sep, index_pix, index_starcat, dx, dy, dr \
            = check_wcs (header, xpix, ypix, ra_starcat, dec_starcat, dirtree,
                         plot=plot)


    # check if solution is within constraints
    if (status == 0 and
        np.abs(dra) < cfg.wcs_offset and dra_std < cfg.wcs_std and
        np.abs(ddec) < cfg.wcs_offset and ddec_std < cfg.wcs_std):

        log.info ('Astrometry.net WCS solution within constraints for {}'
                  .format(filename))

        # if solution is within constraints, save it
        save_fetch_wcs (header, fetch=False)

    else:

        log.warning ('Astrometry.net WCS solution not acceptable for {}'
                     .format(filename))

        # keep track of the best header solution, in terms of total
        # separation, while fetching solutions from astromaster
        if status == 0:
            header_best = header
            sep_best = sep
        else:
            sep_best = np.inf


        for niter in range(1,3):

            if niter==1:
                use_LST = True
                label = 'LST'
            else:
                use_LST = False
                label = 'UTC-OBS'


            # if solution not acceptable, infer WCS from astromaster,
            # first using nearest solution in LST, and in the 2nd
            # iteration UTC-OBS
            header = wcs_from_astromaster(header, use_LST=use_LST)


            # determine WCS accuracy for the new solution
            dra, dra_std, ddec, ddec_std, sep, index_pix, index_starcat, dx, dy,\
                dr = check_wcs (header, xpix, ypix, ra_starcat, dec_starcat,
                                dirtree, plot=plot)


            if (np.abs(dra) < cfg.wcs_offset and dra_std < cfg.wcs_std and
                np.abs(ddec) < cfg.wcs_offset and ddec_std < cfg.wcs_std):

                log.info ('nearby {} header WCS solution within constraints '
                          'for {}'.format(label, filename))
                break


            # if still not within constraints, keep track of which WCS
            # solution was best so far
            if sep < sep_best:
                log.info ('new separation {:.3f} lower than before ({:.3f}); '
                          'using new {} header as best solution so far for {}'
                          .format(sep, sep_best, label, filename))
                header_best = header
                sep_best = sep


            msg = ('nearby {} header WCS solution not acceptable for {}'
                   .format(label, filename))
            if niter==1:
                log.warning (msg)
            else:
                log.error (msg)
                # for last iteration, set header to that with best WCS
                # solution so far
                header = header_best

                # flag indicating the poor solution
                aflag += 8



        # determine WCS accuracy for the adopted solution
        dra, dra_std, ddec, ddec_std, sep, index_pix, index_starcat, dx, dy, dr \
            = check_wcs (header, xpix, ypix, ra_starcat, dec_starcat, dirtree,
                         plot=plot)



    # some parameters to keep from original code
    if True:

        # fstars - number of starcat entries that after applying
        # [world2pix] function are finite and not on the image
        # edges (defined by cfg.edge_margin, checked with
        # [clip_rectangle]) and not clipped through astromask
        log.info ('astromask used: {}'.format(astromask))
        x, y, mask = world2pix (header, ra_starcat, dec_starcat,
                                astromask=astromask)
        fstars = len(x)

        # x,y are likely meant to be pixel indices instead of the
        # pixel coordinates that world2pix provides, so subtract one:
        x -= 1
        y -= 1

        # fstars_c - subset of fstars that are inside central
        # area of chip, with a margin of 668 pixels
        mask_central = ((np.abs(x - cfg.xsize/2) < margin) &
                        (np.abs(y - cfg.ysize/2) < margin))
        fstars_c = np.sum(mask_central)

        # ustars - originally: number of stars that were recovered
        # with zero flags after processing the fstars with
        # [find.gaussfit_mp]; now: replaced with number of matches
        # between source-extractor positions and starcat
        ustars = len(index_starcat)


        # central area
        mask_central = ((np.abs(xpix[index_pix] - cfg.xsize/2) < margin) &
                        (np.abs(ypix[index_pix] - cfg.ysize/2) < margin))
        ustars_c = np.sum(mask_central)

        log.info('Found {} of {} expected stars.'.format(ustars, fstars))

        # Check the detection rate.
        if (ustars >= drate*fstars):

            log.info(
                'Current detection rate is {:.2f}'.format(ustars/fstars))

        elif (ustars_c >= drate*fstars_c):

            log.info('Central detection rate is {:.2f}. Attempting a '
                     'solution...'.format(ustars_c/fstars_c))

        else:

            log.info('Too few stars detected, terminating...')
            aflag += 4

            #self._reset_solution()
            #return aflag, dict(), headers




    # Dictionary describing the solution.
    astrosol = dict()

    # Parameters.
    astrosol['lstseq'] = header['LSTSEQ']
    astrosol['fstars'] = fstars
    astrosol['ustars'] = ustars


    # Scatter between predicted and detected coordinates.
    astrosol['dx'] = dx
    astrosol['dy'] = dy
    astrosol['dr'] = dr
    log.info ('dx: {:.3f}, dy: {:.3f}, dr: {:.3f}'.format(dx, dy, dr))

    # Jnow! world coordinates of fixed points on the CCD.
    x = cfg.xsize * np.array([0.25, 0.25, 0.5, 0.75, 0.75])
    y = cfg.ysize * np.array([0.25, 0.75, 0.5, 0.25, 0.75])

    # convert these coordinates, which were probably pixel indices
    # (so origin in all_pix2world should be 0), to ra,dec (ICRS)
    wcs_hdr = WCS (header, naxis=2)
    ra, dec = wcs_hdr.all_pix2world(x, y, 0)

    # convert ICRS coordinates to Jnow
    equinox = Time(header['UTC-OBS']).jyear_str
    ra_now, dec_now = jnow2icrs (ra, dec, equinox, icrs2jnow=True)

    astrosol['ra0'] = ra_now[0]
    astrosol['ra1'] = ra_now[1]
    astrosol['ra2'] = ra_now[2]
    astrosol['ra3'] = ra_now[3]
    astrosol['ra4'] = ra_now[4]
    astrosol['dec0'] = dec_now[0]
    astrosol['dec1'] = dec_now[1]
    astrosol['dec2'] = dec_now[2]
    astrosol['dec3'] = dec_now[3]
    astrosol['dec4'] = dec_now[4]


    return aflag, astrosol, header


################################################################################

def wcs_from_astromaster (header, use_LST=True):

    """function that fetches the header from the astromaster
    dictionary that is closest in time to the input [header] in either
    LST (if use_LST is True) or OBS-UTC (if use_LST is False).  That
    astromaster header is projected to the epoch of the input header
    (updating CRVAL and also CD matrix if there is significant
    rotation) and the relevant WCS keywords are copied to [header],
    which is returned.

    """

    # fetch header from astromaster dictionary
    header_astromaster = save_fetch_wcs (header, fetch=True, use_LST=use_LST)


    # update CRVAL values in [header_astromaster] to account for time
    # difference between the relevant astromaster and the input
    # [header]; note that the CD matrix is also updated in case there
    # is significant rotation between the epochs
    header_astromaster = update_crval (header_astromaster,
                                       lst_image=header['LST'],
                                       obsdate_image=header['UTC-OBS'])


    # update input [header] with all WCS keys from modified
    # [header_astromaster]; cannot simply copy the entire astromaster
    # header as that would overwrite e.g. UTC-OBS and LST in [header]
    for key in get_keys_wcs():
        if key in header_astromaster:
            header[key] = header_astromaster[key]


    return header


################################################################################

def plot_scatter_hist (x, y, limits, color='tab:blue', marker='o', xlabel=None,
                       ylabel=None, title=None, label=None, labelpos=None,
                       filename=None):

    # definitions for the axes
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    bottom_h = left_h = left + width

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.2]
    rect_histy = [left_h, bottom, 0.2, height]

    # start with a rectangular Figure
    plt.figure(1, figsize=(8, 8))

    axScatter = plt.axes(rect_scatter)
    axHistx = plt.axes(rect_histx)
    axHisty = plt.axes(rect_histy)

    # the scatter plot:
    axScatter.scatter(x, y, color=color, marker=marker, s=20,
                      edgecolors='black')

    # make some labels invisible
    axHistx.xaxis.set_tick_params(labelbottom=False)
    axHisty.yaxis.set_tick_params(labelleft=False)

    # limits scatter plot
    axScatter.set_xlim((limits[0], limits[1]))
    axScatter.set_ylim((limits[2], limits[3]))

    if xlabel is not None:
        axScatter.set_xlabel(xlabel, fontsize=11)
    if ylabel is not None:
        axScatter.set_ylabel(ylabel, fontsize=11)

    binwidth = 0.2
    xbins = np.arange(limits[0], limits[1] + binwidth, binwidth)
    axHistx.hist(x, bins=xbins, color=color, edgecolor='black')
    ybins = np.arange(limits[2], limits[3] + binwidth, binwidth)
    axHisty.hist(y, bins=ybins, orientation='horizontal',
                 color=color, edgecolor='black')

    yticks = axHistx.yaxis.get_major_ticks()
    yticks[0].set_visible(False)
    xticks = axHisty.xaxis.get_major_ticks()
    xticks[0].set_visible(False)

    # limits histograms
    axHistx.set_xlim((limits[0], limits[1]))
    axHisty.set_ylim((limits[2], limits[3]))

    if label is not None:
        for i in range(len(label)):
            plt.annotate(label[i], xy=(0,0), xytext=labelpos[i],
                         textcoords='figure fraction', fontsize=11)

    if title is not None:
        axHistx.set_title(title)
    if filename is not None:
        plt.savefig(filename)
    plt.close()


################################################################################

def find_stars (ra_cat, dec_cat, ra, dec, dist_lon, dist_lat, rotation):

    """find entries in [ra_cat] and [dec_cat] within [dist_lon] of [ra] and
       within [dist_lat] of [dec]; all in degrees"""

    # center
    center = SkyCoord(ra*u.deg, dec*u.deg)

    # create skyoffset frame centered at center, see
    # https://docs.astropy.org/en/stable/coordinates/matchsep.html#sky-offset-frames
    aframe = center.skyoffset_frame(rotation=Angle(rotation*u.deg))

    # targets
    targets = SkyCoord(ra=ra_cat*u.deg, dec=dec_cat*u.deg)

    # offsets between targets and center in rotated frame
    offsets = targets.transform_to(aframe)

    # determine which targets are on the rotated frame
    mask_fov = ((abs(offsets.lon.deg) <= dist_lon) &
                (abs(offsets.lat.deg) <= dist_lat))

    return mask_fov


################################################################################

def prep_ds9regions(filename, x, y, radius=5, width=2, color='green', value=None):

    """Function that creates a text file with the name [filename] that
    can be used to mark objects at an array of pixel positions (x,y)
    with a circle when displaying an image with ds9."""

    # prepare ds9 region file
    f = open(filename, 'w')
    ncoords = len(x)
    for i in range(ncoords):
        if value is None:
            f.write('circle({},{},{}) # color={} width={}\n'
                    .format(x[i], y[i], radius, color, width))
        else:
            val_tmp = value[i]
            try:
                if 'float' in str(val_tmp.dtype):
                    val_tmp = '{:.2f}'.format(value[i])
            except:
                pass

            f.write('circle({},{},{}) # color={} width={} text={{{}}} '
                    'font="times 12"\n'
                    .format(x[i], y[i], radius, color, width, val_tmp))

    f.close()
    return

################################################################################

def apply_pm (ra, dec, pmra, pmdec, obsdate, epoch=2016.0):

    """function to correct the [ra], [dec] coordinates (ICRS/J2000 in
    decimal degrees) for the proper motion [pmra] and [pmdec] (in
    milli-arcsecond per year) from epoch to obsdate; these inputs are
    all assumed to be numpy arrays. The input [obsdate] string needs
    to be readable by astropy's time.Time, e.g. fits format
    yyyy-mm-ddThh:mm:ss.sss. That date is compared to [epoch] -
    default=2016 for Gaia DR3 - to calculate the offset due to the
    proper motion.

    """

    log.info('executing apply_pm ...')


    # delta time in years between [obsdate] and epoch
    dtime_yr = Time(obsdate).decimalyear - epoch


    # define SkyCoord
    coords_in = SkyCoord(ra*u.deg, dec*u.deg, frame='icrs')


    # offset due to proper motion in arcseconds
    offset_ra = 1e-3 * pmra * dtime_yr
    offset_dec = 1e-3 * pmdec * dtime_yr


    # use astropy's SkyCoord.spherical_offsets_by method to calculate
    # coordinates corrected for offset due to proper motion
    coords_out = coords_in.spherical_offsets_by(offset_ra *u.arcsec,
                                                offset_dec*u.arcsec)


    # return the ra and dec arrays corrected for proper motion
    return np.array(coords_out.ra), np.array(coords_out.dec)


################################################################################
