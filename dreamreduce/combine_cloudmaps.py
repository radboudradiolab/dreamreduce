
import os
import argparse
import glob

# set up log
import logging
import time
logfmt = ('%(asctime)s.%(msecs)03d [%(levelname)s, %(process)s] %(message)s '
          '[%(funcName)s, line %(lineno)d]')
datefmt = '%Y-%m-%dT%H:%M:%S'
logging.basicConfig(level='INFO', format=logfmt, datefmt=datefmt)
logFormatter = logging.Formatter(logfmt, datefmt)
logging.Formatter.converter = time.gmtime #convert time in logger to UTC
log = logging.getLogger()
#log.propagate = False

import numpy as np
import healpy as hp
import h5py

from astropy.io import fits
from astropy.table import Table

from dreamreduce import configuration as cfg
from dreamreduce import io


################################################################################

def combine_cloudmaps (lstseq_start, nlstseq=5, combine='average',
                       path_files='.', file_type='fits', output_map=None,
                       nobs_clean=5, sigma_factor=0):


    # lstseq_start expected to be an integer
    if not isinstance(lstseq_start, int):
        try:
            lstseq_start = int(lstseq_start)
        except:
            log.error ('cannot convert lstseq_start {} to an integer'
                       .format(lstseq_start))
            raise SystemExit


    # prepare list of files to combine, checking for files with
    # different extensions
    lstseq_list = [lstseq_start + i for i in range(nlstseq)]
    for ext in ['.gz', '', '.fz']:
        filenames = ['{}/clouds{}{}.{}{}'.format(path_files, lstseq, camid,
                                                 file_type, ext)
                     for lstseq in lstseq_list for camid in cfg.camid_list]
        # remove non-existing entries
        filenames = sorted([fn for fn in filenames if os.path.exists(fn)])
        if len(filenames) == 0:
            # try the next extension
            continue
        else:
            # assume all files have this extension
            break


    nfiles = len(filenames)
    if nfiles==0:
        log.error ('error: no cloud files found to combine')
        raise SystemExit
    else:
        log.info ('{} combining these {} cloud files: {}'
                  .format(combine, nfiles, filenames))


    clouds_list = []
    sigma_list = []
    nobs_list = []
    jds_list = []
    for i, cloud_file in enumerate(filenames):

        if 'hdf5' in cloud_file:

            with h5py.File(cloud_file, 'r') as f:
                clouds = np.ma.masked_invalid(f['clouds'])
                sigma = np.ma.masked_invalid(f['sigma'])
                nobs = np.ma.masked_invalid(f['nobs'])

                # get jd from attribute
                jd = np.array([f.attrs['jd']])


        elif 'fits' in cloud_file:

            t = Table.read(cloud_file)
            clouds = np.ma.masked_invalid(t['clouds'])
            sigma = np.ma.masked_invalid(t['sigma'])
            nobs = np.ma.masked_invalid(t['nobs'])

            # get jd from header
            with fits.open(cloud_file, memmap=True) as hdulist:
                header = hdulist[-1].header
                jd = header['JD']


        # clouds, sigma and nobs are arrays with the number of
        # healpix tiles at a chosen level: 12*4**(cfg.hp_level)
        clouds_list.append(clouds)
        sigma_list.append(sigma)
        nobs_list.append(nobs)
        # convert jd to an array with the same size and mask as clouds
        # to be able to determine the effective JD as a function of
        # healpixel (e.g. if an expected clouds value is missing in a
        # particular healpixel)
        jds_list.append(np.ma.zeros_like(clouds, dtype=float) + jd)



    # stack the arrays
    clouds_stack = np.ma.stack(clouds_list)
    sigma_stack = np.ma.stack(sigma_list)
    nobs_stack = np.ma.stack(nobs_list)
    jds_stack = np.ma.stack(jds_list)


    # combine cloud/sigma/nobs
    if combine == 'average':
        clouds_comb = np.ma.average(clouds_stack, axis=0)
        sigma_comb = calc_sigma (sigma_stack, axis=0)
        #nobs_comb = np.ma.average(nobs_stack, axis=0)

    elif combine == 'median':
        clouds_comb = np.ma.median(clouds_stack, axis=0)
        sigma_comb = np.sqrt(np.pi/2) * calc_sigma (sigma_stack, axis=0)
        #nobs_comb = np.ma.median(nobs_stack, axis=0)

    elif combine == 'weighted':
        clouds_comb, weights_sum = np.ma.average(clouds_stack, axis=0,
                                                weights=1/sigma_stack**2,
                                                returned=True)
        sigma_comb = 1/np.sqrt(weights_sum)
        # just use average for Nobs
        #nobs_comb = np.ma.average(nobs_stack, axis=0)


    # record nobs as sum
    nobs_comb = np.ma.sum(nobs_stack, axis=0)


    mask = ~clouds_comb.mask & ~sigma_comb.mask
    print ('clouds_comb[mask]: {}'.format(clouds_comb[mask]))
    print ('sigma_comb[mask]: {}'.format(sigma_comb[mask]))
    print ('nobs_comb[mask]: {}'.format(nobs_comb[mask]))


    # record jds as average
    jds_comb =  np.ma.average(jds_stack, axis=0)
    # effective jd
    jd_eff = np.ma.median(jds_comb)


    # convert dtype to float32/int32
    clouds_comb = clouds_comb.astype('float32')
    sigma_comb = sigma_comb.astype('float32')
    nobs_comb = nobs_comb.astype('int32')


    # clean arrays in place
    clean_map (clouds_comb, sigma_comb, nobs_comb, nobs_clean, sigma_factor)


    # default name for output_map
    lstmin = lstseq_start
    lstmax = lstseq_start + nlstseq - 1
    if output_map is None:
        output_map = '{}/clouds{}_{}_{}.{}'.format(path_files, lstmin, lstmax,
                                                   combine, file_type)


    # write hdf5 file or fits table depending on filename extension
    log.info ('writing combined output map: {}'.format(output_map))
    if output_map.endswith('.hdf5'):

        io.write_clouds(output_map, nobs_comb, clouds_comb, sigma_comb,
                        jd=jd_eff, lstmin=lstmin, lstmax=lstmax)


    elif output_map.endswith('.fits'):

        # write info to header, starting with an empty one
        header = fits.Header()
        header['JD'] = (jd_eff, '[d] effective mid-exposure Julian Date')
        for i, cloud_file in enumerate(filenames):
            header['file{}'.format(i+1)] = (
                os.path.basename(cloud_file),
                'cloud file {} used to combine'.format(i+1))

        header['NEPOCHS'] = (nlstseq, 'number of LSTSEQ epochs combined')
        header['NFILES'] = (nfiles, 'total number of cloud files combined')
        header['COMBINE'] = (combine, 'combination method applied')

        # write fits table
        io.write_clouds(output_map, nobs_comb, clouds_comb, sigma_comb,
                        header=header, lstmin=lstmin, lstmax=lstmax)

    else:
        log.warning ('output cloud filename {} does not contain .hdf5 or .fits '
                     'extension; not writing any file'.format(output_map))


    return


################################################################################

def clean_map (clouds, sigma, nobs, nobs_clean, sigma_factor, combine='median'):

    # healpix parameters
    nside = 2**cfg.hp_level
    nest = cfg.hp_nest
    lonlat = True


    # identify healpix indices with low Nobs
    mask_valid = ((~clouds.mask) & (~sigma.mask))
    indices_replace = list(np.nonzero((nobs < nobs_clean) & mask_valid)[0])

    print ('np.sum(sigma[mask_valid]==0): {}'.format(np.sum(sigma[mask_valid]==0)))


    # identify healpix indices with high sigma
    if sigma_factor > 0:

        # get indices of neighbours of unmasked healpixels
        indices_valid = np.nonzero(mask_valid)[0]
        print ('mask_valid.shape: {}'.format(mask_valid.shape))
        ras, decs = hp.pixelfunc.pix2ang(nside, indices_valid, nest=nest,
                                         lonlat=lonlat)
        indices_near = hp.pixelfunc.get_all_neighbours(nside, ras, decs, nest=nest,
                                                       lonlat=lonlat)
        print ('indices_near: {}'.format(indices_near))
        print ('indices_near.shape: {}'.format(indices_near.shape))

        print ('sigma[indices_near]: {}'.format(sigma[indices_near]))

        # some healpixels that are not actual neighbours will result
        # in indices of -1; mask out that entry in sigma so that it is
        # not used
        mask_invalid = (indices_near==-1)
        print ('np.sum(mask_invalid): {}'.format(np.sum(mask_invalid)))
        if np.sum(mask_invalid) > 0:
            sigma[-1] = np.ma.masked


        sigma_median = np.ma.median(sigma[indices_near], axis=0)
        print ('sigma_median.shape: {}'.format(sigma_median.shape))
        print ('sigma_median: {}'.format(sigma_median))
        mask_sigma = (sigma[mask_valid] > (sigma_factor * sigma_median))
        print ('mask_sigma.shape: {}'.format(mask_sigma.shape))
        print ('np.sum(mask_sigma): {}'.format(np.sum(mask_sigma)))
        indices_replace += list(np.nonzero(mask_sigma)[0])



    # loop indices to replace
    for index in indices_replace:

        # identify neighboring indices
        ra, dec = hp.pixelfunc.pix2ang(nside, index, nest=nest, lonlat=lonlat)
        indices_near = hp.pixelfunc.get_all_neighbours(nside, ra, dec, nest=nest,
                                                       lonlat=lonlat)

        # only use valid indices
        indices_use = [i for i in indices_near if
                       # discard pixels that are not actual neighbors
                       i != -1 and
                       # discard indices that are being replaced
                       i not in indices_replace]


        n_use = len(indices_use)
        if n_use >= 3:

            if combine == 'average':
                cloud_val = np.ma.average(clouds[indices_use])
                sigma_val = np.sqrt(np.ma.sum(sigma[indices_use]**2)) / n_use

            elif combine == 'median':
                print (clouds[indices_use])
                cloud_val = np.ma.median(clouds[indices_use])
                sigma_val = (np.sqrt(np.pi/2) *
                               np.sqrt(np.ma.sum(sigma[indices_use]**2)) / n_use)

            elif combine == 'weighted':
                cloud_val, weights_sum = np.ma.average(
                    clouds[indices_use], weights=1/sigma[indices_use]**2,
                    returned=True)
                sigma_val = 1/np.sqrt(weights_sum)


            # sum of Nobs in neighboring pixels
            nobs_val = np.sum(nobs[indices_use])

            print ('cloud_val: {}'.format(cloud_val))
            print ('sigma_val: {}'.format(sigma_val))
            print ('nobs_val: {}'.format(nobs_val))

            clouds[index] = cloud_val
            sigma[index] = sigma_val
            nobs[index] = nobs_val



    return


################################################################################

def calc_sigma (sigma_array, axis=0):

    #log.info ('sigma_array.shape: {}'.format(sigma_array.shape))
    #log.info ('sigma_array**2: {}'.format(sigma_array[:,:,0:10]**2))

    # calculate quadratic average of [sigma_array] along [axis]; input
    # array needs to be a masked array
    sigma = np.ma.sum(sigma_array**2, axis=axis)

    #log.info ('sigma.shape: {}'.format(sigma.shape))
    #log.info ('sigma 1: {}'.format(sigma[:,0:10]))

    # sum of non-masked elements along axis (N.B. masked array mask
    # indicates masked elements with True)
    nsum = np.ma.sum(~(sigma_array.mask), axis=axis)

    #log.info ('sigma_array.mask.shape: {}'.format(sigma_array.mask.shape))
    #log.info ('nsum.shape: {}'.format(nsum.shape))
    #log.info ('nsum: {}'.format(nsum[:,0:10]))
    #log.info ('sigma_array.mask: {}'.format(sigma_array.mask[:,:,0:10]))

    # only take square root and divide by nsum if latter is not zero
    mask_nonzero = (nsum != 0)
    sigma[mask_nonzero] = np.sqrt(sigma[mask_nonzero]) / nsum[mask_nonzero]

    #log.info ('sigma 2: {}'.format(sigma[:,0:10]))
    #log.info ('sigma[mask_nonzero]: {}'.format(sigma[mask_nonzero]))

    return sigma


################################################################################

def main():

    parser = argparse.ArgumentParser(description='Combine DREAM cloud maps')
    parser.add_argument('lstseq_start', type=int,
                        help='starting LSTSEQ of maps to combine')
    parser.add_argument('--nlstseq', type=int, default=5,
                        help='number of LSTSEQ epochs to combine; default=5')
    parser.add_argument('--combine', type=str, default='average',
                        choices=['average', 'weighted', 'median'],
                        help='combination method; default=\'average\'')
    parser.add_argument('--path_files', default='.',
                        help='path to the cloud files; default=\'.\'')
    parser.add_argument('--file_type', default='fits', choices=['fits', 'hdf5'],
                        help='input and output file type; default=\'fits\'')
    parser.add_argument('--output_map', type=str, default=None,
                        help='name of output combined cloud map; default: '
                        '[path_files]/clouds[lstseq_start]_[lstseq_end]_[combine]'
                        '.[file_type]')
    parser.add_argument('--nobs_clean', type=int, default=5,
                        help='replace healpix values with their surroundings '
                        'if sum(Nobs) < nobs_clean; default=5')
    args = parser.parse_args()


    # combine cloud maps
    combine_cloudmaps (args.lstseq_start, args.nlstseq, args.combine,
                       args.path_files, args.file_type, args.output_map,
                       args.nobs_clean)



################################################################################

def oldcode():

    # if [input_maps] is a file, read it
    if os.path.exists(input_maps):

        table = Table.read(input_maps, format='ascii', data_start=0,
                           names=['filenames'])
        maps_list = table['filenames']

    elif ',' in input_maps:

        # assume input_maps is a comma-separated list of cloud maps
        maps_list = input_maps.split(',')

    else:

        # assume input_maps is a search string to use in glob.glob
        maps_list = glob.glob(input_maps)


    # check if all maps in selected list exist
    for fn in maps_list:
        if not os.path.exists(fn):
            log.error ('{} not found; exiting'.format(fn))
            raise SystemExit


    combine_cloudmaps (maps_list, output_map)
    log.info ('combined output map: {}'.format(output_map))


    return


################################################################################

if __name__ == "__main__":
    main()
