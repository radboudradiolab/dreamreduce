# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 15:49:42 2017

@author: talens
"""

import os
import glob

import numpy as np

from matplotlib.ticker import MultipleLocator
from matplotlib import rcParams
import matplotlib.gridspec as gridspec
import matplotlib.dates as dates
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')

import h5py

from dreamreduce import configuration as cfg
from dreamreduce import astrometry, io
from dreamreduce import misc


rcParams['xtick.labelsize'] = 'large'
rcParams['ytick.labelsize'] = 'large'
rcParams['axes.labelsize'] = 'x-large'
rcParams['image.interpolation'] = 'none'
rcParams['image.origin'] = 'lower'
rcParams['axes.titlesize'] = 'xx-large'

color_sequence = ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c',
                  '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5',
                  '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f',
                  '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']


def send_mail(subject, message, images):

    import smtplib
    import ssl
    #from email.MIMEMultipart import MIMEMultipart
    #from email.MIMEText import MIMEText
    #from email.MIMEImage import MIMEImage

    #from email.mime.base import MIMEBase
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.image import MIMEImage

    #mailing = io.read_mailing(cfg.mailing)
    mailing = cfg.mailing

    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = mailing['sender']
    msg['To'] = ', '.join(mailing['recipients'])

    body = MIMEText(message, 'plain')
    msg.attach(body)

    for filename in images:
        print('attaching file \'{}\' to email'.format(filename))
        with open(filename, 'rb') as fp:
            img = MIMEImage(fp.read())
            img.add_header('Content-Disposition', 'attachment',
                           filename=os.path.basename(filename))
            msg.attach(img)

    try:
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(mailing['host'], mailing['port'], context) as server:
            server.login(mailing['user'], mailing['pwd'])
            server.sendmail(mailing['user'],
                            mailing['recipients'], msg.as_string())

        # smtpserver = smtplib.SMTP()
        # smtpserver.ehlo()
        # smtpserver.starttls()
        # smtpserver.login(mailing['user'], mailing['pwd'])
        # smtpserver.sendmail(mailing['user'], mailing['recipients'], msg.as_string())
        # smtpserver.close()
    except:
        print('The following email was not sent \n')
        print(message)

    return

###############################################################################
# Functions to send a reduction e-mail.
###############################################################################


def reduction_summary(directory):

    head, tail = os.path.split(directory)
    head, tail = os.path.split(head)


    subject = 'Reduction Summary {}'.format(tail)
    message = []
    images = []

    # Create diagnostic figures.
    figname = os.path.join(directory, tail+'astro.png')

    try:
        # TODO; is this as robust as the original?
        #fastphot = os.path.join(directory, 'fast_'+tail+'.hdf5')
        fastphot = glob.glob(directory + '/fast_*.hdf5')[0]
        nsols = fig_astrometry(fastphot, figname)
    except:
        nsols = 0
    else:
        images.append(figname)

    figname = os.path.join(directory, tail+'phot.png')

    try:
        # TODO; is this as robust as the original?
        #slowphot = os.path.join(directory, 'slow_'+tail+'.hdf5')
        slowphot = glob.glob(directory + '/slow_*.hdf5')[0]
        nimages = fig_photometry(fastphot, slowphot, figname)
    except:
        nimages = 0
    else:
        images.append(figname)

    # Create the text message.
    message.append('Hi,\n\n')
    message.append('{} science frames were processed.\n'.format(nimages))
    message.append('The astrometry was solved {} times.\n'.format(nsols))
    message.append('\nCheers,\nLa Silla')
    message = ''.join(message)

    send_mail(subject, message, images)

    return


def fig_astrometry(fastphot, figname):

    import datetime

    # Read the astrometry.
    with h5py.File(fastphot, 'r') as f:

        grp = f['header']
        siteinfo = dict()
        siteinfo['lon'] = grp.attrs['lon']
        siteinfo['lat'] = grp.attrs['lat']
        siteinfo['height'] = grp.attrs['alt']

        grp = f['astrometry']

        lstseq = grp['lstseq'][()]
        fstars = grp['fstars'][()]
        ustars = grp['ustars'][()]
        dr = grp['dr'][()]

        ra0 = grp['ra0'][()]
        ra1 = grp['ra1'][()]
        ra2 = grp['ra2'][()]
        ra3 = grp['ra3'][()]
        ra4 = grp['ra4'][()]

        dec0 = grp['dec0'][()]
        dec1 = grp['dec1'][()]
        dec2 = grp['dec2'][()]
        dec3 = grp['dec3'][()]
        dec4 = grp['dec4'][()]

        grp = f['station']
        lstseq0 = grp['lstseq'][()]
        lst = grp['lst'][()]
        jd = grp['jd'][()]
        year = grp['year'][()]
        month = grp['month'][()]
        day = grp['day'][()]
        hour = grp['hour'][()]
        mn = grp['min'][()]
        sec = grp['sec'][()]

    utc = np.array([datetime.datetime(year[i], month[i], day[i],
                                      hour[i], mn[i], sec[i]) for i in range(len(lst))])

    # Normalize the julian date.
    offset = np.floor(jd[0])
    jd = jd - offset

    idx = np.searchsorted(lstseq0, lstseq)

    if False:

        # Convert to relative ha, dec.
        ha0 = np.mod(lst[idx]*15 - ra0, 360)
        ha1 = np.mod(lst[idx]*15 - ra1, 360)
        ha2 = np.mod(lst[idx]*15 - ra2, 360)
        ha3 = np.mod(lst[idx]*15 - ra3, 360)
        ha4 = np.mod(lst[idx]*15 - ra4, 360)

        # dec? in arcminutes
        ddec0 = (dec0 - np.mean(dec0)) * 60
        ddec1 = (dec1 - np.mean(dec1)) * 60
        ddec2 = (dec2 - np.mean(dec2)) * 60
        ddec3 = (dec3 - np.mean(dec3)) * 60
        ddec4 = (dec4 - np.mean(dec4)) * 60

        # original code (*60 was previously done when plotting)
        if False:
            dha0 = (ha0 - np.mean(ha0)) * 60
            dha1 = (ha1 - np.mean(ha1)) * 60
            dha2 = (ha2 - np.mean(ha2)) * 60
            dha3 = (ha3 - np.mean(ha3)) * 60
            dha4 = (ha4 - np.mean(ha4)) * 60

        # the units of ha? are 'RA' degrees; convert these to actual
        # offsets on the sky (in 'DEC' arcminutes) with respect to the
        # mean HA, using [misc.haversine]
        dha0 = misc.haversine(ha0, dec0, np.mean(ha0), dec0) * 60
        dha1 = misc.haversine(ha1, dec1, np.mean(ha1), dec1) * 60
        dha2 = misc.haversine(ha2, dec2, np.mean(ha2), dec2) * 60
        dha3 = misc.haversine(ha3, dec3, np.mean(ha3), dec3) * 60
        dha4 = misc.haversine(ha4, dec4, np.mean(ha4), dec4) * 60


    # stack dec1..4 arrays
    dec_array = np.vstack((dec0, dec1, dec2, dec3, dec4))
    # offset to mean DEC in arcminutes
    ddec_array = 60 * (dec_array - np.mean(dec_array, axis=1, keepdims=True))


    # stack ra1..4 arrays
    ra_array = np.vstack((ra0, ra1, ra2, ra3, ra4))
    # convert to hour angles
    ha_array = np.mod(lst[idx]*15 - ra_array, 360)
    # offset to mean HA in arcminutes using [misc.haversine]
    dha_array = np.zeros_like(ddec_array)
    for i in range(5):
        dha_array[i] = 60 * misc.haversine(ha_array[i], dec_array[i],
                                           np.mean(ha_array[i]), dec_array[i])


    fig = plt.figure(figsize=(14, 9))

    gs = gridspec.GridSpec(3, 3)

    majorLocator = MultipleLocator(500)

    dt = datetime.timedelta(minutes=10)
    sunset = astrometry.last_sunset(siteinfo, utc[0]+dt)
    sunrise = astrometry.next_sunrise(siteinfo, utc[-1]-dt)

    # Add stars used.
    ax = plt.subplot(gs[0, 0:2])
    ax.xaxis.set_major_formatter(dates.DateFormatter('%H:%M'))
    ax.yaxis.set_major_locator(majorLocator)
    mag1, mag2 = cfg.maglim_astro
    plt.plot(utc[idx], fstars, 'k.',
             label='starcat sources ({}<=Vmag<={}) in FOV'.format(mag1, mag2))
    plt.plot(utc[idx], ustars, '.', c=(146./255, 0, 0),
             label='starcat-source extractor matches')
    plt.xlim(sunset-dt, sunrise+dt)
    plt.axvline(sunset, c=(0./255, 109./255, 219./255), lw=2)
    plt.axvline(sunrise, c=(0./255, 109./255, 219./255), lw=2)
    plt.xlabel('Time [UTC]')
    plt.ylabel('# stars')
    plt.legend()

    # Add residual RMS.
    ax = plt.subplot(gs[1, 0:2])
    ax.xaxis.set_major_formatter(dates.DateFormatter('%H:%M'))
    plt.plot(utc[idx], dr, 'k.')
    plt.xlim(sunset-dt, sunrise+dt)
    plt.axvline(sunset, c=(0./255, 109./255, 219./255), lw=2)
    plt.axvline(sunrise, c=(0./255, 109./255, 219./255), lw=2)
    plt.ylim(0, 2)
    plt.xlabel('Time [UTC]')
    plt.ylabel('RMS [pix]')

    # Add change in position.
    plt.subplot(gs[2, 0], aspect='equal')
    plt.annotate('(1002, 668)', (0, 1), xytext=(10, -10), xycoords='axes fraction',
                 textcoords='offset points', va='top', ha='left', size='x-large')
    plt.plot(dha_array[0], ddec_array[0], 'k.')
    plt.xlim(-2, 2)
    plt.ylim(-2, 2)
    plt.xlabel("$\Delta$HA [']")
    plt.ylabel("$\Delta$Dec [']")

    plt.subplot(gs[2, 1], aspect='equal')
    plt.annotate('(1002, 2004)', (0, 1), xytext=(10, -10), xycoords='axes fraction',
                 textcoords='offset points', va='top', ha='left', size='x-large')
    plt.plot(dha_array[1], ddec_array[1], 'k.')
    #plt.plot(dha1, ddec1, 'k.')
    plt.xlim(-2, 2)
    plt.ylim(-2, 2)
    plt.xlabel("$\Delta$HA [']")
    plt.ylabel("$\Delta$Dec [']")

    plt.subplot(gs[2, 2], aspect='equal')
    plt.annotate('(2004, 1336)', (0, 1), xytext=(10, -10), xycoords='axes fraction',
                 textcoords='offset points', va='top', ha='left', size='x-large')
    plt.plot(dha_array[2], ddec_array[2], 'k.')
    #plt.plot(dha2, ddec2, 'k.')
    plt.xlim(-2, 2)
    plt.ylim(-2, 2)
    plt.xlabel("$\Delta$HA [']")
    plt.ylabel("$\Delta$Dec [']")

    plt.subplot(gs[1, 2], aspect='equal')
    plt.annotate('(3006, 668)', (0, 1), xytext=(10, -10), xycoords='axes fraction',
                 textcoords='offset points', va='top', ha='left', size='x-large')
    plt.plot(dha_array[3], ddec_array[3], 'k.')
    #plt.plot(dha3, ddec3, 'k.')
    plt.xlim(-2, 2)
    plt.ylim(-2, 2)
    plt.xlabel("$\Delta$HA [']")
    plt.ylabel("$\Delta$Dec [']")

    plt.subplot(gs[0, 2], aspect='equal')
    plt.annotate('(3006, 2004)', (0, 1), xytext=(10, -10), xycoords='axes fraction',
                 textcoords='offset points', va='top', ha='left', size='x-large')
    plt.plot(dha_array[4], ddec_array[4], 'k.')
    #plt.plot(dha4, ddec4, 'k.')
    plt.xlim(-2, 2)
    plt.ylim(-2, 2)
    plt.xlabel("$\Delta$HA [']")
    plt.ylabel("$\Delta$Dec [']")

    plt.tight_layout()

    plt.savefig(figname, dpi=100)
    plt.close()

    return len(lstseq)


def fig_photometry(fastphot, slowphot, figname):

    import datetime

    fig = plt.figure(figsize=(14, 9))

    ax = plt.subplot(111)
    ax.xaxis.set_major_formatter(dates.DateFormatter('%H:%M'))

    with h5py.File(fastphot, 'r') as f, h5py.File(slowphot, 'r') as g:

        grp = f['header']
        siteinfo = dict()
        siteinfo['lon'] = grp.attrs['lon']
        siteinfo['lat'] = grp.attrs['lat']
        siteinfo['height'] = grp.attrs['alt']

        # Select stars in a small magnitude range.
        grp = f['stars']
        ascc = grp['ascc'][()]
        vmag = grp['vmag'][()]

        mask = (vmag < 4.8) & (vmag > 4.75)
        ascc = ascc[mask]

        # Read the station fields of both files.
        grp = f['station']
        lstseq1 = grp['lstseq'][()]
        jd1 = grp['jd'][()]
        exptime1 = grp['exptime'][()]

        year = grp['year'][()]
        month = grp['month'][()]
        day = grp['day'][()]
        hour = grp['hour'][()]
        mn = grp['min'][()]
        sec = grp['sec'][()]

        utc1 = np.array([datetime.datetime(
            year[i], month[i], day[i], hour[i], mn[i], sec[i]) for i in range(len(jd1))])

        grp = g['station']
        lstseq2 = grp['lstseq'][()]
        jd2 = grp['jdmid'][()]
        exptime2 = grp['exptime'][()]

        # Normalize the julian date.
        offset = np.floor(jd1[0])
        jd1 = jd1 - offset
        jd2 = jd2 - offset

        grp1 = f['lightcurves']
        grp2 = g['lightcurves']

        lines = []
        for i in range(len(ascc)):
            #import ipdb
            # ipdb.set_trace()
            # it = ascc[]
            # Add fast photometry.
            lc = grp1[ascc[i]][()]
            idx = np.searchsorted(lstseq1, lc['lstseq'])

            if np.mean(lc['flux0']/exptime1[idx]) > 30000:
                continue

            l, = ax.plot(utc1[idx], lc['flux0']/exptime1[idx], '.',
                         label=str(ascc[i], 'utf-8'), c=color_sequence[i % 20], zorder=1)
            lines.append(l)

            # Add slow photometry.
            try:
                lc = grp2[ascc[i]]
            except:
                continue
            else:
                idx = np.searchsorted(lstseq1, lc['lstseq'])
                idx2 = np.searchsorted(lstseq2, lc['lstseq'])
                ax.scatter(utc1[idx], lc['flux0']/exptime2[idx2],
                           marker='o', c='w', zorder=2)

    dt = datetime.timedelta(minutes=10)
    sunset = astrometry.last_sunset(siteinfo, utc1[0]+dt)
    sunrise = astrometry.next_sunrise(siteinfo, utc1[-1]-dt)

    plt.xlim(sunset-dt, sunrise+dt)
    plt.axvline(sunset, c=(0./255, 109./255, 219./255), lw=2)
    plt.axvline(sunrise, c=(0./255, 109./255, 219./255), lw=2)

    fig.legend(lines, ascc)
    plt.xlabel('Time [UTC]')
    plt.ylabel('Flux [counts/sec]')

    plt.tight_layout()

    plt.savefig(figname, dpi=100)
    plt.close()

    return len(lstseq1)

###############################################################################
# Functions to send a calibration e-mail.
###############################################################################


def calibration_summary(directory, header_wcs):

    head, tail = os.path.split(directory)
    head, tail = os.path.split(head)

    subject = 'Calibration Summary {}'.format(tail)
    message = []
    images = []

    # Get the systematics file to summarize.
    sysfile = os.path.join(directory, 'sys0_vmag_'+tail+'.hdf5')


    # Create diagnostic figures.
    try:
        figname = fig_transmission(sysfile, header_wcs)
    except:
        pass
    else:
        images.append(figname)

    try:
        figname = fig_intrapix(sysfile, header_wcs)
    except:
        pass
    else:
        images.append(figname)

    try:
        figname = fig_clouds(sysfile)
    except:
        pass
    else:
        images.append(figname)

    # Create the text message.
    message.append('Hi,\n\n')
    message.append('The daily calibration is finished.\n')
    message.append('\nCheers,\nLa Silla')
    message = ''.join(message)

    send_mail(subject, message, images)

    return


def wcsgrid(wcspars):

    # Add lines of constant declination.
    ha = np.linspace(0, 360, 360)
    dec = np.linspace(-80, 80, 17)
    ha, dec = np.meshgrid(ha, dec)

    tmp = ha.shape

    ha, dec = ha.ravel(), dec.ravel()
    ra = astrometry.ha2ra(ha, 0.)

    x, y = astrometry.world2wcs(wcspars, ra, dec, 0.)
    x, y = x.reshape(tmp), y.reshape(tmp)

    here = (x > -50) & (x < cfg.xsize+50) & (y > -50) & (y < cfg.ysize+50)
    x[~here] = np.nan
    y[~here] = np.nan

    plt.plot(x.T, y.T, c='k')

    # Add lines of constant hour angle.
    ha = np.linspace(0, 345, 24)
    dec = np.linspace(-80, 80, 160)
    ha, dec = np.meshgrid(ha, dec)

    tmp = ha.shape

    ha, dec = ha.ravel(), dec.ravel()
    ra = astrometry.ha2ra(ha, 0.)

    x, y = astrometry.world2wcs(wcspars, ra, dec, 0.)
    x, y = x.reshape(tmp), y.reshape(tmp)

    here = (x > -50) & (x < cfg.xsize+50) & (y > -50) & (y < cfg.ysize+50)
    x[~here] = np.nan
    y[~here] = np.nan

    plt.plot(x, y, c='k')

    return


def plot_polar(grid, data, wcspars, **kwargs):

    data = data[1:-1, 1:-1]
    data = np.ma.array(data, mask=np.isnan(data))

    ha, dec = grid.xedges, grid.yedges
    ha, dec = np.meshgrid(ha, dec)
    ra = astrometry.ha2ra(ha, 0.)

    x, y = astrometry.world2wcs(wcspars, ra, dec, 0.)

    print ('ra.shape: {}, dec.shape: {}'.format(ra.shape, dec.shape))
    print ('x.shape: {}, y.shape: {}'.format(x.shape, y.shape))

    im = plt.pcolormesh(x, y, data.T, **kwargs)
    wcsgrid(wcspars)

    return im


def fig_transmission(filename, header_wcs, figname=None):

    if figname is None:
        head, tail = os.path.split(filename)
        tail = tail.rsplit('.')[0].rsplit('_')[-1]
        figname = os.path.join(head, tail+'trans.png')

    # Read the transmission map.
    f = io.SysFile(filename)
    pg, nobs, trans = f.read_transmission()

    #wcspars, polpars, astromask = io.read_astromaster(astromaster)

    # Plot the transmission map.
    fig = plt.figure(figsize=(14, 9))

    plt.suptitle('Transmission', size='xx-large')

    gs = gridspec.GridSpec(2, 2, width_ratios=[15, .5], height_ratios=[1, 10])

    plt.subplot(gs[1, 0], aspect='equal')

    vmin = np.nanpercentile(trans, .1)
    vmax = np.nanpercentile(trans, 99.9)
    #im = plot_polar(pg, trans, wcspars, cmap=plt.cm.viridis,
    #                vmin=vmin, vmax=vmax)
    im = plot_polar(pg, trans, header_wcs, cmap=plt.cm.viridis,
                    vmin=vmin, vmax=vmax)

    plt.xlim(0, cfg.xsize)
    plt.ylim(0, cfg.ysize)

    cax = plt.subplot(gs[1, 1])
    cb = plt.colorbar(im, cax=cax)
    cb.ax.invert_yaxis()
    cb.set_label('Magnitude')

    plt.tight_layout()

    plt.savefig(figname, dpi=100)
    plt.close()

    return figname


def fig_intrapix(filename, astromaster, figname=None):

    if figname is None:
        head, tail = os.path.split(filename)
        tail = tail.rsplit('.')[0].rsplit('_')[-1]
        figname = os.path.join(head, tail+'ipx.png')

    # Read the intrapixel amplitudes.
    f = io.SysFile(filename)
    pg, nobs, amplitudes = f.read_intrapix()

    wcspars, polpars, astromask = io.read_astromaster(astromaster)

    # Plot the amplitudes.
    fig = plt.figure(figsize=(16, 10))

    plt.suptitle('Intrapixel Amplitudes', size='xx-large')

    gs = gridspec.GridSpec(3, 3, width_ratios=[
                           15, 15, .5], height_ratios=[1, 10, 10])

    plt.subplot(gs[1, 0], aspect='equal')
    plt.title(r'$\sin(2\pi x)$')

    im = plot_polar(pg, amplitudes[:, :, 0], wcspars,
                    cmap=plt.cm.viridis, vmin=-.05, vmax=.05)

    plt.xlim(0, cfg.xsize)
    plt.ylim(0, cfg.ysize)

    plt.subplot(gs[1, 1], aspect='equal')
    plt.title(r'$\cos(2\pi x)$')

    im = plot_polar(pg, amplitudes[:, :, 1], wcspars,
                    cmap=plt.cm.viridis, vmin=-.05, vmax=.05)

    plt.xlim(0, cfg.xsize)
    plt.ylim(0, cfg.ysize)

    plt.subplot(gs[2, 0], aspect='equal')
    plt.title(r'$\sin(2\pi y)$')

    im = plot_polar(pg, amplitudes[:, :, 2], wcspars,
                    cmap=plt.cm.viridis, vmin=-.05, vmax=.05)

    plt.xlim(0, cfg.xsize)
    plt.ylim(0, cfg.ysize)

    plt.subplot(gs[2, 1], aspect='equal')
    plt.title(r'$\cos(2\pi y)$')

    im = plot_polar(pg, amplitudes[:, :, 3], wcspars,
                    cmap=plt.cm.viridis, vmin=-.05, vmax=.05)

    plt.xlim(0, cfg.xsize)
    plt.ylim(0, cfg.ysize)

    cax = plt.subplot(gs[1:, 2])
    cb = plt.colorbar(im, cax=cax)
    cb.set_label('Amplitude')

    plt.tight_layout()

    plt.savefig(figname, dpi=100)
    plt.close()

    return figname


def fig_clouds(filename, figname=None):

    if figname is None:
        head, tail = os.path.split(filename)
        tail = tail.rsplit('.')[0].rsplit('_')[-1]
        figname = os.path.join(head, tail+'clouds.png')

    # Read the intrapixel amplitudes.
    f = io.SysFile(filename)
    hg, nobs, clouds, sigma, lstmin, lstmax = f.read_clouds()

    # Plot the clouds.
    fig = plt.figure(figsize=(10, 16))

    plt.suptitle('Clouds', size='xx-large')

    gs = gridspec.GridSpec(2, 2, width_ratios=[15, .5], height_ratios=[1, 20])

    plt.subplot(gs[1, 0], xticks=[], yticks=[])

    mask = np.isfinite(clouds)
    idx1, = np.where(np.any(mask, axis=1))
    idx2, = np.where(np.any(mask, axis=0))
    clouds = clouds[idx1][:, idx2]
    im = plt.imshow(clouds.T, interpolation='None', aspect='auto',
                    cmap=plt.cm.viridis, vmin=-.5, vmax=.5)

    idx, = np.where(np.diff(idx2) > 2)
    for i in idx:
        plt.axhline(i+1, xmax=.1, c='k', lw=2)

    plt.ylabel('Time')
    plt.xlabel('Sky Patch')

    cax = plt.subplot(gs[1, 1])
    cb = plt.colorbar(im, cax=cax)
    cb.ax.invert_yaxis()
    cb.set_label('Magnitude')

    plt.tight_layout()

    plt.savefig(figname, dpi=100)
    plt.close()

    return figname


def main():

    return


if __name__ == '__main__':
    main()
