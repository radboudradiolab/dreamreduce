import os
import argparse
import sys
import resource
import psutil

# set up log
import logging
import time
logfmt = ('%(asctime)s.%(msecs)03d [%(levelname)s, %(process)s] %(message)s '
          '[%(funcName)s, line %(lineno)d]')
datefmt = '%Y-%m-%dT%H:%M:%S'
logging.basicConfig(level='INFO', format=logfmt, datefmt=datefmt)
logFormatter = logging.Formatter(logfmt, datefmt)
#logging.Formatter.converter = time.gmtime #convert time in logger to UTC
log = logging.getLogger()

from dreamreduce import rawdir_monitor as rm
from dreamreduce import configuration as cfg

################################################################################

# from https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

################################################################################

def main():
    """Wrapper around dreamreduce to facilitate reductions"""

    parser = argparse.ArgumentParser(
        description='Start continuous DREAM processing')

    parser.add_argument('--camid', default=None,
                        help='camid; if not specified, it is inferred from the '
                        'environment variable HOSTNAME')
    parser.add_argument('--twilight', type=float, default=5,
                        help='sun altitude at which to start the night; default=5')
    parser.add_argument('--nbias', type=int, default=20,
                        help='number of bias frames to combine; default=20')
    parser.add_argument('--ndark', type=int, default=20,
                        help='number of dark frames to combine; default=20')
    parser.add_argument('--nflat', type=int, default=20,
                        help='number of flat frames to combine; default=20')
    parser.add_argument('--timeout', type=int, default=10,
                        help='number of exposures to wait for calibration files '
                        'to complete; default=nbias/ndark/nflat+10')
    #parser.add_argument('--step', type=float, default=6.4,
    #                    help='wait time for next exposure; default=6.4')
    args = parser.parse_args()


    if args.camid is None:

        # if camid is not specified, read it from the 6th character
        # of the hostname system variable
        hostname = os.environ.get('HOSTNAME')

        # combined with the siteinfo ID defined in cfg
        camid = '{}{}'.format(cfg.siteinfo['ID'], hostname[5].upper())

    else:
        camid = args.camid


    # check if camid is valid
    if camid not in cfg.camid_list:
        log.error ('{} not a valid camera ID; exiting'.format(camid))
        sys.exit()


    # start rawdir_monitor
    rm.rawdir_monitor_mp (camid, twilight=args.twilight,
                          nbias=args.nbias, ndark=args.ndark,
                          nflat=args.nflat, timeout=args.timeout)



################################################################################

if __name__ == "__main__":
    main()
