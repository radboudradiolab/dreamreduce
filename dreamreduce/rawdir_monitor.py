#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import glob
import time
import subprocess
import shutil
import datetime


#from multiprocessing import Pool, Manager
import multiprocessing as mp
mp_start_method_orig = mp.get_start_method()
# define mp start method through context (spawn, forkserver or fork)
mp_ctx = mp.get_context('fork')


# set up log
import logging
import time
logfmt = ('%(asctime)s.%(msecs)03d [%(levelname)s, %(process)s] %(message)s '
          '[%(funcName)s, line %(lineno)d]')
datefmt = '%Y-%m-%dT%H:%M:%S'
logging.basicConfig(level='INFO', format=logfmt, datefmt=datefmt)
logFormatter = logging.Formatter(logfmt, datefmt)
logging.Formatter.converter = time.gmtime #convert time in logger to UTC
log = logging.getLogger()

import numpy as np

from astropy.io import fits
from astropy.visualization import ZScaleInterval as zscale
from astropy.visualization import astropy_mpl_style
from astropy.time import Time

from dreamreduce import io
from dreamreduce import reduction
from dreamreduce import astrometry
from dreamreduce import configuration as cfg
from dreamreduce import summarize
from dreamreduce import misc
from dreamreduce import cdecor_vmag
from dreamreduce import zeropoints

import matplotlib
import matplotlib.pyplot as plt
plt.style.use(astropy_mpl_style)
matplotlib.rcParams.update({'font.size': 8})


# functions to let other modules define multiprocessing instances of
# Lock(), Queue() and Pool() while using mp start method defined at
# top of this module
def get_mp_Queue(*args):
    return mp_ctx.Queue(*args)

def get_mp_Pool(*args):
    return mp_ctx.Pool(*args)

def get_mp_Manager(*args):
    return mp_ctx.Manager(*args)


###############################################################################
# Helper functions.
###############################################################################


def get_datestring():

    t = datetime.datetime.utcnow()

    return t.strftime('%Y%m%d')


def listdir_fullpath(d):

    # return list of fits files in folder d
    return [os.path.join(d, f) for f in os.listdir(d) if '.fits' in f]


###############################################################################
# Placeholders.
###############################################################################

def combine_temporary_files(date, camid, dirtree):
    #log = logging.getLogger('dreamreduce')

    # Combine the fast lightcurve files.
    filelist = glob.glob(os.path.join(dirtree['tmp'], 'tmp_fast*.hdf5'))

    if (len(filelist) > 0):

        filename = 'fast_{}{}.hdf5'.format(date, camid)
        filename = os.path.join(dirtree['lightcurves'], filename)

        log.info('Combining {} temporary fast files, and writing results to {}'.format(
            len(filelist), filename))

        io.combine_photometry(filename, filelist)

        # TODO: re-enable cleanup
        # for filename in filelist:
        #    os.remove(filename)

    # Combine the slow lightcurve files.
    filelist = glob.glob(os.path.join(dirtree['tmp'], 'tmp_slow*.hdf5'))

    if (len(filelist) > 0):

        filename = 'slow_{}{}.hdf5'.format(date, camid)
        filename = os.path.join(dirtree['lightcurves'], filename)

        log.info('Combining {} temporary slow files, and writing results to {}'.format(
            len(filelist), filename))

        io.combine_photometry(filename, filelist)
        # TODO; re-enable cleanup
        # for filename in filelist:
        #    os.remove(filename)

    return


###############################################################################

def calibrate_photometry(date, camid, dirtree, systable, zpstable=None):

    #log = logging.getLogger('dreamreduce')

    # log memory usage
    misc.mem_use ('calibrate_photometry at start')


    # Get all fast lightcurves in products directory.
    log.info('Getting list of fast lightcurve files.')

    reddir = cfg.reddir
    filelist = glob.glob(os.path.join(
        reddir, '*/lightcurves/fast_*{}.hdf5'.format(camid)))


    # if reddir starts with /tmpdata, also look for files with
    # /tmpdata replaced by cfg.datadir; in rawdir_monitor mode the
    # lightcurves from previous nights will have moved there (possibly
    # also in rereduce)
    if 'tmpdata' in cfg.reddir:
        reddir = cfg.reddir.replace('tmpdata', 'data')
        if os.path.exists(reddir):
            filelist += glob.glob(os.path.join(
                reddir, '*/lightcurves/fast_*{}.hdf5'.format(camid)))


    # unique files
    filelist = np.unique(filelist)

    # sort filelist by date; N.B.: start of filenames can be a mix of
    # /tmpdata and /data, so need to sort on filename without the path
    idx_sort = np.argsort([fn.split('/')[-1] for fn in filelist])
    filelist = np.array(filelist)[idx_sort]


    if (len(filelist) == 0):
        log.warn('No lightcurves found.')
        return
    else:
        log.info ('lightcurves found: {}'.format(filelist))


    # Find fast lightcurves from past 15 nights.
    ndays = cfg.ndays_cal
    log.info('Obtaining fast lightcurves over the interval {} with respect to '
             'the current date {}'.format(cfg.ndays_cal, date))

    # current date in [yyyymmdd]
    date_c = datetime.datetime.strptime(date, '%Y%m%d')
    # start date to include
    date0 = (date_c + datetime.timedelta(days=ndays[0])).strftime('%Y%m%d')
    # end date to include
    date1 = (date_c + datetime.timedelta(days=ndays[1])).strftime('%Y%m%d')

    # dates in [yyyymmdd][camid]
    dates = np.array([fn.split('/')[-1].split('.hdf5')[0].split('fast_')[-1]
                      for fn in filelist])

    # select relevant dates
    mask = ((dates >= (date0 + camid)) & (dates <= (date1 + camid)))
    filelist = filelist[mask]


    if (len(filelist) == 0):
        log.warn('No lightcurves found in the past 15 nights.')
        return

    log.info ('Found {} good nights.'.format(len(filelist)))
    log.info ('lightcurves to be used: {}'.format(filelist))


    # Save the combined lightcurves.
    hdf5_cfastphot = 'cfast_{}{}.hdf5'.format(date, camid)
    hdf5_cfastphot = os.path.join(dirtree['sys'], hdf5_cfastphot)

    log.info('Combining lightcurves to be (temporarily) saved to {}'
             .format(hdf5_cfastphot))
    io.combine_photometry(hdf5_cfastphot, filelist, astrometry=False)


    misc.mem_use ('in calibrate_photometry after creating {}'
                  .format(hdf5_cfastphot))


    try:

        # Perform the coarse decorrelation.
        log.info('Running the coarse decorrelation.')
        syscal = cdecor_vmag.CoarseDecorVmag(hdf5_cfastphot, 0) # 0=aperture

        # Update the systable.
        log.info('Updating the systematics table.')
        io.update_table(syscal.sysfile, systable)

    except Exception as e:

        log.exception('following exception occurred in '
                      'cdecor_vmag.CoarseDecorVmag; sysfile was not created: {}'
                      .format(e))


    misc.mem_use ('calibrate_photometry after creating {}'
                  .format(syscal.sysfile))


    # in case zpstable is not None, also create the zeropoints file
    # that can be used in the alternative photometric calibration
    if zpstable is not None:

        # using the combined fast photometry file created by
        # io.combine_photometry above, create the combined zeropoints
        # calibration file

        try:

            # define combined zeropoints file
            fits_zps_comb = os.path.join(
                dirtree['zeropoints'], 'zps_comb_{}{}.fits'.format(date, camid))


            log.info ('creating combined zeropoints calibration fits table {} '
                      'from {}'.format(fits_zps_comb, hdf5_cfastphot))

            zeropoints.create_zeropoints_comb (hdf5_cfastphot, fits_zps_comb)


            # update zpstable with the newly created combined zeropoints file
            log.info('updating zpstable with combined zeropoints')
            io.update_table(fits_zps_comb, zpstable)


            misc.mem_use ('calibrate_photometry after creating {}'
                          .format(fits_zps_comb))

        except Exception as e:

            log.exception('following exception occurred in '
                          'zeropoints.create_zeropoints_comb; zpsfile was not '
                          'created: {}'.format(e))


        # this block below is the old method of first creating a
        # zeropoints table for the night, before combining it with the
        # existing combined one
        if False:

            # create zeropoints calibration file of single night set by [date]
            # from combined lightcurves file that was created in
            # [combine_temporary_files]
            hdf5_fast_lcs = '{}/fast_{}{}.hdf5'.format(dirtree['lightcurves'],
                                                       date, camid)

            # name of output fits table
            fits_zps_night = os.path.join(dirtree['zeropoints'],
                                          'zps_night_{}{}.fits'.format(date,
                                                                       camid))

            # create zeropoints table for this night
            log.info ('creating zeropoints calibration fits table {} using {}'
                      .format(fits_zps_night, hdf5_fast_lcs))
            zeropoints.create_zeropoints_night (hdf5_fast_lcs, fits_zps_night)


            # infer existing combined zeropoints file from zpstable; if
            # not, fits_zps_comb will be None
            fits_zps_comb = io.get_file(zpstable)

            # create combined zeropoints table
            if fits_zps_comb is not None and os.path.exists(fits_zps_comb):

                # define output combined zeropoints file
                fits_zps_comb_out = os.path.join(
                    dirtree['zeropoints'], 'zps_comb_{}{}.fits'.format(date,
                                                                       camid))

                log.info ('creating combined zeropoints calibration fits table '
                          '{} by combining {} and {}'
                          .format(fits_zps_comb_out, fits_zps_comb,
                                  fits_zps_night))

                zeropoints.update_zeropoints (fits_zps_comb, fits_zps_night,
                                              fits_zps_comb_out,
                                              ncpus=cfg.ncpus)

                # update zpstable with the newly created combined zeropoints file
                log.info('updating zpstable with combined zeropoints')
                io.update_table(fits_zps_comb_out, zpstable)

            else:

                # update zpstable with the night zeropoints file
                log.info('updating zpstable with night zeropoints')
                io.update_table(fits_zps_night, zpstable)



    # Remove the combined lightcurves.
    # CHECK!!! keep for the moment to test with!
    #if False:
    os.remove(hdf5_cfastphot)


    return


###############################################################################
# Main reduction loop.
###############################################################################

def build_dirtree(date, camid, reddir=None, tmpdir=None, arcdir=None,
                  mode_rereduce=False):

    log.info('Building directory tree for {}{}.'.format(date, camid))
    date_camid = '{}{}'.format(date, camid)

    if reddir is None:
        reddir = cfg.reddir

    if tmpdir is None:
        tmpdir = cfg.tmpdir

    if arcdir is None:
        if not mode_rereduce:
            arcdir = cfg.arcdir


    dirtree = dict()

    dirtree['red'] = os.path.join(reddir, date_camid)
    dirtree['tmp'] = tmpdir

    if arcdir is not None:
        dirtree['rawarchive'] = os.path.join(arcdir, date_camid)
    else:
        # in rereduce mode, arcdir can be None, so that the raw data
        # are not archived; should keep it to None in that case
        dirtree['rawarchive'] = None


    # subfolders of reddir
    for key in ['lightcurves', 'thumbs', 'sys', 'targets',
                # the following ones are new folders in dreamreduce
                'logs', 'masters', 'clouds', 'diff', 'zeropoints']:
        dirtree[key] = os.path.join(reddir, date_camid, key)


    # create directories
    for key in list(dirtree.keys()):
        try:
            os.makedirs(dirtree[key])
        except:
            log.warn('Directory exists, {}'.format(dirtree[key]))


    return dirtree


################################################################################

def parse_files_mp (filelist, nbias, ndark, nflat, use_all=False):

    filelist = np.sort(filelist)

    calnames = ['bias', 'dark', 'flat', 'blank']

    # Separate calibration and science frames.
    bias_frames = [fn for fn in filelist if 'bias' in fn]
    dark_frames = [fn for fn in filelist if 'dark' in fn]
    flat_frames = [fn for fn in filelist if 'flat' in fn]
    # blank frames are not used in the reduction at the moment
    blank_frames = [fn for fn in filelist if 'blank' in fn]

    for calname in calnames:
        filelist = [fn for fn in filelist if calname not in fn]

    science_frames = filelist

    # why not use all available bias/dark/flats, i.e. from evening and
    # morning calibrations? this is determined by input parameter
    # use_all
    if not use_all:
        bias_frames = bias_frames[:nbias]
        dark_frames = dark_frames[:ndark]
        flat_frames = flat_frames[:nflat]

    return science_frames, bias_frames, dark_frames, flat_frames


################################################################################

def rereduce_mp (rawdir, confdir, reddir, tmpdir, arcdir,
                 nbias=cfg.nbias, ndark=cfg.ndark, nflat=cfg.nflat,
                 socket2control=False, nocal=False):


    t1 = time.time()


    #log = logging.getLogger('rereduce')
    log.info('Starting rereduction')


    # Check if the raw directory exists.
    if not os.path.exists(rawdir):
        raise IOError('Directory {} does not exist.'.format(rawdir))


    # in case tmp folder exists and it is not empty,
    # remove it
    if os.path.exists(tmpdir) and len(os.listdir(tmpdir))>0:
        log.info ('removing directory {}'.format(tmpdir))
        shutil.rmtree(tmpdir)


    # Parse the raw location to obtain the date and camid.
    head, tail = os.path.split(rawdir)
    date, camid = tail[:8], tail[8:]


    # Create the dirtree for rereduction.
    dirtree = build_dirtree(date, camid, reddir=reddir, tmpdir=tmpdir,
                            arcdir=arcdir, mode_rereduce=True)


    # attach logfile
    logfile = os.path.join(dirtree['logs'], 'rereduce.log')
    misc.attach_log(log, logfile)
    log.info ('created logfile: {}'.format(logfile))


    # Get the siteinfo, darktables and astrometry.
    starcat = cfg.starcat
    targets = cfg.targets
    siteinfo = cfg.siteinfo


    # darktable, systable and zpstable
    darktable = os.path.join(confdir, 'darktable{}.dat'.format(camid))
    systable = os.path.join(confdir, 'systable{}.dat'.format(camid))
    zpstable = os.path.join(confdir, 'zpstable{}.dat'.format(camid))


    # Get the files to be processed.
    filelist = listdir_fullpath(rawdir)

    # Parse files.
    science_frames, bias_frames, dark_frames, flat_frames = parse_files_mp(
        filelist, nbias, ndark, nflat, use_all=True)

    if (len(bias_frames) > 0):
        reduction.reduce_bias_frames(camid, bias_frames, dirtree)

    if (len(dark_frames) > 0):
        reduction.reduce_dark_frames(
            camid, dark_frames, dirtree, darktable)

    if (len(flat_frames) > 0):
        reduction.reduce_flat_frames(
            camid, flat_frames, dirtree, darktable)


    misc.log_timing_memory (t1, label='rereduce_mp after creating '
                            'bias/dark/flat master frames')


    # read the catalogue
    cat = io.read_catalogue(cfg.starcat)


    # multiprocess science frames
    # ---------------------------

    # add some info to the log
    log.info ('rawdir             : {}'.format(rawdir))
    log.info ('reddir             : {}'.format(reddir))
    log.info ('tmpdir             : {}'.format(tmpdir))
    log.info ('arcdir             : {}'.format(arcdir))

    log.info ('starcat            : {}'.format(starcat))
    log.info ('targets            : {}'.format(targets))
    for key in siteinfo.keys():
        log.info ('{:18s} : {}'.format(key, siteinfo[key]))

    log.info ('camid              : {}'.format(camid))
    log.info ('ncpus              : {}'.format(cfg.ncpus))
    log.info ('mp start method    : {}'.format(mp_ctx.get_start_method()))

    log.info ('wcs_offset         : {}'.format(cfg.wcs_offset))
    log.info ('wcs_std            : {}'.format(cfg.wcs_std))
    log.info ('nimages_astro      : {}'.format(cfg.nimages_astro))
    log.info ('nimages_astromaster: {}'.format(cfg.nimages_astromaster))
    log.info ('initial pixscale   : {}'.format(cfg.pixscale))
    log.info ('pixscale_varyfrac  : {}'.format(cfg.pixscale_varyfrac))

    log.info ('maglim_astro       : {}'.format(cfg.maglim_astro))
    log.info ('maglim_fast        : {}'.format(cfg.maglim_fast))
    log.info ('aper_fast          : {}'.format(cfg.aper_fast))
    log.info ('skyrad             : {}'.format(cfg.skyrad))
    log.info ('hp_level           : {}'.format(cfg.hp_level))
    log.info ('imdiff_dseq_max    : {}'.format(cfg.imdiff_dseq_max))

    log.info ('nocal              : {}'.format(nocal))
    if nocal:
        log.warning('running rereduce without live_calibration')


    # limit number of frames for testing
    #science_frames = science_frames[100:110]

    # determine lists of leading and lstseq_leading; could simplify
    # this by updating function is_leading to accept a list of lstseq
    # rather than scalar
    leading_list = []
    leadfollow_list = []
    lstseq_leading_list = []
    for filename in science_frames:
        #lstseq = int(os.path.split(filename)[-1][:8])
        lstseq = int(filename.split('/')[-1].split(camid)[0])
        leading, lstseq_leading = is_leading(leading_list, lstseq)
        leadfollow_list.append(leading)
        lstseq_leading_list.append(lstseq_leading)


    # initialize list to be updated by all threads
    manager = mp_ctx.Manager()
    astro_done_list = manager.list([])
    lock = manager.Lock()

    mode_rereduce = True
    science_frames_red = pool_func (reduction.try_reduce_science_single,
                                    science_frames, camid, leadfollow_list,
                                    lstseq_leading_list, darktable,
                                    cat, siteinfo, systable, dirtree, zpstable,
                                    astro_done_list, socket2control, lock,
                                    mode_rereduce, nocal, nproc=cfg.ncpus)
    misc.log_timing_memory (t1, label='rereduce_mp after pool_func')



    # Combine temporary lightcurve files.
    log.info('Combining temporary lightcurves.')
    combine_temporary_files(date, camid, dirtree)
    misc.log_timing_memory (t1, label='rereduce_mp after combining '
                            'temporary lightcurve files')


    # Send summary email.
    summarize.reduction_summary(dirtree['lightcurves'])


    # Run daytime calibration.
    log.info('Running daytime calibration.')
    calibrate_photometry(date, camid, dirtree, systable, zpstable=zpstable)
    misc.log_timing_memory (t1, label='rereduce_mp after daytime calibraton')


    # using [header_wcs] instead of astromaster[1] inside
    # [calibration_summary]; use header of image close to midnight UT
    name = cfg.astromaster[camid]
    if os.path.exists(name):
        # load the astromaster file
        astromaster_headers = np.load(name, allow_pickle=True).item()
        # determine approximate lst at midnight on date
        date_obs = Time('{}-{}-{}T23:59'
                        .format(date[0:4], date[4:6], date[6:8]))
        lst_hrs = date_obs.sidereal_time('apparent',
                                         longitude=siteinfo['lon']).hour
        # find the nearest key in astromasters_headers to lst_hrs
        lst_array = np.array([d['LST']
                              for d in astromaster_headers.values()])
        # absolute difference, taking care of the discontinuity at 24
        dlst = np.abs((lst_array - lst_hrs + 12) % 24 - 12)
        # minimum index and corresponding key
        i_min = dlst.argmin()
        key = list(astromaster_headers.keys())[i_min]
        # extract corresponding header
        header_wcs = astromaster_headers[key]
    else:
        header_wcs = None


    # Send summary email.
    summarize.calibration_summary(dirtree['sys'], header_wcs)


    # jpeg
    # ----
    for folder in [dirtree['tmp'], dirtree['diff'], dirtree['masters']]:

        # check if folder exists
        if os.path.exists(folder):

            log.info ('jpegging fits files in {}'.format(folder))
            list_jpg = glob.glob('{}/*.fits*'.format(folder))
            # split list into cfg.ncpus lists with roughly equal lengths
            # using misc.split_list
            lists_jpg = misc.split_list(list_jpg, cfg.ncpus)
            pool_func (create_jpg, lists_jpg, nproc=cfg.ncpus)


    # copy the reduced jpegs to dirtree['thumbs']
    list_jpg = glob.glob('{}/*_red.jpg'.format(dirtree['tmp']))
    dst = dirtree['thumbs']
    for src in list_jpg:
        shutil.copy2 (src, dst)



    # remove tmp folder if not kept
    # -----------------------------
    if not cfg.keep_tmp and os.path.exists(tmpdir):
        log.info ('removing directory {}'.format(tmpdir))
        shutil.rmtree(tmpdir)



    # fpack
    # -----
    for folder in [dirtree['tmp'], dirtree['diff'], dirtree['masters'],
                   dirtree['rawarchive']]:

        # rawarchive folder may be None and tmp folder may not exist
        # anymore
        if folder is not None and os.path.exists(folder):

            log.info ('fpacking fits files in {}'.format(folder))
            list_fpack = glob.glob('{}/*.fits'.format(folder))
            # split list into cfg.ncpus lists with roughly equal lengths
            # using misc.split_list
            lists_fpack = misc.split_list(list_fpack, cfg.ncpus)
            # multiprocess
            pool_func (fpack, lists_fpack, nproc=cfg.ncpus)



    # close logfile
    misc.log_timing_memory (t1, label='rereduce_mp at end')
    misc.close_log(log, logfile)


    return


################################################################################

def is_leading (leading_list, lstseq):

    """returns True if lstseq corresponds to that of a leading image,
       i.e. the first in a sequence of [cfg.nimages_astro] images;
       returns False otherwise. The input list is updated with the
       lstseq of any new leading image.

    """

    if len(leading_list) == 0:
        leading = True

    else:
        dlstseq = lstseq - np.array(leading_list)
        index = np.nonzero((dlstseq > 0) & (dlstseq < cfg.nimages_astro))[0]
        if len(index) == 1:
            leading = False
            lstseq_leading = leading_list[index[0]]
        else:
            leading = True

    if leading:
        lstseq_leading = lstseq
        if lstseq not in leading_list:
            leading_list.append(lstseq)


    return leading, lstseq_leading


################################################################################

def fpack (filenames):

    """fpack list of fits images; skip fits tables"""

    # if input filenames is a string with a single filename, convert
    # it to a single-item list
    if isinstance(filenames, str):
        filenames = [filenames]

    # empty list for output filenames
    filenames_out = []

    # loop filenames
    for filename in filenames:

        try:

            # fits check if extension is .fits and not an LDAC fits file
            if filename.split('.')[-1] == 'fits' and '_ldac.fits' not in filename:

                # read header
                with fits.open(filename) as hdulist:
                    header = hdulist[-1].header

                # check if it is an image
                if int(header['NAXIS'])==2 and 'XTENSION' not in header:

                    log.info ('fpacking {}'.format(filename))

                    # determine if integer or float image
                    if int(header['BITPIX']) > 0:
                        cmd = ['fpack', '-D', '-Y', filename]
                    else:
                        quant = 16
                        cmd = ['fpack', '-q', str(quant), '-D', '-Y', filename]


                    # if output fpacked file already exists, delete it
                    filename_packed = '{}.fz'.format(filename)
                    if os.path.exists(filename_packed):
                        os.remove(filename_packed)
                        log.warning ('fpacking over already existing file {}'
                                     .format(filename_packed))

                    subprocess.run(cmd)
                    filenames_out.append(filename_packed)


        except Exception as e:
            #log.exception (traceback.format_exc())
            log.exception ('exception was raised in fpacking of image {}: {}'
                           .format(filename,e))


    # if input list was a single file, return that filename as a string instead
    # of a list
    if len(filenames_out) == 1:
        return filenames_out[0]
    else:
        return filenames_out


################################################################################

def create_jpg (filenames, cmap='gray'):

    """Create jpg images from list of fits files"""

    # if input filenames is a string with a single filename, convert
    # it to a single-item list
    if isinstance(filenames, str):
        filenames = [filenames]

    # loop filenames
    for filename in filenames:

        try:

            image_jpg = '{}.jpg'.format(filename.split('.fits')[0])

            if not os.path.isfile(image_jpg):

                # read input image
                with fits.open (filename, memmap=True) as hdulist:
                    data = hdulist[-1].data
                    header = hdulist[-1].header


                # check if it is an image
                if int(header['NAXIS'])==2 and 'XTENSION' not in header:


                    file_str = image_jpg.split('/')[-1].split('.jpg')[0]
                    title = ('file: {} '.format(file_str))


                    fig = plt.figure()
                    vmin, vmax = zscale(contrast=0.25).get_limits(data)
                    plt.imshow(data, cmap=cmap, vmin=vmin, vmax=vmax, origin='lower')
                    plt.colorbar(fraction=0.031, pad=0.04)
                    plt.title(title, fontsize=10)
                    plt.xlabel('X (pixels)')
                    plt.ylabel('Y (pixels)')
                    plt.grid(None)
                    plt.tight_layout()
                    log.info ('saving {} to {}'.format(filename, image_jpg))
                    plt.savefig(image_jpg, dpi=175)
                    plt.close()


        except Exception as e:
            #log.exception (traceback.format_exc())
            log.exception ('exception was raised in creating jpg of image {}: {}'
                           .format(filename,e))


    return


################################################################################

def rsync (src, dst):

    """rsync using the shell"""

    try:
        cmd = ['rsync', '-av', src, dst]
        subprocess.run(cmd)

    except Exception as e:
        log.exception ('exception occcurred in rsync command: {}: {}'
                       .format(cmd, e))


    return


################################################################################

def pool_func (func, itemlist, *args, nproc=1, args_lists=True):

    try:
        # list to record the results
        results = []
        # create pool of workers
        #pool = Pool(nproc)
        pool = mp_ctx.Pool(nproc)
        # loop items in itemlist
        for i, item in enumerate(itemlist):
            args_tmp = [item]
            # add any additional arguments
            for arg in args:
                # if args_lists is True and an argument is a list with
                # same number of entries as itemlist, provide the
                # corresponding element
                if (args_lists and isinstance(arg, list) and
                    len(arg)==len(itemlist)):
                    args_tmp.append(arg[i])
                else:
                    # otherwise provide entire arg
                    args_tmp.append(arg)

            # issue function and arguments to the worker pool
            results.append(pool.apply_async(func, args_tmp))

        # close the pool
        pool.close()
        # wait for the worker processes to exit
        pool.join()
        # get the results
        results = [r.get() for r in results]

    except Exception as e:
        log.exception ('exception was raised during [pool.apply_async({})]: '
                       '{}'.format(func, e))

    return results


################################################################################

def rawdir_monitor_mp (camid, twilight=cfg.twilight, nbias=cfg.nbias,
                       ndark=cfg.ndark, nflat=cfg.nflat, timeout=10, step=6.4,
                       socket2control=True):


    #log = logging.getLogger('dreamreduce')
    log.info('Initializing main reduction loop for camera {}.'.format(camid))


    # create pool of workers
    #pool = Pool(cfg.ncpus)
    pool = mp_ctx.Pool(cfg.ncpus)
    #astro_done_list = Manager().list([])
    manager = mp_ctx.Manager()
    astro_done_list = manager.list([])
    lock = manager.Lock()

    # list of leading lstseq
    list_leading = []


    # read the catalogue
    cat = io.read_catalogue(cfg.starcat)


    # Set up the listener.
    siteinfo = cfg.siteinfo
    darktable = cfg.darktable[camid]
    systable = cfg.systable[camid]
    zpstable = cfg.zpstable[camid]
    rawdir = cfg.rawdir


    in_queue = set()
    # CHECK!!! - set to False for testing
    day_tasks_finished = True
    bias_time = 0
    dark_time = 0
    flat_time = 0
    mdark_present = False
    niter_day = 0


    # See if the reduction is being restarted.
    queuefiles = glob.glob(os.path.join(cfg.tmpdir, '20??????.txt'))

    if (len(queuefiles) == 1):

        log.info('Restarting night.')
        queuefile = queuefiles[0]


        # Get directory tree for this night.
        head, tail = os.path.split(queuefile)
        date = tail.rstrip('.txt')
        dirtree = build_dirtree(date, camid)


        # attach logfile
        logfile = os.path.join(dirtree['logs'], 'rawdir_monitor.log')
        misc.attach_log(log, logfile)


        # Get processed files for this night.
        in_queue = io.read_in_queue(queuefile)


        # Get contents of rawdir and add any processed files to the archive.
        filelist = listdir_fullpath(rawdir)
        filelist = in_queue.intersection(set(filelist))
        if (len(filelist) > 0):
            io.archive_files(list(filelist), dirtree['rawarchive'])

        day_tasks_finished = False


        # log memory usage
        misc.mem_use ()


    elif (len(queuefiles) == 0):
        queuefile = None

    else:
        log.warning(
            'Found multiple .txt files in tmp directory, unknown situation.')
        exit()


    # start continuous/infinite loop
    while True:

        # Get the sun altitude.
        #log.info('Computing current sun position.')
        ra, dec, sun_alt = astrometry.sun_position(siteinfo)


        # Obtain the current list of raw files.
        #log.info('Getting filelist.')
        filelist = listdir_fullpath(rawdir)
        filelist = set(filelist)


        # See which files are not in the processing queue yet.
        #log.info('Getting unprocessed files.')
        # N.B.: in_queue is a set with all processed files from the
        # night, which is also written to queuefile so that the files
        # already processed can be recovered in case the processing is
        # interrupted - see block with len(queuefiles)==1 above; filelist
        # contains all files in rawdir, and new_files are the files in
        # filelist which are not in in_queue
        new_files = filelist.difference(in_queue)
        new_files = list(new_files)


        # CHECK!!! if new condition len(new_files) > 0 is working
        # fine; previously this was handled in the block below with
        # sun_alt > twilight by continuing to processing remaining raw
        # files
        #
        # it is not working if files arrive before twilight; once they
        # are processed, the night will considered finished while it
        # has not even started yet
        if sun_alt < twilight: # or len(new_files) > 0:

            if day_tasks_finished:

                # in case tmp folder exists and it is not empty,
                # remove it
                if os.path.exists(cfg.tmpdir) and len(os.listdir(cfg.tmpdir))>0:
                    log.info ('removing directory {}'.format(cfg.tmpdir))
                    shutil.rmtree(cfg.tmpdir)


                # Build directory tree for the current night.
                date = get_datestring()

                # CHECK!!! - pretend it's 20211121
                #date = '20211121'

                dirtree = build_dirtree(date, camid)


                # attach logfile
                logfile = os.path.join(dirtree['logs'], 'rawdir_monitor.log')
                misc.attach_log(log, logfile)
                log.info ('created logfile: {}'.format(logfile))


                log.info('Sun altitude {:.2f} < twilight'.format(sun_alt))
                log.info('Beginning of new night: {}'.format(date))

                # Write a queuefile to indicate the night has begun.
                queuefile = os.path.join(dirtree['tmp'], date + '.txt')
                io.write_in_queue(queuefile, set())

                day_tasks_finished = False

                # log memory usage
                misc.mem_use ()


            if len(new_files) == 0:
                # if there are no new files, wait for a second and
                # continue at the top of while True loop
                time.sleep(0.5)
                continue


            # Parse files.
            #log.info('Parsing files.')
            science_frames, bias_frames, dark_frames, flat_frames = \
                parse_files_mp(new_files, nbias, ndark, nflat)

            log.info('frames parsed of type science:{} bias:{} dark:{} flat:{} '
                     .format(len(science_frames), len(bias_frames),
                             len(dark_frames), len(flat_frames)))


            # combine bias frames
            # -------------------
            # if required number has been reached or number of
            # iterations (bias_time) larger than input nbias +
            # timeout, where timeout unit is iterations, not seconds
            if len(bias_frames) == nbias or bias_time >= nbias + timeout:

                # Reduce the frames.
                reduction.reduce_bias_frames(camid, bias_frames, dirtree)

                # update in_queue with processed files
                in_queue.update(bias_frames)
                io.write_in_queue(queuefile, in_queue)
                io.archive_files(bias_frames, dirtree['rawarchive'])

                bias_time = 0

            elif (len(bias_frames) > 0):

                if (bias_time == 0):
                    bias_time = len(bias_frames)
                else:
                    bias_time += 1

                # wait for step seconds
                time.sleep(step)


            # same for darks
            # --------------
            if len(dark_frames) == ndark or dark_time >= ndark + timeout:

                # Reduce the frames.
                reduction.reduce_dark_frames(
                    camid, dark_frames, dirtree, darktable)

                # update in_queue with processed files
                in_queue.update(dark_frames)
                io.write_in_queue(queuefile, in_queue)
                io.archive_files(dark_frames, dirtree['rawarchive'])

                dark_time = 0

                # boolean indicating that master dark has been made so
                # that flats can be processed as well
                mdark_present = True


            elif (len(dark_frames) > 0):

                if (dark_time == 0):
                    dark_time = len(dark_frames)
                else:
                    dark_time += 1

                # wait for step seconds
                time.sleep(step)


            # same for flats
            # --------------
            # except that flats require master dark to be present,
            # while the flats will likely be taken before the darks
            if (mdark_present and
                # in case flats are taken before the darks, need a generous
                # timeout, so added 60 epochs/iterations
                len(flat_frames) == nflat or flat_time >= nflat + timeout + 60):

                # Reduce the frames.
                reduction.reduce_flat_frames(
                    camid, flat_frames, dirtree, darktable)

                # update in_queue with processed files
                in_queue.update(flat_frames)
                io.write_in_queue(queuefile, in_queue)
                io.archive_files(flat_frames, dirtree['rawarchive'])

                flat_time = 0

                # set mdark_present to False again, so that for the
                # next set of calibration files the most recent dark
                # is used to reduce the flats
                mdark_present = False


            elif (len(flat_frames) > 0):

                if (flat_time == 0):
                    flat_time = len(flat_frames)
                else:
                    flat_time += 1


                # only wait for step seconds if that was not already
                # done under the dark block above; in case the flats
                # are waiting for the master dark, both the number of
                # darks and flats are nonzero, so would wait for 2
                # steps
                if len(dark_frames) == 0:
                    # wait for step seconds
                    time.sleep(step)

                    if len(flat_frames) == nflat:
                        log.info ('waiting for the master dark, needed to '
                                  'reduce the flats')



            # reduce science frames using pool of workers
            # -------------------------------------------
            if len(science_frames) > 0:

                for filename in science_frames:

                    # determine if image is leading or not
                    #lstseq = int(os.path.split(filename)[-1][:8])
                    lstseq = int(filename.split('/')[-1].split(camid)[0])
                    leading, lstseq_leading = is_leading (list_leading, lstseq)

                    log.info ('submitting {} to multiprocessing pool'
                              .format(filename))

                    # reduce
                    pool.apply_async(reduction.try_reduce_science_single,
                                     [filename, camid, leading, lstseq_leading,
                                      darktable, cat, siteinfo, systable,
                                      dirtree, zpstable, astro_done_list,
                                      socket2control, lock])


                # update in_queue with processed files
                in_queue.update(science_frames)
                io.write_in_queue(queuefile, in_queue)
                # archiving of science frames is done inside function
                # reduce_science_single
                #io.archive_files(science_frames, dirtree['rawarchive'])



        elif sun_alt >= twilight and not day_tasks_finished:

            # CHECK!!! - should we check for any remaining files to be
            # reduced after sun has reached the altitude set by
            # twilight? As is done in the original function
            # rawdir_monitor. Or simply set twilight such that no new
            # files will be taken afterwards?

            log.info('End of night and finished processing raw files; starting '
                     'daytime tasks.')


            # Combine temporary lightcurve files.
            log.info('Combining temporary lightcurves.')
            combine_temporary_files(date, camid, dirtree)


            # remove queuefile
            os.remove(queuefile)


            # Send summary email.
            summarize.reduction_summary(dirtree['lightcurves'])


            # Run daytime calibration.
            log.info('Running daytime calibration.')
            calibrate_photometry(date, camid, dirtree, systable,
                                 zpstable=zpstable)


            # using [header_wcs] instead of astromaster[1] inside
            # [calibration_summary]; use header of image close to midnight UT
            name = cfg.astromaster[camid]
            if os.path.exists(name):
                # load the astromaster file
                astromaster_headers = np.load(name, allow_pickle=True).item()
                # determine approximate lst at midnight on date
                date_obs = Time('{}-{}-{}T23:59'
                                .format(date[0:4], date[4:6], date[6:8]))
                lst_hrs = date_obs.sidereal_time('apparent',
                                                 longitude=siteinfo['lon']).hour
                # find the nearest key in astromasters_headers to lst_hrs
                lst_array = np.array([d['LST']
                                      for d in astromaster_headers.values()])
                # absolute difference, taking care of the discontinuity at 24
                dlst = np.abs((lst_array - lst_hrs + 12) % 24 - 12)
                # minimum index and corresponding key
                i_min = dlst.argmin()
                key = list(astromaster_headers.keys())[i_min]
                # extract corresponding header
                header_wcs = astromaster_headers[key]
            else:
                header_wcs = None


            # Send summary email.
            summarize.calibration_summary(dirtree['sys'], header_wcs)


            # fpack relevant files, archive folders to /data and
            # delete older ones using fpack_jpeg_archive_cleanup function
            fpack_jpeg_archive_cleanup (dirtree, date, camid)


            log.info('Finished daytime tasks.')

            # close and join pool of workers so that they can be
            # restarted below, just in case one or more workers got
            # stalled
            pool.close()
            pool.join()

            # close the manager
            manager.close()


            # log memory usage
            misc.mem_use ()


            # remove files in raw folder
            if os.path.exists(cfg.rawdir):
                log.info ('removing files in directory {}'.format(cfg.rawdir))
                for filename in listdir_fullpath(cfg.rawdir):
                    os.remove(filename)


            # remove tmp folder
            if os.path.exists(cfg.tmpdir):
                log.info ('removing directory {}'.format(cfg.tmpdir))
                shutil.rmtree(cfg.tmpdir)



            # Reset.
            day_tasks_finished = True
            bias_time = 0
            dark_time = 0
            flat_time = 0
            mdark_present = False
            niter_day = 0
            in_queue = set()


            # refresh pool of workers
            #pool = Pool(cfg.ncpus)
            pool = mp_ctx.Pool(cfg.ncpus)

            # and the manager
            manager = mp_ctx.Manager()
            astro_done_list = manager.list([])
            lock = manager.Lock()


            # list of leading lstseq
            list_leading = []

            # detach logfile from logging
            misc.close_log(log, logfile)


        elif day_tasks_finished:

            # only show message every ~10min
            if niter_day % 100 == 0:
                log.info('Waiting for sunset; current sun altitude: {:5.2f}'
                         .format(sun_alt))

            # wait for step seconds
            time.sleep(step)

            niter_day += 1

        else:
            log.warn('Encountered unknown situation.')


    return


################################################################################

def fpack_jpeg_archive_cleanup (dirtree, date, camid):


    # fpack
    # -----
    # use [pool_func] to fpack raw, difference and master
    # images - all still in /tmpdata
    for folder in [dirtree['rawarchive'], dirtree['diff'],
                   dirtree['masters']]:

        # check if folder exists
        if os.path.exists(folder):

            log.info ('fpacking fits files in {}'.format(folder))
            list_fpack = glob.glob('{}/*.fits'.format(folder))
            # split list into cfg.ncpus lists with roughly equal lengths
            # using misc.split_list
            lists_fpack = misc.split_list(list_fpack, cfg.ncpus)
            # multiprocess
            pool_func (fpack, lists_fpack, nproc=cfg.ncpus)



    # jpeg
    # ----
    for folder in [dirtree['tmp'], dirtree['diff'], dirtree['masters']]:

        # check if folder exists
        if os.path.exists(folder):

            log.info ('jpegging fits files in {}'.format(folder))
            list_jpg = glob.glob('{}/*.fits*'.format(folder))
            # split list into cfg.ncpus lists with roughly equal lengths
            # using misc.split_list
            lists_jpg = misc.split_list(list_jpg, cfg.ncpus)
            pool_func (create_jpg, lists_jpg, nproc=cfg.ncpus)


    # copy the reduced jpegs to dirtree['thumbs']
    list_jpg = glob.glob('{}/*_red.jpg'.format(dirtree['tmp']))
    dst = dirtree['thumbs']
    for src in list_jpg:
        shutil.copy2 (src, dst)



    # archive
    # -------
    def help_rsync (src):
        if src is not None:
            basedir = '/{}'.format(src.split('/')[1])
            dst = src.replace(basedir, cfg.datadir)
            # remove [date][camid] folder from dst folder
            dst = os.path.dirname(dst)
            log.info ('rsyncing {} to {}'.format(src, dst))
            rsync (src, dst)


    # rsync rawarchive images and products from /tmpdata to /data
    help_rsync (dirtree['rawarchive'])
    help_rsync (dirtree['red'])



    # backup relevant files from /tmpdate/conf to /data/conf/[date][camid]
    dst = '/data/conf/{}{}'.format(date, camid)
    if not os.path.exists(dst):
        os.makedirs(dst)

    # select all files with camid in their name and copy them - those
    # are the files that are updated on a daily basis
    files2copy = glob.glob('{}/*{}*'.format(dirtree['conf'], camid))
    io.archive_files(files2copy, dst, delete=False)



    # cleanup
    # -------

    # remove different folders depending on their age, following the
    # dictionary cfg.folder_age_limit
    for key, value in cfg.folder_age_limit.items():

        # add full path to all subfolders of key
        folder_list = ['{}/{}'.format(key, t[0]) for t in os.walk(key)]

        # loop folder_list
        for folder in folder_list:

            # do not consider the base folder, which is equal to key
            if folder == key:
                continue

            # determine the age of the subfolder using function
            # folder_age_days
            age = folder_age_days (folder)

            # if value is not a dictionary, then value = age limit
            remove = False
            if not isinstance(value, dict):
                # remove the folder if its age is greater than value
                age_limit = value
                if age > age_limit:
                    remove = True

            else:
                # if value is a dictionary, loop its keys
                for subfolder, age_limit in value.items():
                    if subfolder in folder and age > age_limit:
                        remove = True
                        # no need to check any of the remaining
                        # subfolders
                        break

            if remove:
                log.info ('removing directory: {}'.format(folder))
                shutil.rmtree(folder)
            else:
                log.info ('keeping directory: {}'.format(folder))


    return


################################################################################

def folder_age_days (folder):

    # extract date from folder name, assuming any digit in the name is
    # part of the date
    date = ''.join([c for c in folder if c.isdigit()])

    # if date does not contain 8 digits, set age to zero
    if len(date) != 8:
        age = 0
    else:
        try:
            # make date legible for Time and set it to just before midnight
            date_midnight = '{}-{}-{}T23:59'.format(date[0:4], date[4:6],
                                                    date[6:8])
            # age in days as a float
            age = (Time.now() - Time(date_midnight)).value
        except:
            # in case of exception, set age to zero
            log.warning ('exception in [folder_age_days] for folder {}; its age '
                         'is set to zero'.format(folder))
            age = 0


    return age


################################################################################

def main():
    return


if __name__ == '__main__':
    #logging.basicConfig(
    #    filename='/home/talens/MASCARA/lsreduce/example.log', level=logging.DEBUG)
    #log = logging.getLogger('lsreduce')
    main()
