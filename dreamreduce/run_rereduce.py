import os
import argparse
import sys
import resource
import psutil

# set up log
import logging
import time
logfmt = ('%(asctime)s.%(msecs)03d [%(levelname)s, %(process)s] %(message)s '
          '[%(funcName)s, line %(lineno)d]')
datefmt = '%Y-%m-%dT%H:%M:%S'
logging.basicConfig(level='INFO', format=logfmt, datefmt=datefmt)
logFormatter = logging.Formatter(logfmt, datefmt)
#logging.Formatter.converter = time.gmtime #convert time in logger to UTC
log = logging.getLogger()

from astropy.time import Time
from astropy import units as u

from dreamreduce import rawdir_monitor as rm
from dreamreduce import io
from dreamreduce import misc
from dreamreduce import configuration as cfg


################################################################################

# from https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

################################################################################

def main():
    """Wrapper around dreamreduce to facilitate reductions"""

    parser = argparse.ArgumentParser(
        description='(Re)reduce night of DREAM data')

    parser.add_argument('rawdir',
                        help='input directory with raw files to process; string '
                        'is required to end with yyyymmdd[camid], e.g. '
                        '\'/data/rawarchive/20231119CPC\')')
    parser.add_argument('--sysfile', default=None,
                        help='photometric calibration sysfile from previous '
                        'night, e.g. '
                        '\'/data/products/20231118CPC/sys/sys0_vmag_20231118CPC.hdf5\''
                        '; default is to use the top entry in systable in '
                        'confdir (see below); if explicitly set to the string '
                        '\'None\' then rereduce is run without sysfile'
                        )
    parser.add_argument('--zpsfile', help='combined zeropoints calibration fits '
                        'table to use, e.g. '
                        '\'/data/products/20231118LSC/zeropoints/zps_comb_20231118CPC.fits\''
                        '; default is to use the top entry in '
                        'zpstable in confdir (see below), if explicitly set to '
                        'the string \'None\' then rereduce is run without zpsfile'
                        )
    parser.add_argument('--confdir', default='./conf',
                        help='input directory with configuration files; '
                        'default=\'./conf\'')
    parser.add_argument('--reddir', type=str, default='./products',
                        help='output directory to write products; '
                        'default=\'./products\'')
    parser.add_argument('--tmpdir', type=str, default='./tmp',
                        help='directory to write temporary files; '
                        'default=\'./tmp\'')
    parser.add_argument('--arcdir', type=str, default=None,
                        help='directory to archive files; default=None; if left '
                        'at the default, the raw data with WCS header will not '
                        'be saved')

    args = parser.parse_args()


    # remove trailing slashes if present
    rawdir = args.rawdir.rstrip('/')
    confdir = args.confdir.rstrip('/')
    reddir = args.reddir.rstrip('/')
    tmpdir = args.tmpdir.rstrip('/')
    sysfile = args.sysfile
    zpsfile = args.zpsfile
    arcdir = args.arcdir

    # log memory usage and initialize timing
    misc.mem_use ('run_rereduce at start')
    t0 = time.time()


    # extract camid from the last three characters of rawdir
    camid = rawdir[-3:]

    # check if camid is valid
    if camid not in cfg.camid_list:
        log.error ('{} not a valid camera ID; exiting'.format(camid))
        sys.exit()


    # different possibilities for sysfile to use
    # ------------------------------------------
    systable = os.path.join(confdir, 'systable{}.dat'.format(camid))
    if sysfile is None:

        # top entry in systable will be used; check if it exists
        sysfile = io.get_file(systable)
        if sysfile is None or not os.path.exists(sysfile):
            log.error ('sysfile {} inferred from systable {} not found; '
                       'exiting'.format(sysfile, systable))
            raise SystemExit
        else:
            log.info ('using sysfile {}'.format(sysfile))


    elif sysfile == 'None':
        log.warning('running rereduce without original sysfile calibration')
        # remove systable from conf folder if it exists
        if os.path.exists(systable):
            log.info ('removing {}'.format(systable))
            os.remove(systable)

    elif os.path.exists(sysfile):

        # update systable.dat in conf folder with sysfile
        log.info ('updating {} with sysfile {}'.format(systable, sysfile))
        io.update_table(sysfile, systable)

    else:
        log.error ('sysfile {} not found and not set to \'None\'; exiting'
                   .format(syfile))
        raise SystemExit


    # different possibilities for zpsfile to use
    # ------------------------------------------
    zpstable = os.path.join(confdir, 'zpstable{}.dat'.format(camid))
    if zpsfile is None:

        # top entry in zpstable will be used; check if it exists
        zpsfile = io.get_file(zpstable)
        if zpsfile is None or not os.path.exists(zpsfile):
            log.error ('zpsfile {} inferred from zpstable {} not found; '
                       'exiting'.format(zpsfile, zpstable))
            raise SystemExit
        else:
            log.info ('using zpsfile {}'.format(zpsfile))


    elif zpsfile == 'None':
        log.warning('running rereduce without alternative zeropoints '
                    'calibration')
        # remove zpstable from conf folder if it exists
        if os.path.exists(zpstable):
            log.info ('removing {}'.format(zpstable))
            os.remove(zpstable)

    elif os.path.exists(zpsfile):

        # update zpstable.dat in conf folder with zpsfile
        log.info ('updating {} with zpsfile {}'.format(zpstable, zpsfile))
        io.update_table(zpsfile, zpstable)

    else:
        log.error ('zpsfile {} not found and not set to \'None\'; exiting'
                   .format(syfile))
        raise SystemExit



    # if both sysfile and zpsfile are None, turn off live_cabribation
    # through parameter nocal; if one of them is None, the
    # corresponding live_calibration should be performed
    if sysfile == 'None' and zpsfile == 'None':
        nocal = True
    else:
        nocal = False


    # process files in [rawdir]
    try:
        rm.rereduce_mp (rawdir, confdir, reddir, tmpdir, arcdir, nocal=nocal)
    except Exception as e:
        log.exception(e)


    # log memory usage and runtime
    misc.mem_use ('run_rereduce at end')
    log.info ('total wall-time spent reducing {}: {:.2f} hours'
              .format(rawdir, (time.time()-t0)/3600))


################################################################################

if __name__ == "__main__":
    main()
