# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 16:19:48 2016

@author: talens
"""

import numpy as np

import os
import sys
import resource
import psutil
import subprocess

# set up log
import logging
import time
logfmt = ('%(asctime)s.%(msecs)03d [%(levelname)s, %(process)s] %(message)s '
          '[%(funcName)s, line %(lineno)d]')
datefmt = '%Y-%m-%dT%H:%M:%S'
logging.basicConfig(level='INFO', format=logfmt, datefmt=datefmt)
logFormatter = logging.Formatter(logfmt, datefmt)
logging.Formatter.converter = time.gmtime #convert time in logger to UTC
log = logging.getLogger()


################################################################################

def flux2mag (flux, eflux=None, m0=25):

    # convert flux to numpy array if needed
    if not isinstance(flux, np.ndarray):
        flux = np.array(flux)

    # make sure to take log of positive fluxes
    mask_pos = (flux > 0)
    mag = np.zeros_like(flux) + m0
    mag[mask_pos] = m0 - 2.5*np.log10(flux[mask_pos])

    if eflux is not None:
        if not isinstance(eflux, np.ndarray):
            eflux = np.array(eflux)

        pogson = 2.5 / np.log(10)
        emag = pogson * np.abs(eflux/flux)
        return mag, emag
    else:
        return mag


################################################################################

def mag2flux (mag, emag=None, m0=25.0):

    # convert magnitude to flux; if m0=23.9 and input mag is AB,
    # resulting flux will be in microJy

    if not isinstance(mag, np.ndarray):
        mag = np.array(mag)

    # infer flux
    flux = 10**(-0.4 * (mag - m0))

    if emag is not None:
        if not isinstance(emag, np.ndarray):
            emag = np.array(emag)

        # determined corresponding error
        pogson = 2.5 / np.log(10)
        eflux = emag * flux / pogson
        return flux, eflux
    else:
        return flux


################################################################################

def flux2mag_alt (flux, eflux=None, m0=25):

    # check if flux is a scalar
    if isinstance(flux, (float, np.floating)):

        # in case flux is a scalar
        if flux > 0:
            mag = m0 - 2.5*np.log10(flux)
        else:
            mag = m0

    else:

        # make sure to take log of positive fluxes
        mask_pos = (flux > 0)
        mag = np.zeros_like(flux) + m0
        mag[mask_pos] = m0 - 2.5*np.log10(flux[mask_pos])


    if eflux is not None:
        emag = 2.5/np.log(10.)*np.abs(eflux/flux)
        return mag, emag

    return mag


def sigma_clip (values, sigma=5., maxiter=5):

    mask = np.ones(len(values), dtype='bool')

    for i in range(maxiter):

        m0 = np.nanmean(values[mask])
        m1 = np.nanstd(values[mask])

        mask = (np.abs(values - m0) < sigma*m1)

    return m0, m1


def clip_rectangle(x, y, nx, ny, margin):

    maskx = (x > margin) & (x < (nx - margin))
    masky = (y > margin) & (y < (ny - margin))

    return maskx & masky


################################################################################

def gzip (filenames):

    """gzip list of fits files using the shell"""

    # if input filenames is a string with a single filename, convert
    # it to a single-item list
    if isinstance(filenames, str):
        filenames = [filenames]

    # empty list for output filenames
    filenames_out = []

    # loop filenames
    for filename in filenames:

        try:

            # fits check if extension is .fits
            if filename.split('.')[-1] == 'fits':

                log.info ('gzipping {}'.format(filename))

                # if output gzipped file already exists, delete it
                filename_gzip = '{}.gz'.format(filename)
                if os.path.exists(filename_gzip):
                    os.remove(filename_gzip)
                    log.warning ('gzipping over already existing file {}'
                                 .format(filename_gzip))


                cmd = ['gzip', '-q', filename]
                subprocess.run(cmd)
                filenames_out.append(filename_gzip)


        except Exception as e:
            #log.exception (traceback.format_exc())
            log.exception ('exception was raised in gzipping of image {}: {}'
                           .format(filename,e))


    # if input list was a single file, return that filename as a string instead
    # of a list
    if len(filenames_out) == 1:
        return filenames_out[0]
    else:
        return filenames_out


    return


################################################################################

def log_timing_memory(t0, label=''):

    log.info ('wall-time spent in {}: {:.3f} s'.format(label, time.time()-t0))
    mem_use (label=label)

    return


################################################################################

def mem_use (label=''):

    # see https://gmpy.dev/blog/2016/real-process-memory-and-environ-in-python

    # ru_maxrss is in units of kilobytes on Linux; however, this seems
    # to be OS dependent as on mac os it is in units of bytes; see
    # manpages of "getrusage"
    if sys.platform=='darwin':
        norm = 1024**3
        show_swap = False
    else:
        norm = 1024**2
        show_swap = True

    maxrss = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/norm

    full_info = psutil.Process().memory_full_info()
    uss = full_info.uss / 1024**3
    rss = full_info.rss / 1024**3
    vms = full_info.vms / 1024**3

    if show_swap:
        swap = full_info.swap / 1024**3
        log.info ('memory use [GB]: uss={:.3f}, rss={:.3f}, maxrss={:.3f}, '
                  'vms={:.3f}, swap={:.3f} {}'
                  .format(uss, rss, maxrss, vms, swap, label))
    else:
        log.info ('memory use [GB]: uss={:.3f}, rss={:.3f}, maxrss={:.3f}, '
                  'vms={:.3f} {}'
                  .format(uss, rss, maxrss, vms, label))

    return


################################################################################

def run_cmd (cmd, verbose=True, shell=False):

    result = subprocess.run(cmd, capture_output=True, shell=shell)
    status = result.returncode
    if verbose:
        log.info('stdout: {}'.format(result.stdout.decode('UTF-8')))
        log.info('stderr: {}'.format(result.stderr.decode('UTF-8')))
        log.info('status: {}'.format(status))

    return status


################################################################################

def close_log (log, logfile):

    handlers = log.handlers[:]
    for handler in handlers:
        if logfile in str(handler):
            log.info('removing handler {} from log'.format(handler))
            log.removeHandler(handler)

    # remove the last handler, which is assumed to be the filehandler
    # added inside blackbox_reduce
    #log.removeHandler(log.handlers[-1])

    return


################################################################################

def attach_log (log, logfile):

    fileHandler = logging.FileHandler(logfile, 'a')
    fileHandler.setFormatter(logFormatter)
    fileHandler.setLevel('INFO')
    log.addHandler(fileHandler)

    return


################################################################################

def haversine (ra1, dec1, ra2, dec2):

    """Function that calculates (absolute) angle in degrees between RA,
    DEC coordinates ra1, dec1 and ra2, dec2. Input coordinates can be
    scalars or arrays; all are assumed to be in decimal degrees."""

    # convert to radians
    ra1, ra2, dec1, dec2 = map(np.radians, [ra1, ra2, dec1, dec2])

    d_ra = np.abs(ra1-ra2)
    d_dec = np.abs(dec1-dec2)

    a = np.sin(d_dec/2)**2 + np.cos(dec1) * np.cos(dec2) * np.sin(d_ra/2)**2
    c = 2*np.arcsin(np.sqrt(a))
    return list(map(np.degrees, [c]))[0]


################################################################################

def split_list (l, n):

    """split input list l into n roughly equal-length lists, which are
       returned in a list"""

    # indices is a list with the starting (i) and ending (i+1) indices
    # for the different output lists
    indices = list(np.linspace(0, len(l), num=n+1).astype(int))
    # return output list
    return [l[indices[i]:indices[i+1]] for i in range(len(indices)-1)]

################################################################################

def cleanup_cloudheader (header):

    # keys to delete; start with main astrometry keywords
    keys_del = ['CTYPE1', 'CTYPE2', 'CRVAL1', 'CRVAL2', 'CRPIX1', 'CRPIX2',
                'CUNIT1', 'CUNIT2', 'CD1_1', 'CD1_2', 'CD2_1', 'CD2_2']
    # PV keywords
    keys_del += ['PV{}_{}'.format(i,j) for i in range(1,3) for j in range(1,41)]
    # various
    keys_del += ['HISTORY', 'COMMENT']

    # loop keys and pop them
    for key in keys_del:
        header.pop(key, None)


################################################################################
