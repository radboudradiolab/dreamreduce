#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from astropy.stats import sigma_clip

import logging
log = logging.getLogger()

def _par_sigma_function(idx, res, errsq, err):

    # function to determine the weighted average magnitude [par] for
    # the same indices in [idx]; the resulting arrays will have length
    # = maximum index in [idx] + 1 (=length of array returned by np.bincount)
    #weights = 1/(errsq + (err**2)[idx])
    weights = 1/(errsq + err[idx]**2)
    par = np.bincount(idx, weights*res)/np.bincount(idx, weights)
    # with weights = 1/sigma**2, "weights**2*(res - par[idx])**2 -
    # weights" can be rewritten as weights * (chisq - 1), so diff will
    # be negative if chisq < 1 and positive if chisq > 1
    if False:
        diff = np.bincount(idx, weights**2*(res - par[idx])**2 - weights)
        # try chisq - 1: this indeed results in chisq converging to unity
        # assuming the above weights are used, but then err (=err3) often
        # becomes significant: a few tenths of mag. One possible
        # explanation is that indeed the error is underestimated.
        # But at the same time, many healpixels will have err=0 (sigma=0),
        # because even when adding err=0, i.e. reaching the maximum
        # chi-square, the chi-square will still be < 1 in some cases (
        # errsq is relatively large).

    else:
        # try calculating: diff = reduced chisquare - 1
        nobs =  np.bincount(idx)
        chi2 = np.bincount(idx, weights*(res - par[idx])**2)
        chi2red = chi2 / np.maximum(nobs-1, 1)
        diff = chi2red - 1

    return par, diff


def find_par_sigma(idx, res, errsq, maxiter=10):

    # Search for a solution between 0 and 2.
    N = np.amax(idx) + 1
    err1 = np.zeros(N)
    err2 = np.full(N, 2)

    # Compute the value of the function at the beginning the interval.
    par, diff1 = _par_sigma_function(idx, res, errsq, err1)
    # find indices where chi2 is still below 1, even while a zero sigma was added
    # to the indivual errors; these sigmas will be set to zero further
    # below
    args1, = np.where(diff1 < 1e-10)

    # Compute the value of the function at the end the interval.
    par, diff2 = _par_sigma_function(idx, res, errsq, err2)
    # find indices where chi2 is still above 1 even while a sigma of 2
    # was added to the indivual errors; these sigmas will be set to 2
    # further below
    args2, = np.where(diff2 > 1e-10)

    # Find the solution.
    for niter in range(maxiter):

        err3 = (err1 + err2)/2

        par, diff3 = _par_sigma_function(idx, res, errsq, err3)

        # if diff3 is positive, so chi2 > 1: move lower error up to
        # err3 to decrease chi2 in next iteration, otherwise unchanged
        err1 = np.where(diff3 > 1e-10, err3, err1)
        # if diff3 is positive, so chi2 > 1: leave upper error
        # unchanged, otherwise move upper error down to err3,
        err2 = np.where(diff3 > 1e-10, err2, err3)



    err3 = (err2 + err1)/2
    err3[args1] = 0
    err3[args2] = 2

    par, _ = _par_sigma_function(idx, res, errsq, err3)

    # N.B.: the cdecor_vmag is calling this function and appears to be
    # expecting only the additional error (err3), so that should not
    # be changed here

    return par, err3


def find_par_sigma_alt (idx, res, errsq, method):

    """alternative function to find_par_sigma that returns the
    weighted mean and the corresponding error, potentially with
    clipping; this assumes that there is only a single LST epoch in
    the arrays res and errsq and so idx=skyidx

    """

    # see https://en.wikipedia.org/wiki/Weighted_arithmetic_mean

    # first determine additional error using
    # original find_par_sigma
    __, err3 = (find_par_sigma (idx, res, errsq))
    #err3[:] = 0


    # weights
    w = 1/(errsq+err3[idx]**2)


    # weighted mean values for each index in idx; clip in case
    # required
    if 'clip' in method:
        err = np.sqrt(errsq)
        for i in np.unique(idx):

            # indices of entries with same idx
            idx_hp = np.nonzero((idx==i))[0]

            if len(idx_hp) == 0:
                continue

            # discard values that are separated by more than 3
            # sigma from the median, where sigma is scatter in the
            # distribution of points
            mask_reject = sigma_clip(res[idx_hp], sigma=3).mask
            #mask_reject = sigma_clip(res[idx_hp], sigma=3,
            #                         stdfunc='mad_std').mask

            # set corresponding weights to zero
            w[idx_hp[mask_reject]] = 0



    # sum of weights and weights**2
    V1 = np.bincount(idx, w)
    V2 = np.bincount(idx, w**2)


    # calculate weighted mean extinction
    par = np.bincount(idx, w * res) / V1

    # the standard variance of the weighted mean
    var_mean_w = 1 / V1


    if False:
        # the biased weighted sample variance
        var_w_biased = np.bincount(idx, w*(res - par[idx])**2) / V1
        # the unbiased weighted sample variance
        var_w_unbiased = np.bincount(idx, w*(res - par[idx])**2) / (V1 - V2/V1)


    # calculate simple median and std
    if 'median' in method:
        N = len(par)
        med = np.zeros(N, dtype='float32')
        std = np.zeros(N, dtype='float32')
        emed = np.zeros(N, dtype='float32')
        for i in range(N):
            # mask of entries with same idx
            idx_hp = np.nonzero((idx==i))[0]
            if len(idx_hp) > 0:

                if False:
                    # sigma clipping
                    res_ma = sigma_clip(res[idx_hp], sigma=3)
                    # median and std
                    med[i] = np.ma.median(res_ma)
                    std[i] = np.ma.std(res_ma, ddof=1)
                    # error in the median (=error in the mean * sqrt(np.pi/2))
                    emed[i] = std[i] * np.sqrt( (np.pi/2) / np.sum(~res_ma.mask))

                else:
                    # without sigma clipping
                    med[i] = np.median(res[idx_hp])
                    std[i] = np.std(res[idx_hp], ddof=1)
                    emed[i] = std[i] * np.sqrt( (np.pi/2) / len(idx_hp))


                log.info('i: {}, med[i]: {}, std[i]: {}, emed[i]: {}'
                         .format(i, med[i], std[i], emed[i]))


        return med, emed


    # return the weighted mean and corresponding error
    return par, np.sqrt(var_mean_w)


def _sigma_function(idx, ressq, errsq, err):

    #weights = 1/(errsq + (err**2)[idx])
    weights = 1/(errsq + err[idx]**2)
    # with weights = 1/sigma**2, weights**2*ressq - weights below can
    # be rewritten as weights * (chisq - 1), so term will be negative
    # if chisq < 1 and positive if chisq > 1
    term = np.bincount(idx, ressq*weights**2 - weights)

    return term


def find_sigma(idx, residuals, errsq, maxiter=10):

    # Search for a solution between 0 and 2.
    N = np.amax(idx) + 1
    err1 = np.zeros(N)
    err2 = np.full(N, 2.)

    ressq = residuals*residuals

    # Compute the value of the function at the beginning the interval.
    diff1 = _sigma_function(idx, ressq, errsq, err1)
    args1, = np.where(diff1 < 1e-10)

    # Compute the value of the function at the end the interval.
    diff2 = _sigma_function(idx, ressq, errsq, err2)
    args2, = np.where(diff2 > 1e-10)

    # Find the solution.
    for niter in range(maxiter):

        err3 = (err2 + err1)/2.
        diff3 = _sigma_function(idx, ressq, errsq, err3)

        err1 = np.where(diff3 > 1e-10, err3, err1)
        err2 = np.where(diff3 > 1e-10, err2, err3)

    err3 = (err2 + err1)/2.
    err3[args1] = 0.
    err3[args2] = 2.

    # why not return: np.sqrt(errsq + err3**2) ??

    return err3
