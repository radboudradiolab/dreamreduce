# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 17:18:01 2016

@author: talens
"""

import os
import numpy as np

control_ip = '192.168.1.12'
control_port = '7002'

# Name of the observing site.
#sitename = 'LaSilla'

# previously: siteinfo was recorded in a file
#siteinfo = os.path.join(confdir, 'siteinfo.dat')
# now: define here in config
if True:
    siteinfo = {'sitename': 'CerroPachon',
                'lat':      -30.244769,
                'lon':      -70.747433,
                'height':   2650,
                'ID':       'CP'}
else:
    siteinfo = {'sitename': 'LaSilla',
                'lat':      -29.257124,
                'lon':      -70.737795,
                'height':   2369.4,
                'ID':       'LS'}


# I/O and tmp folders
rawdir  = '/tmpdata/data'
confdir = '/tmpdata/conf'
tmpdir  = '/tmpdata/tmp'
reddir  = '/tmpdata/products'
arcdir  = '/tmpdata/rawarchive'
# base folder where products and raw images are saved in
# rawdir_monitor mode
datadir = '/data'


# define age limit in days for different folders for cleaning up disks
folder_age_limit = {reddir: {'clouds':      16,
                             'diff':         1,
                             'lightcurves': 16,
                             'masters':     16,
                             'sys':         16,
                             'targets':     16,
                             'thumbs':      16,
                             'zeropoints':  16,
                             },
                    '/data/rawarchive': 21,
                    '/data/products': {'clouds':      365,
                                       'diff':          8,
                                       'lightcurves': 365,
                                       'masters':     365,
                                       'sys':         365,
                                       'targets':     365,
                                       'thumbs':      365,
                                       'zeropoints':  365,
                                       },
                    # images in rawdir are deleted when archiving them
                    # to rawarchive
                    # rawdir: 1,
                    }


# keep temporary folder in case of rereduction
keep_tmp = False


###############################################################################
# Locations of various files.
###############################################################################

# Location of the stellar catalogue.
starcat = os.path.join(confdir, 'bringcat20180428_GaiaDR3.fits')

# Locations of the darktables, astromasters, systables and zpstables
darktable = {}
astromaster = {}
systable = {}
zpstable = {}
camid_list = ['{}{}'.format(siteinfo['ID'], l) for l in ['N', 'E', 'S', 'W', 'C']]
for camid in camid_list:
    darktable[camid] = os.path.join(confdir, 'darktable{}.dat'.format(camid))
    astromaster[camid] = os.path.join(confdir, 'astromaster{}.npy'.format(camid))
    systable[camid] = os.path.join(confdir, 'systable{}.dat'.format(camid))
    zpstable[camid] = os.path.join(confdir, 'zpstable{}.dat'.format(camid))


# source-extractor config file
sex_config = '{}/sextractor.config'.format(confdir)
sex_conv = '{}/default.conv'.format(confdir)
sex_params = '{}/sextractor.params'.format(confdir)
sex_nnw = '{}/default.nnw'.format(confdir)

# swarp config file
swarp_config = '{}/swarp.config'.format(confdir)

# swarp config file
psfex_config = '{}/psfex.config'.format(confdir)


###############################################################################
# Parameters of the reduction.
###############################################################################

# sun altitude at which to start monitor for new files
twilight = 5

# number of CPUs to use
ncpus = 6


# preprocessing
###############

# nominal and minimum number of bias frames to create a new masterbias
nbias = 20
nbias_min = 10
# nominal and minimum number of dark frames to create a new masterdark
ndark = 20
ndark_min = 10
# nominal and minimum number of flat frames to create a new masterflat
nflat = 20
nflat_min = 10


# astrometry
############

# apply Gaia DR3 proper motion when inferring x,y positions to perform
# the photometry?
apply_gaia_pm = True

# Astrometry.net configuration file; /etc/astrometry.cfg is the
# default one referring to index files in /usr/share/astrometry, both
# config and index files are created inside the singularity container
astronet_cfg = '/etc/astrometry.cfg'

# pixel index section of active ccd region including buffers
#ccd_active = [slice(17,2705), slice(24,4057)]
# excluding buffers
ccd_active = [slice(25,2697), slice(36,4044)]

# reduced image size in pixels
ysize, xsize = np.empty((5000,5000))[tuple(ccd_active)].shape

# overscan regions at top, bottom, left and right
ccd_os_top = [slice(2705,2721), slice(4,4077)]
ccd_os_bottom = [slice(1,17), slice(4,4077)]
ccd_os_left = [slice(1,None), slice(4,25)]
ccd_os_right = [slice(1,None), slice(4057,4077)]


# WCS solution constraints in arcmin
wcs_offset = 0.1
wcs_std = 0.5

# number of images for which Astrometry.net is run once
nimages_astro = 5

# maximum time for follower images to wait for leader-image WCS
# solution; if exceeded the follower images will fetch the WCS from
# the astromaster
tmax_leader_wcs = 30

# timescale in number of images with which astromaster solution is saved
nimages_astromaster = 15

# limits for solving the astrometry.
maglim_astro = [4, 7.5]
# lower limit to dist8 column in ASCC catalog
dist8_lim = 10


# approximate J2000 declinations and local hour angles of camera
# centers used as initial guess for the astrometry
if siteinfo['ID'] == 'LS':
    # CHECK!!! - LSW still needs to be checked; rest is ok
    cam_dec_cntr = {'LSC': -32.5, 'LSN': 12.6, 'LSS': -70.1,
                    'LSE': -20.4, 'LSW': -30}
    cam_lha = {'LSC': 0, 'LSN': 0, 'LSS': 0, 'LSE': -3, 'LSW': 3}

elif siteinfo['ID'] == 'CP':
    # dec in degrees
    cam_dec_cntr = {'CPC': -30.5, 'CPN': 11.0, 'CPS': -71.0, 'CPE': -23.2,
                    'CPW': -22.8}
    # lha in hours
    cam_lha = {'CPC': -0.3, 'CPN': 0.0, 'CPS': -0.1, 'CPE': -3.0, 'CPW': 3.1}


# search in Astrometry.net index files within this radius of the
# approximated RA and DEC
astronet_radius = 30

# approximate pixel scale and its fractional range within which
# to search for an astrometric solution
pixscale = 75
pixscale_varyfrac = 0.03

# margin of pixels from the image edge in which to discard sources for
# the astrometric solution
edge_margin = 50


# photometry
############

# subtract sky background using source extractor before performing
# photometry
subtract_skybkg = True

# use zeropoints_[camid].fits table to infer the transmission in the
# live calibration; currently this parameter is not used as both
# calibrations are applied simultaneously
use_zeropoints = False

maglim_fast = 8.4        # Magnitude limit for the fast photometry.
maglim_slow = 10.        # Magnitude limit for the slow photometry.

aper_fast = [2.5, 4.5]  # Apertures for the fast photometry.
aper_slow = [2.5, 3.5, 4.5, 5.5]  # Apertures for the slow photometry.
skyrad = [6, 21]  # Inner and outer radius of the sky annulus.


# calibration
#############

# number of days/nights before and after the date being processed for
# which to include the lightcurves in the sysfile and zeropoint
# calibration
ndays_cal = [-15, 0]


# difference image
##################

# maximum number of images/time steps in the past for an image still
# to be used for image subtraction
imdiff_dseq_max = 1


# cloud maps
############

# number of images to combine into cloud map; at the moment this does
# not do anything
#nimages_cloudmap = 5

# resolution of the cloud map in healpix level parameter, where npix =
# 12*nside**2 and nside = 2**level (or level = int(np.log2(nside))),
# so npix = 12 * 4**level
hp_level = 4
# nested (T) or ring (F)
hp_nest = True


# mailing
#########
mailing = {'host': 'smtp.strw.leidenuniv.nl',
           'port': 587,
           'pwd': 'NoClouds',
           'recipients': ['p.vreeswijk@astro.ru.nl'],
           'sender': 'mascara',
           'user': 'weather'}


# targets
#########
targets = {'bpic':
           ['2110269', '2110219', '2110368', '2110027', '2110417', '2110564',
            '2110053', '2110474', '2110539', '2012682', '2109901', '2110616',
            '2110144', '2109936', '2109868', '2110399', '2600793', '2109845',
            '2110717', '2110718', '2013224', '2012725', '2110555'],
           'proxcen':
           ['2347852', '2347915', '2347585', '2348624', '2346970', '2348379',
            '2348643', '2346992', '2347267', '2348753', '2346595', '2346587',
            '2346586', '2348874', '2347230', '2348180', '2347865', '2348510',
            '2346543', '2348584', '2346360']}


# miscellaneous
###############

nhalf = 25  # Size of postage stamps.
