#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import healpy as hp
from dreamreduce import configuration as cfg


class PolarGrid(object):
    """ Create a polar coordinate grid.

    Attributes:
        nx (int): The resolution of the polargrid along the ra-axis.
        ny (int): The resolution of the polargrid along the dec-axis.
        xedges (float): The binedges of the grid along the ra-axis.
        yedges (float): The binedges of the grid along the dec-axis.
        npix (int): The total number of pixels on the grid.

    """

    def __init__(self, nx, ny):
        """ Initialize the coordinate grid.

        Args:
            nx (int): The resolution of the polargrid along the ra-axis.
            ny (int): The resolution of the polargrid along the dec-axis.

        """

        self.nx = nx
        self.ny = ny

        self.xedges = np.linspace(0, 360, self.nx+1)
        self.yedges = np.linspace(-90, 90, self.ny+1)

        self.npix = (nx + 2)*(ny + 2)

        return

    def _ra2idx(self, ra):

        idx = np.searchsorted(self.xedges, ra, 'right')

        return idx

    def _dec2idx(self, dec):

        idx = np.searchsorted(self.yedges, dec, 'right')

        return idx

    def radec2idx(self, ra, dec):
        """ Convert ra, dec coordinates to indices on the grid.

        Args:
            ra (float): An array of ra coordinates.
            dec (float): An array of dec coordinates.

        Returns:
            idx1 (int): First index of the cell, corresponds to ra.
            idx2 (int): Second index of the cell, corresponds to dec.

        """

        idx1 = self._ra2idx(ra)
        idx2 = self._dec2idx(dec)

        return idx1, idx2

    def _idx2ra(self, idx):

        ra = np.full(self.nx+2, fill_value=np.nan)
        ra[1:-1] = (self.xedges[:-1] + self.xedges[1:])/2.

        return ra[idx]

    def _idx2dec(self, idx):

        dec = np.full(self.ny+2, fill_value=np.nan)
        dec[1:-1] = (self.yedges[:-1] + self.yedges[1:])/2.

        return dec[idx]

    def idx2radec(self, idx1, idx2):
        """ Convert indices on the grid to gridcell coordinates.

        Args:
            idx1 (int): An array of cell indices along the ra-axis.
            idx2 (int): An array of cell indices along the dec-axis.

        Returns:
            ra (float): The ra coordinates of the gridcells.
            dec (float): The dec coordinates of the gridcells.

        """

        ra = self._idx2ra(idx1)
        dec = self._idx2dec(idx2)

        return ra, dec

    def values2grid(self, idx1, idx2, values, fill_value=0):
        """ Given grid indices and values return an array.

        Args:
            idx1 (int): An array of indices along the ra-axis.
            idx2 (int): An array of indices along the dec-axis.
            values (float): An array of values corresponding to idx1, idx2.
            fill_value (float): Value to fill unspecified cells with.

        Returns:
            array (float): An array corresponding to the grid.

        """

        array = np.full((self.nx+2, self.ny+2), fill_value=fill_value)
        array[idx1, idx2] = values

        return array


class HealpixGrid(object):
    """ Create a Healpix coordinate grid.

    Attributes:
        nside (int): Determines the resolution of the grid for further
            information refer to the healpy documentation.
        npix (int): The total number of pixels on the grid.

    """

    def __init__(self, nside):
        """ Initialize the coordinate grid.

        Args:
            nside (int): Determines the resolution of the grid for further
            information refer to the healpy documentation.

        """

        self.hp = hp

        self.nside = nside
        self.npix = self.hp.nside2npix(self.nside)

        return

    def radec2idx(self, ra, dec):
        """ Convert ra, dec coordinates to indices on the grid.

        Args:
            ra (float): An array of ra coordinates.
            dec (float): An array of dec coordinates.

        Returns:
            idx (int): The index of the gridcell to which each ra, dec pair
                belongs.

        """

        #idx = self.hp.ang2pix(
        #    self.nside, (90. - dec)*np.pi/180., ra*np.pi/180.)
        idx = self.hp.ang2pix(self.nside, ra, dec, nest=cfg.hp_nest, lonlat=True)

        return idx


    def idx2radec(self, idx):
        """ Converts gridcell indices the the ra, dec of the cell.

        Args:
            idx (int): Gridcell indices.

        Returns:
            ra (float): The ra coordinates of each of the cells.
            dec (float): The dec coordinates of each of the cells.

        """

        #theta, phi = self.hp.pix2ang(self.nside, idx)
        #dec = 90. - theta*180./np.pi
        #ra = phi*180./np.pi
        ra, dec = self.hp.pix2ang(self.nside, idx, nest=cfg.hp_nest, lonlat=True)

        return ra, dec

    def values2grid(self, idx, values, fill_value=0):
        """ Given grid indices and values return an array.

        Args:
            idx (int): An array of gridcell indices.
            values (float): An array of values corresponding to idx.
            fill_value (float): Value to fill unspecified cells with.

        Returns:
            array (float): An array corresponding to the grid.

        """

        array = np.full(self.npix, fill_value=fill_value)
        array[idx] = values

        return array
