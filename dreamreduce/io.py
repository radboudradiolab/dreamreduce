# -*- coding: utf-8 -*-
"""
Created on Wed Nov 16 17:21:45 2016

@author: talens
"""

import os
import logging
import shutil

import time
import numpy as np
from threading import Thread
import queue
import atexit

import h5py
from astropy.io import fits
from astropy.table import Table
from astropy import units as u

import yaml

import healpy as hp

from dreamreduce import rawdir_monitor as rm
from dreamreduce import configuration as cfg
from dreamreduce import grids
from dreamreduce import misc

log = logging.getLogger()


def ensure_dir(path):

    if not os.path.exists(path):
        os.makedirs(path)

    return


def read_siteinfo(filename, sitename):

    # not used anymore; siteinfo is now a dictionary defined in configuration.py

    # Read the siteinfo file.
    #dtype = [('sitename', '|S20'), ('lat', 'float32'), ('lon', 'float32'), ('height', 'float32'), ('ID', '|S2')]
    #siteinfo = np.genfromtxt(filename, dtype=dtype)

    # Select the desired site.
    #mask = siteinfo['sitename'] == bytes(sitename, 'utf-8')
    #siteinfo = siteinfo[mask]
    with open(filename) as f:
        for line in f:
            try:
                name, lat, lon, alt, id = line.split()
            except ValueError:
                continue
            if sitename == name:
                return {'sitename': name,
                        'lat': float(lat),
                        'lon': float(lon),
                        'height': float(alt),
                        'ID': id}
        raise AssertionError(
            "Site {} not found in siteinfo file '{}'".format(sitename, siteinfo))
    # return siteinfo


def read_targets(filename):
    with open(filename, 'r') as ymlfile:
        targets = yaml.unsafe_load(ymlfile)

    return targets


def read_mailing(filename):
    with open(filename, 'r') as ymlfile:
        mailing = yaml.unsafe_load(ymlfile)

    return mailing


def write_masterframe(filename, image, header, overwrite=True):
    image = image.astype('float32')

    hdu = fits.PrimaryHDU(data=image, header=header)
    hdu.writeto(filename, overwrite=overwrite)

    return


def read_masterdark(darktable):

    # extract first line from darktable using io.get_file()
    filename = get_file(darktable)

    # read data and header
    if filename is not None:
        data, header = fits.getdata(filename, header=True)
    else:
        data = header = None

    return data, header, filename


def archive_files(filelist, dest_dir, delete=True):

    # CHECK!!! - archiving files that can be deleted is now done using
    # shutil.move() - update to shell rsync and deleting afterwards??


    # if filelist is a string, put it in a list
    if isinstance(filelist, str):
        filelist = [filelist]

    # loop filelist
    for f in filelist:

        # check if the destination file already exists
        f_dest = os.path.join(dest_dir, f.split('/')[-1])
        # if so, skip it
        if os.path.exists(f_dest):
            log.warning ('{} already exists; not archiving {}'.format(f_dest, f))
            continue

        # check if the source file exists
        if os.path.exists(f):

            # move or copy the files to destination directory
            if delete:
                shutil.move (f, dest_dir)
            else:
                shutil.copy2 (f, dest_dir)


    return


def read_astromaster(filename):
    """
    Read the astromater file.
    This converts the hdf5 frame to vanilla numpy arrays on the way.
    """
    wcspars = dict()
    polpars = dict()

    with h5py.File(filename, 'r') as f:
        grp = f['wcspars']
        wcspars['lst'] = grp.attrs['lst']
        # [()] notation is the somewhat obscure way to convert a Dataframe to a numpy array
        wcspars['crval'] = grp['crval'][()]
        wcspars['crpix'] = grp['crpix'][()]
        wcspars['cdelt'] = grp['cdelt'][()]
        wcspars['pc'] = grp['pc'][()]

        grp = f['polpars']
        polpars['nx'] = grp.attrs['nx']
        polpars['ny'] = grp.attrs['ny']
        polpars['order'] = grp.attrs['order']
        polpars['x_wcs2pix'] = grp['x_wcs2pix'][()]
        polpars['y_wcs2pix'] = grp['y_wcs2pix'][()]
        polpars['x_pix2wcs'] = grp['x_pix2wcs'][()]
        polpars['y_pix2wcs'] = grp['y_pix2wcs'][()]

        if 'astromask' in list(f.keys()):
            astromask = f['astromask'][()]
        else:
            astromask = None

    return wcspars, polpars, astromask


def write_astromaster(filename, wcspars, polpars, astromask=None, overwrite=True):

    if overwrite:
        mode = 'w'
    else:
        mode = 'a'

    with h5py.File(filename, mode=mode) as f:

        grp = f.create_group('wcspars')
        grp.attrs['lst'] = wcspars['lst']
        grp.create_dataset('crval', data=wcspars['crval'])
        grp.create_dataset('crpix', data=wcspars['crpix'])
        grp.create_dataset('cdelt', data=wcspars['cdelt'])
        grp.create_dataset('pc', data=wcspars['pc'])

        grp = f.create_group('polpars')
        grp.attrs['nx'] = polpars['nx']
        grp.attrs['ny'] = polpars['ny']
        grp.attrs['order'] = polpars['order']
        grp.create_dataset('x_wcs2pix', data=polpars['x_wcs2pix'])
        grp.create_dataset('y_wcs2pix', data=polpars['y_wcs2pix'])
        grp.create_dataset('x_pix2wcs', data=polpars['x_pix2wcs'])
        grp.create_dataset('y_pix2wcs', data=polpars['y_pix2wcs'])

        if astromask is not None:
            f.create_dataset('astromask', data=astromask)

    return


def read_catalogue(starcat=cfg.starcat):

    catalogue = dict()

    data = fits.getdata(starcat)

    catalogue['ascc'] = data['ASCC']
    catalogue['ra'] = data['_RAJ2000']
    catalogue['dec'] = data['_DEJ2000']

    # replace ASCC coordinates by Gaia DR3; Gaia stars with
    # matches within 1" of the ASCC J2000/epoch 2000 position
    # will have non-zero entries
    mask_gaia = (data['source_id'] != 0)
    catalogue['ra'][mask_gaia] = data['ra'][mask_gaia]
    catalogue['dec'][mask_gaia] = data['dec'][mask_gaia]
    log.info ('adopting Gaia DR3 coordinates for {} entries in {}'
              .format(np.sum(mask_gaia), starcat))

    # add proper motion columns
    catalogue['pmra'] = data['pmra']
    catalogue['pmdec'] = data['pmdec']

    catalogue['vmag'] = data['Vmag']
    catalogue['bmag'] = data['Bmag']
    catalogue['sptype'] = data['SpType']
    catalogue['blend'] = data['Blend']
    catalogue['var'] = data['Var']
    catalogue['dist6'] = data['Dist6']
    catalogue['dist8'] = data['Dist8']
    catalogue['dist10'] = data['Dist10']
    catalogue['inclusion'] = data['Inclusion']

    catalogue = np.rec.fromarrays([catalogue[key] for key in list(
        catalogue.keys())], names=[key for key in list(catalogue.keys())])

    sort = np.argsort(catalogue['ascc'])
    catalogue = catalogue[sort]

    return catalogue


def read_skyidx(ascc0, starcat=cfg.starcat):

    data = fits.getdata(starcat)

    ascc = data['ASCC']

    # skyidx or the healpix index was set to depend on the column
    # SKYIDX in starcat, which is set for level 3:
    #skyidx = data['SKYIDX']

    # make this dependent of the level defined in the configuration
    # module
    nside = 2**cfg.hp_level
    skyidx = hp.pixelfunc.ang2pix(nside, data['_RAJ2000'], data['_DEJ2000'],
                                  nest=cfg.hp_nest, lonlat=True)

    sort = np.argsort(ascc)
    ascc = ascc[sort]
    skyidx = skyidx[sort]

    args = np.searchsorted(ascc, ascc0)

    return skyidx[args]


def read_stack(filelist):

    nimages = len(filelist)

    stack = []
    headers = []

    for i in range(nimages):

        filename = filelist[i]

        try:
            image, header = fits.getdata(filename, header=True)
        except:
            log.warn('Failed to read image {}'.format(filename))
            continue
        else:
            stack.append(image)
            headers.append(header)

    stack = np.array(stack)

    return stack, headers


def write_binimage(filename, image, header, overwrite=True):

    image = image.astype('float32')

    # Adds info to header, CompImageHDU does not do this on its own and crashes.
    hdu = fits.ImageHDU(image, header)
    hdu = fits.CompImageHDU(image, hdu.header, compression_type='RICE_1', tile_size=[
                            167, 167], quantize_level=32)
    hdu.writeto(filename, overwrite=overwrite)

    return


class TmpFile(object):

    def __init__(self, filename, overwrite=True):
        # remove old file if present
        if overwrite and os.path.exists(filename):
            os.remove(filename)

        self.filename = filename
        return

    def add_header(self, header, siteinfo, aper, skyrad):
        with h5py.File(self.filename, 'a') as f:
            grp = f.create_group('header')
            grp.attrs['site-obs'] = header['SITE-OBS']
            grp.attrs['cam-obs'] = header['CAMERA']
            grp.attrs['lat'] = siteinfo['lat']
            grp.attrs['lon'] = siteinfo['lon']
            grp.attrs['alt'] = siteinfo['height']
            grp.attrs['aversion'] = header['APIVER']
            grp.attrs['cversion'] = header['AVERSION']
            grp.attrs['rversion'] = '2017.00'  # TODO
            grp.attrs['exptime'] = [6.4, 3.2]  # TODO
            grp.attrs['ccdtemp'] = -15  # TODO
            grp.attrs['aper'] = aper
            grp.attrs['skyrad'] = skyrad

    def add_station(self, station):

        # Sort the array by lstseq.
        sort = np.argsort(station['lstseq'])
        station = station[sort]

        with h5py.File(self.filename, 'a') as f:

            grp = f.create_group('station')

            for field in station.dtype.names:
                grp.create_dataset(field, data=station[field])

        return

    def add_lightcurves(self, curves, cat):

        # Sort the array be ascc first and lstseq second.
        sort = np.lexsort((curves['lstseq'], curves['ascc']))
        curves = curves[sort]

        # Get the unique stars and the number of points for each star.
        ascc, nobs = np.unique(curves['ascc'], return_counts=True)

        strides = np.append(0, np.cumsum(nobs))
        names = list(curves.dtype.names)
        names.remove('ascc')

        # Create arrays.
        lstsqmin = np.zeros(len(ascc), dtype='uint32')
        lstsqmax = np.zeros(len(ascc), dtype='uint32')

        with h5py.File(self.filename, 'a') as f:

            grp = f.create_group('lightcurves')

            for i in range(len(ascc)):

                lc = curves[strides[i]:strides[i+1]][names]

                lstsqmin[i] = lc['lstseq'][0]
                lstsqmax[i] = lc['lstseq'][-1]

                grp.create_dataset(ascc[i], data=lc)

            grp = f.create_group('stars')
            grp.create_dataset('ascc', data=ascc)
            grp.create_dataset('nobs', data=nobs.astype('uint16'))
            grp.create_dataset('lstsqmin', data=lstsqmin)
            grp.create_dataset('lstsqmax', data=lstsqmax)

            idx = np.searchsorted(cat['ascc'], ascc)
            grp.create_dataset('ra', data=cat[idx]['ra'])
            grp.create_dataset('dec', data=cat[idx]['dec'])
            grp.create_dataset('vmag', data=cat[idx]['vmag'])
            grp.create_dataset('bmag', data=cat[idx]['bmag'])
            grp.create_dataset(
                'sptype', data=cat[idx]['sptype'].astype('|S20'))
            grp.create_dataset('blend', data=cat[idx]['blend'])
            grp.create_dataset('var', data=cat[idx]['var'])

        return

    def add_astrometry(self, astrosol):

        if astrosol:

            with h5py.File(self.filename, 'a') as f:

                grp = f.create_group('astrometry')

                for field in list(astrosol.keys()):
                    grp.create_dataset(field, data=astrosol[field])

        return


def write_stamps(filename, stamps, curves, station, cat, overwrite=True):

    if overwrite:
        try:
            os.remove(filename)
        except:
            pass

    # Sort the array by ascc first and lstseq second.
    sort = np.lexsort((curves['lstseq'], curves['ascc']))
    curves = curves[sort]
    stamps = stamps[sort]

    # Get the unique stars and the number of points for each star.
    ascc, nobs = np.unique(curves['ascc'], return_counts=True)

    strides = np.append(0, np.cumsum(nobs))
    names = list(curves.dtype.names)
    names.remove('ascc')

    lstsqmin = np.zeros(len(ascc), dtype='uint32')
    lstsqmax = np.zeros(len(ascc), dtype='uint32')

    with h5py.File(filename, 'a') as f:

        grp = f.create_group('stamps')
        for i in range(len(ascc)):

            grp.create_dataset(
                ascc[i]+'/lstseq', data=curves['lstseq'][strides[i]:strides[i+1]])
            grp.create_dataset(ascc[i]+'/stamps',
                               data=stamps[strides[i]:strides[i+1]])

        grp = f.create_group('lightcurves')
        for i in range(len(ascc)):

            lc = curves[strides[i]:strides[i+1]][names]

            lstsqmin[i] = lc['lstseq'][0]
            lstsqmax[i] = lc['lstseq'][-1]

            grp.create_dataset(ascc[i], data=lc)

        grp = f.create_group('stars')
        grp.create_dataset('ascc', data=ascc)
        grp.create_dataset('nobs', data=nobs.astype('uint16'))
        grp.create_dataset('lstsqmin', data=lstsqmin)
        grp.create_dataset('lstsqmax', data=lstsqmax)

        idx = np.searchsorted(cat['ascc'], ascc)
        grp.create_dataset('ra', data=cat[idx]['ra'])
        grp.create_dataset('dec', data=cat[idx]['dec'])
        grp.create_dataset('vmag', data=cat[idx]['vmag'])
        grp.create_dataset('bmag', data=cat[idx]['bmag'])
        grp.create_dataset('sptype', data=cat[idx]['sptype'].astype('|S20'))
        grp.create_dataset('blend', data=cat[idx]['blend'])
        grp.create_dataset('var', data=cat[idx]['var'])

        grp = f.create_group('station')
        for key in station.dtype.names:
            grp.create_dataset(key, data=station[key])

    return

###############################################################################
# Keep track of processed files so we can restart.
###############################################################################


def write_in_queue(filename, filelist):

    import json

    filelist = list(filelist)

    with open(filename, mode='w') as f:
        json.dump(filelist, f)

    return


def read_in_queue(filename):

    import json

    with open(filename, mode='r') as f:
        filelist = json.load(f)

    filelist = set(filelist)

    return filelist

###############################################################################
# Functions for combining the photometry files.
###############################################################################


def _index_files(filelist):

    nfiles = len(filelist)

    idx1 = np.array([], dtype='uint16')

    stars = dict()
    for i in range(nfiles):

        filename = filelist[i]

        with h5py.File(filename, 'r') as f:

            grp = f['stars']

            for key in list(grp.keys()):

                # skip bmag; not all stars have an associated bmag in
                # bringcat20180428.fits
                #if 'bmag' in key:
                #    continue

                ascc_ = grp['ascc'][()]

                if key not in list(stars.keys()):
                    stars[key] = grp[key][()]
                else:
                    stars[key] = np.append(stars[key], grp[key][()])

            grp = f['lightcurves']

            dtype = grp[ascc_[0]].dtype

        idx1 = np.append(idx1, np.repeat(i, len(ascc_)))

    ascc, args, idx2 = np.unique(
        stars['ascc'], return_index=True, return_inverse=True)
    nstars = len(ascc)
    nobs = np.zeros((nfiles, nstars), dtype='uint32')
    stars['nobs'] = stars['nobs'].astype('uint32')
    nobs[idx1, idx2] = stars['nobs']

    for key in list(stars.keys()):
        stars[key] = stars[key][args]

    return stars, nobs, dtype


def _index_files_pool (filelist):

    nfiles = len(filelist)

    idx1 = np.array([], dtype='uint16')

    stars = dict()
    for i in range(nfiles):

        filename = filelist[i]

        with h5py.File(filename, 'r') as f:

            grp = f['stars']

            for key in list(grp.keys()):

                # skip bmag; not all stars have an associated bmag in
                # bringcat20180428.fits
                if 'bmag' in key:
                    continue

                ascc_ = grp['ascc'][()]

                if key not in list(stars.keys()):
                    stars[key] = grp[key][()]
                else:
                    stars[key] = np.append(stars[key], grp[key][()])

            grp = f['lightcurves']

            dtype = grp[ascc_[0]].dtype

        idx1 = np.append(idx1, np.repeat(i, len(ascc_)))


    # this block is part of the original function [_index_files]
    if False:
        ascc, args, idx2 = np.unique(
            stars['ascc'], return_index=True, return_inverse=True)
        nstars = len(ascc)
        nobs = np.zeros((nfiles, nstars), dtype='uint32')
        stars['nobs'] = stars['nobs'].astype('uint32')
        nobs[idx1, idx2] = stars['nobs']

        for key in list(stars.keys()):
            stars[key] = stars[key][args]

            return stars, nobs, dtype


    return stars, idx1, dtype


def _read_header(filelist):

    header = dict()
    with h5py.File(filelist[0], 'r') as f:

        grp = f['header']
        for key in list(grp.attrs.keys()):
            header[key] = grp.attrs[key]

    return header


def _read_curves(filelist, ascc, nobs, dtype):

    nfiles = len(filelist)
    nstars = len(ascc)

    strides = np.row_stack([nstars*[0], np.cumsum(nobs, axis=0)]).astype('int')
    curves = {ascc[i]: np.recarray(strides[-1, i], dtype=dtype)
              for i in range(nstars)}

    for i in range(nfiles):

        filename = filelist[i]

        with h5py.File(filename, 'r') as f:

            grp = f['lightcurves']

            for j in range(nstars):

                if (nobs[i, j] > 0):

                    curves[ascc[j]][strides[i, j]:strides[i+1, j]] = grp[ascc[j]][()]

    return curves


def _read_curves_pool (index_start_stop, filelist, ascc, nobs, dtype):

    i1, i2 = index_start_stop
    ascc = ascc[i1:i2]
    nobs = nobs[:,i1:i2]

    nfiles = len(filelist)
    nstars = len(ascc)

    strides = np.row_stack([nstars*[0], np.cumsum(nobs, axis=0)]).astype('int')
    curves = {ascc[i]: np.recarray(strides[-1, i], dtype=dtype)
              for i in range(nstars)}

    for i in range(nfiles):

        filename = filelist[i]

        with h5py.File(filename, 'r') as f:

            grp = f['lightcurves']

            for j in range(nstars):

                if (nobs[i, j] > 0):

                    curves[ascc[j]][strides[i, j]:strides[i+1, j]] = grp[ascc[j]][()]

    return curves



def _read_station(filelist):

    station = dict()
    for filename in filelist:

        with h5py.File(filename, 'r') as f:

            grp = f['station']

            for key in list(grp.keys()):

                if key not in list(station.keys()):
                    station[key] = grp[key][()]
                else:
                    station[key] = np.append(station[key], grp[key][()])

    return station


def _read_astrometry(filelist):

    astrometry = dict()
    for filename in filelist:

        with h5py.File(filename, 'r') as f:

            try:
                grp = f['astrometry']
            except:
                continue
            else:

                for key in list(grp.keys()):

                    if key not in list(astrometry.keys()):
                        astrometry[key] = [grp[key][()]]
                    else:
                        astrometry[key].append(grp[key][()])

    for key in list(astrometry.keys()):
        astrometry[key] = np.array(astrometry[key])

    return astrometry


def combine_photometry(filename, filelist, overwrite=True, astrometry=True,
                       nsteps=1000):

    if overwrite and os.path.exists(filename):
        os.remove(filename)

    filelist = np.sort(filelist)


    # Read the combined stars field and index the files
    t0 = time.time()
    stars, nobs, dtype = _index_files(filelist)
    nstars = len(stars['ascc'])

    log.info ('time to read the combined stars field and index the files: '
              '{:.2f}s'.format(time.time()-t0))


    if cfg.ncpus > 1:

        t0 = time.time()

        # indices_start_stop is a list of lists with the start and
        # stop indices for the threads to process, spread evenly over
        # the different threads
        nchunks = cfg.ncpus
        # make sure chunksize is less than nsteps
        while nstars/nchunks > nsteps:
            nchunks *= 2

        indices = list(np.linspace(0, nstars, num=nchunks+1).astype(int))
        indices_start_stop = [indices[i:i+2] for i in range(len(indices)-1)]


        # multiprocess [_read_curves_pool]
        curves_list = rm.pool_func(_read_curves_pool, indices_start_stop,
                                   filelist, stars['ascc'], nobs,
                                   dtype, nproc=cfg.ncpus)


        # results is a list of dictionaries; merge them into curves
        curves = {}
        for d in curves_list:
            curves.update(d)

        log.info ('time to read_curves in [combine_photometry]: {:.2f}s'
                  .format(time.time()-t0))
        t0 = time.time()


        # Write the combined lightcurves for a group of stars.
        with h5py.File(filename, 'a') as f:

            for j in range(nstars):

                tmp = curves[stars['ascc'][j]]

                stars['nobs'][j] = len(tmp)
                stars['lstsqmin'][j] = tmp['lstseq'][0]
                stars['lstsqmax'][j] = tmp['lstseq'][-1]

                #import ipdb
                # ipdb.set_trace()
                f.create_dataset(
                    'lightcurves/{}'.format(str(stars['ascc'][j], 'utf-8')),
                    data=tmp)

        log.info ('time to add stars to {} in [combine_photometry]: {:.2f}s'
                  .format(filename, time.time()-t0))


    else:

        # original code
        for i in range(0, nstars, nsteps):

            t0 = time.time()

            # Read the combined lightcurves for a group of stars.
            curves = _read_curves(
                filelist, stars['ascc'][i:i+nsteps], nobs[:, i:i+nsteps], dtype)


            log.info ('len(curves): {}'.format(len(curves)))
            log.info ('time to read_curves in [combine_photometry]: {:.2f}s'
                      .format(time.time()-t0))
            t0 = time.time()


            # Write the combined lightcurves for a group of stars.
            with h5py.File(filename, 'a') as f:

                for j in range(i, i+len(stars['ascc'][i:i+nsteps])):

                    tmp = curves[stars['ascc'][j]]

                    stars['nobs'][j] = len(tmp)
                    stars['lstsqmin'][j] = tmp['lstseq'][0]
                    stars['lstsqmax'][j] = tmp['lstseq'][-1]

                    #import ipdb
                    # ipdb.set_trace()
                    f.create_dataset(
                        'lightcurves/{}'.format(str(stars['ascc'][j], 'utf-8')),
                        data=tmp)


            log.info ('time to add stars to {} in [combine_photometry]: {:.2f}s'
                      .format(filename, time.time()-t0))



    # Write the combined "stars" field.
    with h5py.File(filename, 'a') as f:

        grp = f.create_group('stars')
        for key in list(stars.keys()):
            grp.create_dataset(key, data=stars[key])


    # Read the "header" field.
    header = _read_header(filelist)

    # Write the "header" field..
    with h5py.File(filename, 'a') as f:

        grp = f.create_group('header')
        for key in list(header.keys()):
            grp.attrs[key] = header[key]


    # Read the combined "station" field.
    station = _read_station(filelist)

    # Write the combined "station" field.
    with h5py.File(filename, 'a') as f:

        grp = f.create_group('station')
        for key in list(station.keys()):
            grp.create_dataset(key, data=station[key])


    # Read the combined "astrometry" field.
    if astrometry:
        astrometry = _read_astrometry(filelist)

        # Write the combined "astrometry" field.
        with h5py.File(filename, 'a') as f:

            grp = f.create_group('astrometry')
            for key in list(astrometry.keys()):
                grp.create_dataset(key, data=astrometry[key])


    return

###############################################################################
# Function for reading the photometry.
###############################################################################


class PhotFile(object):

    def __init__(self, filename):

        self.filename = filename

        return

    def read_stars(self, fields=None):

        stars = dict()

        with h5py.File(self.filename, 'r') as f:

            grp = f['stars']

            if fields is None:
                fields = list(grp.keys())

            for field in fields:

                if field in list(grp.keys()):
                    stars[field] = grp[field][()]
                else:
                    log.warning ('skipping field {}, field not found.'
                                 .format(field))

        return stars

    def read_station(self, fields=None, lstseq=None):

        station = dict()

        with h5py.File(self.filename, 'r') as f:

            grp = f['station']

            lstseq_station = grp['lstseq'][()]
            if fields is None:
                fields = list(grp.keys())

            for field in fields:

                if field in list(grp.keys()):
                    station[field] = grp[field][()]
                else:
                    log.warning ('skipping field {}, field not found.'
                                 .format(field))

        if lstseq is not None:

            idx = np.searchsorted(lstseq_station, lstseq)
            for key in list(station.keys()):
                station[key] = station[key][idx]

        return station

    def read_lightcurves(self, ascc=None, fields=None, perstar=True):

        onestar = False

        if ascc is None:
            stars = self.read_stars(['ascc'])
            ascc = stars['ascc']

        elif isinstance(ascc, str):
            onestar = True
            ascc = [ascc]

        nstars = len(ascc)
        #log.info ('reading lightcurves of {} stars'.format(nstars))
        curves = dict()
        nobs = np.zeros(nstars, dtype='int')

        with h5py.File(self.filename, 'r') as f:

            grp = f['lightcurves']
            if not hasattr(self, 'ascc0'):
                self.ascc0 = set(grp.keys())

            for i in range(nstars):

                if ascc[i].decode('utf-8') in self.ascc0:
                    curves[ascc[i]] = grp[ascc[i]][()]
                    nobs[i] = len(curves[ascc[i]])
                else:
                    log.warning ('skipping star {}, star not found.'
                                 .format(ascc[i]))

        if not curves:
            return curves

        # Select specified fields.
        if fields is not None:

            for i in range(nstars):
                curves[ascc[i]] = curves[ascc[i]][fields]

        # Combine lightcurves.
        if not perstar:

            strides = np.append(0, np.cumsum(nobs))
            tmp = np.recarray(strides[-1], dtype=curves[ascc[0]].dtype)

            for i in range(nstars):
                tmp[strides[i]:strides[i+1]] = curves[ascc[i]]

            curves = tmp

        if onestar:
            return curves[ascc[0]]

        return curves

###############################################################################
# Function for reading the systematics.
###############################################################################


def update_systable_old (filename, systable, nmax=5):

    data = np.loadtxt(systable, dtype='S')
    data = np.atleast_1d(data)

    if (len(data) == nmax):

        data = np.roll(data, 1)
        data[0] = filename

    else:

        data = np.append(filename, data)

    np.savetxt(systable, data, fmt='%s')

    return



def get_file (table):

    # check if input table exists
    if os.path.exists(table):

        # extract first line from table
        with open(table) as f:
            filename = f.readline().strip()

        # make sure that filename exists
        if not os.path.exists(filename):

            # try prepending cfg.datadir to the filename, as that is
            # the folder where the final data are saved in
            # rawdir_monitor mode
            filename_alt = os.path.normpath(os.path.join(cfg.datadir, filename))
            if os.path.exists(filename_alt):
                filename = filename_alt

            else:
                log.warn ('filename {} or {} not found; returning None'
                          .format(filename, filename_alt))
                filename = None

    else:
        log.warning ('{} not found; cannot extract filename'.format(table))
        filename = None


    return filename


def update_table(filename, table, nmax=10, abspath=True):

    if abspath:
        # add filename with absolute path
        filename = os.path.abspath(filename)

    # create table if it does not exist yet
    if not os.path.exists(table):
        with open(table, 'a') as f:
            f.write(filename + '\n')
    else:
        # copied from old update_darktable
        with open(table, 'r+') as f:
            data = f.readlines()                   # read old list
            data.insert(0, filename + '\n')        # prepend new file
            data = data[:nmax]                     # truncate to nmax
            f.truncate(0)                          # empty the file
            f.seek(0)                              # rewind to beginning
            f.writelines(data)                     # write back

    return



#def write_clouds(filename, nobs, clouds, sigma, std_w, lstmin=None, lstmax=None,
def write_clouds(filename, nobs, clouds, sigma, lstmin=None, lstmax=None,
                 lstlen=None, jd=None, header=None, overwrite=True):

    # HEALPix parameters
    level = cfg.hp_level
    nside = 2**cfg.hp_level
    npix = 12*4**cfg.hp_level

    # depending on filename extension, write hdf5 or fits file
    if '.hdf5' in filename:

        if os.path.exists(filename) and overwrite:
            os.remove(filename)

        # write .hdf5 file
        with h5py.File(filename, 'w') as f:

            f.create_dataset('nobs', data=nobs)
            f.create_dataset('clouds', data=clouds)
            f.create_dataset('sigma', data=sigma)
            #f.create_dataset('std_w', data=std_w)
            f.attrs['grid'] = 'healpix'
            # changed nx to nside as nx is also used for other things like
            # shape of the images and the various grids; made the value
            # configurable through parameter hp_level
            #f.attrs['nx'] = 8
            #f.attrs['nx'] = 2**cfg.hp_level
            f.attrs['level'] = level
            f.attrs['nside'] = nside
            f.attrs['npix'] = npix
            if lstmin is not None: f.attrs['lstmin'] = lstmin
            if lstmax is not None: f.attrs['lstmax'] = lstmax
            if lstlen is not None: f.attrs['lstlen'] = lstlen
            if jd is not None: f.attrs['jd'] = jd


    elif '.fits' in filename:

        # create fits table
        #names = ['nobs', 'clouds', 'sigma', 'std_w']
        names = ['nobs', 'clouds', 'sigma']
        #dtypes = ['int32', 'float32', 'float32', 'float32']
        dtypes = ['int32', 'float32', 'float32']
        descriptions = ['number of stars in HEALPix tile',
                        '[mag] weighted mean extinction in HEALPix tile',
                        '[mag] error in weighted mean extinction in HEALPix tile',
                        #'[mag] weighted sample standard deviation in HEALPix tile'
                        ]
        table = Table([nobs, clouds, sigma], names=names, dtype=dtypes,
                      descriptions=descriptions)
        table['clouds'].unit = u.mag
        table['sigma'].unit = u.mag
        #table['std_w'].unit = u.mag
        table.write(filename, overwrite=overwrite)

        # add header
        if header is not None:

            # add some info
            header['LEVEL'] = (level, 'HEALPix level')
            header['NSIDE'] = (nside, 'HEALPix nside = 2**level')
            header['NPIX'] = (npix, 'number of HEALPix tiles')
            if cfg.hp_nest:
                scheme = 'NEST'
            else:
                scheme = 'RING'

            header['SCHEME'] = (scheme, 'HEALPix scheme')

            if lstmin is not None: header['LSTMIN'] = lstmin
            if lstmax is not None: header['LSTMAX'] = lstmax
            if lstlen is not None: header['LSTLEN'] = lstlen

            # update header of fits table
            with fits.open(filename, 'update', memmap=True) as hdulist:

                hdulist[-1].header.update(header)

                # remove unnecessary keywords
                misc.cleanup_cloudheader (hdulist[-1].header)

                # flush hdulist with fix+warn verification
                hdulist.flush(output_verify='fix+warn')


    else:
        log.warning ('input cloud filename {} does not contain .hdf5 or .fits '
                     'extension; not writing any file'.format(filename))


    return


class SysFile(object):

    def __init__(self, filename):

        self.filename = filename

        return

    def read_magnitudes(self):

        with h5py.File(self.filename, 'r') as f:

            grp = f['data/magnitudes']

            ascc = grp['ascc'][()]
            nobs = grp['nobs'][()]
            mag = grp['mag'][()]
            sigma = grp['sigma'][()]

        return ascc, nobs, mag, sigma

    def read_transmission(self):

        with h5py.File(self.filename, 'r') as f:

            grp = f['data/transmission']

            idx1 = grp['idx1'][()]
            idx2 = grp['idx2'][()]
            nobs = grp['nobs'][()]
            trans = grp['trans'][()]

            nx = grp.attrs['nx']
            ny = grp.attrs['ny']

        pg = grids.PolarGrid(nx, ny)

        nobs = pg.values2grid(idx1, idx2, nobs, np.nan)
        trans = pg.values2grid(idx1, idx2, trans, np.nan)

        return pg, nobs, trans

    def read_intrapix(self):

        with h5py.File(self.filename, 'r') as f:

            grp = f['data/intrapix']

            idx1 = grp['idx1'][()]
            idx2 = grp['idx2'][()]
            nobs = grp['nobs'][()]
            amplitudes = grp['amplitudes'][()]

            nx = grp.attrs['nx']
            ny = grp.attrs['ny']

        pg = grids.PolarGrid(nx, ny)

        nobs = pg.values2grid(idx1, idx2, nobs, np.nan)
        amplitudes = np.stack([pg.values2grid(
            idx1, idx2, amplitudes[:, i], np.nan) for i in range(4)], axis=-1)

        return pg, nobs, amplitudes

    def read_clouds(self):

        with h5py.File(self.filename, 'r') as f:

            grp = f['data/clouds']

            idx = grp['idx'][()]
            lstseq = grp['lstseq'][()]
            nobs = grp['nobs'][()]
            clouds = grp['clouds'][()]
            sigma = grp['sigma'][()]
            #std_w = grp['std_w'][()]

            # changed nx to nside as nx is also used for
            # other things like shape of the images and the various
            # grids, and made it configurable through parameter
            # hp_level; for the moment take into account that
            # it might have been saved as nx
            try:
                nside = grp.attrs['nside']
            except:
                nside = grp.attrs['nx']

            lstmin = grp.attrs['lstmin']
            lstmax = grp.attrs['lstmax']
            lstlen = grp.attrs['lstlen']

        lstseq = lstseq - lstmin

        try:
            hg = grids.HealpixGrid(nside)
            #hg = grids.HealpixGrid(nside)
        except:
            hg = None


        # the number of healpixels, which was hardcoded before at 768
        # (level=3)
        hp_npix = 12 * 4**cfg.hp_level

        tmp = np.full((hp_npix, lstlen), fill_value=np.nan)
        tmp[idx, lstseq] = nobs
        nobs = tmp

        tmp = np.full((hp_npix, lstlen), fill_value=np.nan)
        tmp[idx, lstseq] = clouds
        clouds = tmp

        tmp = np.full((hp_npix, lstlen), fill_value=np.nan)
        tmp[idx, lstseq] = sigma
        sigma = tmp

        #tmp = np.full((hp_npix, lstlen), fill_value=np.nan)
        #tmp[idx, lstseq] = std_w
        #std_w = tmp

        #return hg, nobs, clouds, sigma, std_w, lstmin, lstmax
        return hg, nobs, clouds, sigma, lstmin, lstmax
