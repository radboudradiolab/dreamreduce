from setuptools import setup, find_packages

setup(name='dreamreduce',
      version='2024.03',
      description='DREAM reduction code',
      url='https://bitbucket.org/radboudradiolab/dreamreduce',
      author='Talens, Vreeswijk, Timmer, Stuik, Hoekstra',
      author_email='stuik@strw.leidenuniv.nl',
      packages=['dreamreduce'],
      #packages=find_packages(),
      zip_safe=False,
      install_requires=[
          'astropy',
          'Bottleneck',
          'ephem',
          'fitsio',
          'h5py',
          'healpy',
          'lmfit',
          'matplotlib',
          'numpy',
          'scipy',
          'sip_tpv',
          'zmq',
      ]
      )
