# dreamreduce

The dreamreduce package is an adaptation of the reduction software
used for [MASCARA](https://mascara.strw.leidenuniv.nl), which was
written by Geert Jan Talens. Dreamreduce processes images from DREAM,
a copy of MASCARA with various improvements. DREAM is located on Cerro
Pachon in Chile and has been built to provide full Southern sky cloud
extinction maps with a time resolution of about a minute and a spatial
resolution of a few degrees to aid in optimizing the [LSST
survey](https://www.lsst.org/scientists/survey-design) observations at
[Rubin Observatory](https://rubinobservatory.org/).

The figure below shows an overview of the dreamreduce dataflow.  The
five DREAM cameras record raw images every ~6.4s, which are processed
by dreamreduce independently on five different compute servers, one
for each camera. The calibration frames such as biases, flats and/or
darks are combined into master frames that can be used to reduce the
science images.  Each science images goes through the same processing
steps, consisting of dark subtraction, source extraction and sky
subtraction, astrometric calibration, aperture photometry, photometric
calibration and image subtraction. The compute servers contain 6 cores
each, so the incoming images can be processed in parallel using
python's multiprocessing (in the figure below 5 CPU cores are being
used).

For the processing to keep up with the rate of incoming images, the
image pixel-to-world coordinates astrometric solution is fit for every
N images, where N is a configurable parameter currently set to
5. Astrometry.net with Gaia DR3 index files are used to find the
solution. If the leading-image astrometric solution is within
(configurable) constraints - determined from comparing the ICRS
coordinates of the extracted sources with matching Gaia DR3 sources -
it is applied to the four subsequent or follower images. In that case,
the solution is also saved in the astromaster file along with the LST
and UTC epoch of the leading image. That astromaster file contains
solutions over an LST time span of 24 hours with a time resolution
currently set to 15 images (or ~100s). Should the astrometric solution
not be within constraints, the astromaster file is used to fetch the
solution nearest in LST (projected to the epoch of the relevant
image). If that solution is not acceptable, the nearest solution in
UTC is used instead. And if that is not within constraints either, the
best of the three (poor) solutions is used. Any accepted leading-image
astrometric solution will overwrite any older solution present in the
relevant LST bin of the astromaster file, so the astromaster is being
refreshed constantly.

The astrometric solution is also recorded in the image header (for
both leading and following images), which is saved along with the raw
and reduced images. The WCS solution in the header is used by SWarp to
transform the image taken 6.4s before the image that is being
processed to the astrometric frame of the current image. The
transformed previous image is then subtracted from the current image
to produce the difference image, which can be used to find highly
time-variable or fast-moving objects.

For the photometric calibration, magnitudes from the [ASCC
catalogue](https://vizier.cds.unistra.fr/viz-bin/VizieR-3?-source=I/280B/ascc)
and from performing aperture photometry at the projected Gaia DR3
position (including proper motion) of all stars down to V=8.4 in each
image are combined into calibration maps. These maps contain the
systematic magnitude residuals or zeropoints with an RA time
resolution of 6.4s (1 image) and a DEC resolution of 0.25 degree. The
best nights from the past two weeks of data are selected to create a
final calibration map that does not contain any additional extinction
due to e.g. clouds. The creation of these calibration maps is done
during the morning after the night has finished, so they can be used
for the on-the-fly calibration during the next observing night,
resulting in a cloud extinction map for each image at a chosen HEALPix
level (currently set to 4, correponding to 13.5 square degree per
HEALPix tile).

The cloud maps of the individual cameras are combined into a combined
cloud map of the sky observable by DREAM, where a chosen number of
6.4s epochs can be combined, currently set to 5, correspoinding to
32s. These combined maps, containing the cloud extinction and the
corresponding error (both in magnitudes) and the number of stars used
in each HEALPix tile, are saved as fits tables and sent to Rubin
observatory with a cadence of of 32s and an estimated mid-exposure
delay of about 30s (i.e. the time between the effective mid-exposure
date of the combined cloud map and the time at which the map is sent).


<br/>
![Dataflow](DREAM_dataflow.png)


<br/>
# running dreamreduce

The dreamreduce package is installed inside a singularity container
along with all the necessary software including [Source
Extractor](https://github.com/astromatic/sextractor),
[Astrometry.net](astrometry.net) and
[SWarp](https://github.com/astromatic/swarp).

### monitor mode

To run dreamreduce in a continuous cycle of nightly on-the-fly
processing of incoming images followed by the day-time calibrations:

    singularity run --bind /data,/tmpdata --app pipeline /home/dream/Containers/dreamreduce.sif

or alternatively:

    singularity exec --bind /data,tmpdata /home/dream/Containers/dreamreduce.sif python /Software/dreamreduce/dreamreduce/run_rawdir_monitor.py


This is the accompanying help message (run_rawdir_monitor -h):

```text
    usage: run_rawdir_monitor.py [-h] [--camid CAMID] [--twilight TWILIGHT] [--nbias NBIAS] [--ndark NDARK] [--nflat NFLAT] [--timeout TIMEOUT]

    Start continuous DREAM processing

    options:
      -h, --help           show this help message and exit
      --camid CAMID        camid; if not specified, it is inferred from environment variable HOSTNAME
      --twilight TWILIGHT  sun altitude at which to start the night; default=5
      --nbias NBIAS        number of bias frames to combine; default=20
      --ndark NDARK        number of dark frames to combine; default=20
      --nflat NFLAT        number of flat frames to combine; default=20
      --timeout TIMEOUT    number of exposures to wait for calibration files to complete; default=nbias/ndark/nflat+10
```


To use modules that have been edited instead of the original code
without having to rebuild the singularity container: prepare a new
folder (e.g. dreamreduce_edited) that is a copy of the dreamreduce
folder, and add it to the front of the PYTHONPATH inside the container
as follows so that the edited folder takes precedence over the
original code:

    singularity exec --bind /data,/tmpdata --env "PYTHONPATH=${HOME}/dreamreduce_edited:\$PYTHONPATH" /home/dream/Containers/dreamreduce.sif python /Software/dreamreduce/dreamreduce/run_rawdir_monitor.py


This is the default input and *temporary* output directory structure
(configurable in the configuration.py module):

* /tmpdata/data : incoming raw images
* /tmpdata/conf : configuration files and ASCC catalog
* /tmpdata/tmp  : temporary folder with products created during processing
* /tmpdata/rawarchive : where raw images with updated headers are initially saved
* /tmpdata/products   : output products with subfolders clouds, diff, lightcurves, logs,
                        masters and sys

The output is initially saved in /tmpdata as that is a fast SSD drive;
after a night of reduction, the rawarchive and products are saved to
the output folder (/data) on another, bigger disk:

* /data/rawarchive/[yyyymmdd][camera ID], e.g. /data/rawarchive/20231118CPC
* /data/products/[yyyymmdd][camera ID]:

### rereduce mode

To run dreamreduce in rereduce mode to (re-)process a particular night of
DREAM images, e.g. the central camera observations of 19 November 2023
that uses the calibration file from the night before:

    singularity exec /home/dream/Containers/dreamreduce.sif python /Software/dreamreduce/dreamreduce/run_rereduce.py /data/rawarchive/20231119CPC --sysfile /data/products/20231118CPC/sys/sys0_vmag_20231116CPC.hdf5 --zpsfile /data/products/20231118LSC/zeropoints/zps_comb_20231118CPC.fits --confdir ./conf --reddir ./products --tmpdir ./tmp

This is the accompanying help message (run_rereduce -h):

```text
usage: run_rereduce.py [-h] [--sysfile SYSFILE] [--zpsfile ZPSFILE] [--confdir CONFDIR] [--reddir REDDIR] [--tmpdir TMPDIR] [--arcdir ARCDIR] rawdir

(Re)reduce night of DREAM data

positional arguments:
  rawdir             input directory with raw files to process; string is required to end with yyyymmdd[camid], e.g. '/data/rawarchive/20231119CPC')

options:
  -h, --help         show this help message and exit
  --sysfile SYSFILE  photometric calibration sysfile from previous night, e.g. '/data/products/20231118CPC/sys/sys0_vmag_20231118CPC.hdf5'; default is to use the top entry in systable in confdir (see below); if explicitly set to
                     the string 'None' then rereduce is run without sysfile
  --zpsfile ZPSFILE  combined zeropoints calibration fits table to use, e.g. '/data/products/20231118LSC/zeropoints/zps_comb_20231118CPC.fits'; default is to use the top entry in zpstable in confdir (see below), if explicitly set
                     to the string 'None' then rereduce is run without zpsfile
  --confdir CONFDIR  input directory with configuration files; default='./conf'
  --reddir REDDIR    output directory to write products; default='./products'
  --tmpdir TMPDIR    directory to write temporary files; default='./tmp'
  --arcdir ARCDIR    directory to archive files; default=None; if left at the default, the raw data with WCS header will not be saved
```

The input folder with raw images and the location of the calibration
file are both required parameters. The optional parameters confdir
(configuration folder), reddir (output products) and tmpdir (temporary
folder) are left to their default values in the above command.
